<?php

/**
 * @file
 * Definition of configuration pages and overriden functions.
 */

require dirname(__FILE__) . '/includes/ecommerce-webservicelib/SeApi.php';

/**
 * Engine logic to run external API functions.
 */
function shippingeasy_engine($subfolder = NULL, $resource = NULL) {
  try
  {
    $usern = variable_get('shippingeasy_username', NULL);
    $passw = variable_get('shippingeasy_password', NULL);
    $generated_api_key = variable_get('shippingeasy_generated_apikey', '');

    $api = new SeApi($generated_api_key, SeApiType::Curl);

    $result = $api->executeResource($resource);
  }
  catch (Exception $e)
  {
    SeApiUtils::outputError('Request error: ' . $e->getMessage(), 500);
  }
}

/**
 * Configuration form implementation.
 */
function shippingeasy_admin_settings() {
  $form = array();

  global $base_url;

  $form['shippingeasy_generated_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Drupal API key'),
    '#default_value' =>  variable_get('shippingeasy_generated_apikey', ''),
    '#description' => t("When adding your store to the 'MyStores' hub in you account section of ShippingEasy.com, you will be asked for this API key."),
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form['shippingeasy_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Shop domain'),
    '#default_value' =>  variable_get('shippingeasy_api_url', $base_url . '/shippingeasy/api/'),
    '#description' => t("When adding your store to the 'MyStores' hub in you account section of ShippingEasy.com, you will be asked for this Shop domain."),
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form['shippingeasy_itemsforshipping_order_statuses'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Select the order statuses which the 'MyStores' hub will show available for shipping"),
    '#default_value' => variable_get('shippingeasy_itemsforshipping_order_statuses', _shippingeasy_itemsforshipping_order_statuses()),
    '#options' => _shippingeasy_itemsforshipping_order_statuses(),
    /*  '#description' => t('Select the order statuses that should be scoped and available to API.'),*/
  );

  $form['shippingeasy_markorder_order_status'] = array(
    '#type' => 'radios',
    '#title' => t("Select the order status which the 'MyStores' hub will use once the item has been shipped"),
    '#default_value' => variable_get('shippingeasy_markorder_order_status', 'completed'),
    '#options' => _shippingeasy_itemsforshipping_order_statuses(),
    /*  '#description' => t('Select the order status which will be used for MarkShippedItems API call.'),*/
  );

  return system_settings_form($form);
}

/**
 * Shipping method configuration form implementation.
 */
function shippingeasy_method_admin_settings() {
  $form = array();

  $form['shippingeasy_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('ShippingEasy API KEY'),
    '#default_value' =>  variable_get('shippingeasy_apikey', ''),
    '#required' => TRUE,
    '#description' => t('To start using ShippingEasy, you will need to connect your store to a ShippingEasy account using an API key. The API key can be found at the bottom of the My Details tab in the <a href="https://www.shippingeasy.com/my" target="_blank">My Account</a> area of the site.'),
  );

  drupal_add_js("
    function checkUncheckAll(theElement, courier)
    {
      var theForm = theElement.form, z = 0;

      for(z=0; z<theForm.length;z++)
      {
        if(theForm[z].type == 'checkbox' && theForm[z].name != 'checkall' && theForm[z].name.indexOf(courier+'[') != -1)
        {
          theForm[z].checked = theElement.checked;
        }
      }
    }
  ", 'inline');

  $couriers = _shippingeasy_couriers();

  foreach ($couriers as $key => $value) {
    $courier_name = str_replace(' ', '_', strtolower($value));

    $form[$key . '_service_choices'] = array(
      '#type' => 'fieldset',
      '#title' => t($value . ' Services'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    switch ($key) {
      case 1:
        $description = 'Select which Fedex services are shown to the customer on checkout.<br/>
        . Note: Fedex services are available for US domestic and any country to any country only.';
        break;

      case 4:
        $description = 'Select which UPS services are shown to the customer on checkout.<br/>
        . Note: UPS services are available for AU export only.';
        break;

      case 7:
        $description = 'Select which USPS services are shown to the customer on checkout.<br/>
        . Note: USPS services are available for US domestic and US export only.';
        break;

      case 6:
        $description = 'Select which Fastway services are shown to the customer on checkout.<br/>
        . Note: Fastway services are available for AU domestic only.';
        break;

      case 8:
        $description = 'Select which MailCall services are shown to the customer on checkout.<br/>
        . Note: MailCall services are available for AU domestic - Sydney to Sydney and Melbourne to Melbourne only.';
        break;

      default:
        $description = 'Select the ShippingEasy ' . $value . ' services that are available to customers.';
    }

    $form[$key . '_service_choices']['shippingeasy_' . $courier_name . $key] = array(
      '#type' => 'checkboxes',
      /*  '#title' => t($value . ' Services'),*/
      '#default_value' => variable_get('shippingeasy_' . $courier_name . $key, _shippingeasy_services($key)),
      '#options' => _shippingeasy_services($key),
      '#description' => t($description),
    );

    $form[$key . '_service_choices']['shippingeasy_' . $courier_name . 'checkall'] = array(
      '#type' => 'checkbox',
      '#title' => t('Select / Unselect all'),
      '#default_value' => variable_get('shippingeasy_' . $courier_name . 'checkall', NULL),
      '#attributes' => array('onclick' => 'checkUncheckAll(this, ' . $key . ');'),
      '#weight' => -1,
    );
  }

  $form['shippingeasy_handling_markup'] = array(
    '#type' => 'textfield',
    '#title' => t('Handling mark-up'),
    '#maxlength' => 6,
    '#size' => 4,
    '#default_value' =>  variable_get('shippingeasy_handling_markup', 0),
    '#description' => t("For a fixed amount enter number, for percentage enter number followed by %"),
  );

  $form['shippingeasy_display_graphics'] = array(
    '#type' => 'radios',
    '#title' => t('Display Service Logos During Checkout?'),
    '#default_value' => variable_get('shippingeasy_display_graphics', 1),
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
    '#description' => t('Indicate whether you would like the carrier logos shown next to the available services.'),
  );

  $form['shippingeasy_display_time_estimate'] = array(
    '#type' => 'radios',
    '#title' => t('Display Delivery Time Estimate During Checkout?'),
    '#default_value' => variable_get('shippingeasy_display_time_estimate', 1),
    '#options' => array(
      t('No'),
      t('Yes'),
    ),
    '#description' => t('Display estimated delivery time in days'),
  );

  $form['shippingeasy_all_in_one'] = array(
    '#type' => 'radios',
    '#title' => t('Product values'),
    '#default_value' => variable_get('shippingeasy_all_in_one', 0),
    '#options' => array(
      /*  0 => t('Each in its own package'),*/
      /*  1 => t('All in one'),*/
    ),
    '#description' => t('Please specify a default package value to be used across your entire product range. If you would like to specifiy specific default package values per product, you are able to do this in the product settings section.'),
    /*  '#description' => t('Indicate whether each product is quoted as shipping separately or all in one package.'),*/
  );

  $form['shippingeasy_default_product_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#maxlength' => 6,
    '#size' => 4,
    '#default_value' =>  variable_get('shippingeasy_default_product_height', 2),
    '#required' => TRUE,
    /*  '#description' => t('Default product height if it is not already defined for product.'),*/
  );

  $form['shippingeasy_default_product_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#maxlength' => 6,
    '#size' => 4,
    '#default_value' =>  variable_get('shippingeasy_default_product_width', 2),
    '#required' => TRUE,
    /*  '#description' => t('Default product width if it is not already defined for product.'),*/
  );

  $form['shippingeasy_default_product_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Length'),
    '#maxlength' => 6,
    '#size' => 4,
    '#default_value' =>  variable_get('shippingeasy_default_product_length', 2),
    '#required' => TRUE,
    /*  '#description' => t('Default product length if it is not already defined for product.'),*/
  );

  $form['shippingeasy_default_product_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#maxlength' => 6,
    '#size' => 4,
    '#default_value' =>  variable_get('shippingeasy_default_product_weight', 2),
    '#required' => TRUE,
    /*  '#description' => t('Default product weight if it is not already defined for product.'),*/
  );

  $form['shippingeasy_default_product_units'] = array(
    '#type' => 'radios',
    '#title' => t('Measurement units'),
    '#default_value' => variable_get('shippingeasy_default_product_units', 0),
    '#options' => array(
      0 => t('Imperial (lb, in)'),
      1 => t('Metric (kg, cm)'),
    ),
    /*  '#description' => t('Default product units if they are not already defined for product.'),*/
  );

  return system_settings_form($form);
}

/**
 * Displays the main order admin screen, an overview of all received orders.
 */
function shippingeasy_admin($sql = NULL, $args = NULL, $search = FALSE) {
  $header = array(
    array('data' => t('Actions')),
    array(
      'data' => t('Order ID'),
      'field' => 'o.order_id',
      'sort' => 'desc',
    ),
    array('data' => t('Customer')),
    array(
      'data' => t('Total'),
      'align' => 'center',
      'field' => 'o.order_total',
    ),
    array(
      'data' => t('Purchase date'),
      'align' => 'center',
      'field' => 'o.created',
    ),
    array(
      'data' => t('Status'),
      'field' => 'os.title',
    ),
    array(
      'data' => t('Shipping Method'),
      'field' => 'oh.method',
    ),
  );

  if (is_null($sql)) {
    $args = array();
    $show_status = 1;

    $sql = 'SELECT o.order_id, o.uid, o.billing_first_name, o.billing_last_name, o.billing_company, o.order_total, o.order_status, o.created, oh.method, os.title '
          . 'FROM {uc_orders} o LEFT OUTER JOIN {uc_order_quotes} oh ON o.order_id = oh.order_id LEFT JOIN {uc_order_statuses} os ON o.order_status = os.order_status_id ';

    if (arg(3) == 'sort' && !is_null(arg(4))) {
      $_SESSION['sort_status'] = arg(4);
      $args[] = arg(4);
    }
    $where = '';
    if (!isset($_SESSION['sort_status']) || $_SESSION['sort_status'] != 'all') {
      if (!empty($_SESSION['sort_status']) && is_string($_SESSION['sort_status'])) {
        $where = "WHERE o.order_status = '%s'";
        $args[] = $_SESSION['sort_status'];
      }
      else {
        $where = 'WHERE o.order_status IN ' . uc_order_status_list('general', TRUE);
      }
    }
    $sql .= $where . tablesort_sql($header);
  }

  $address = variable_get('uc_customer_list_address', 'billing');
  if ($address == 'shipping') {
      $sql = str_replace('billing', 'delivery', $sql);
  }
  else {
    $address = 'billing';
  }

  $context = array(
    'revision' => 'themed-original',
    'type' => 'amount',
  );

  $rows = array();
  $result = pager_query($sql, variable_get('uc_order_number_displayed', 30), 0, NULL, $args);
  while ($order = db_fetch_object($result)) {
    if ($address == 'shipping') {
      $order_name = $order->delivery_first_name . ' ' . $order->delivery_last_name;
    }
    else {
      $order_name = $order->billing_first_name . ' ' . $order->billing_last_name;
    }
    if (trim($order_name) == '') {
      if ($order->uid !== 0) {
        $account = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $order->uid));
      }
      if (empty($account)) {
        $order_name = t('User: none');
      }
      else {
        $order_name = t('User: !name', array('!name' => $account));
      }
    }

    $rows[] = array(
      'data' => array(
        array(
          'data' => uc_order_actions($order, TRUE),
          'nowrap' => 'nowrap',
        ),
        array('data' => $order->order_id),
        array(
          'data' => check_plain($order_name),
          'nowrap' => 'nowrap',
        ),
        array(
          'data' => uc_price($order->order_total, $context),
          'align' => 'right',
          'nowrap' => 'true',
        ),
        array(
          'data' => format_date($order->created, 'custom', variable_get('uc_date_format_default', 'm/d/Y')),
          'align' => 'center',
        ),
        array('data' => $order->title),
        array('data' => $order->method),
      ),
      'id' => 'order-' . $order->order_id,
    );
  }

  drupal_add_js(array(
    'ucURL' => array(
      'adminOrders' => url('admin/store/orders/'),
    ),
  ), 'setting');
  drupal_add_js(drupal_get_path('module', 'uc_order') . '/uc_order.js');

  if ($search === FALSE) {
    $output = '<div class="order-overview-form">' . drupal_get_form('uc_order_select_form') . '</div>'
             . '<div class="order-overview-form">' . drupal_get_form('uc_order_admin_sort_form') . '</div>';
  }

  $output .= theme('table', $header, $rows, array('class' => 'uc-orders-table'));
  $output .= theme('pager', NULL, variable_get('uc_order_number_displayed', 30), 0);
  return $output;
}

/**
 * Displays a search form to browse all received orders.
 */
function shippingeasy_usearch() {
  $output = drupal_get_form('uc_order_search_form');

  if (arg(4) == 'results') {
    $output .= '<p>' . t('Search returned the following results:') . '</p>';

    $billing_first_name = strtolower(str_replace('*', '%', check_plain(arg(5))));
    $billing_last_name = strtolower(str_replace('*', '%', check_plain(arg(6))));
    $billing_company = strtolower(str_replace('*', '%', check_plain(arg(7))));
    $shipping_first_name = strtolower(str_replace('*', '%', check_plain(arg(8))));
    $shipping_last_name = strtolower(str_replace('*', '%', check_plain(arg(9))));
    $shipping_company = strtolower(str_replace('*', '%', check_plain(arg(10))));
    $start_date = check_plain(arg(11));
    $end_date = check_plain(arg(12));

    $args = array();
    if ($billing_first_name !== '0' && $billing_first_name !== '%') {
      $where .= " AND LOWER(o.billing_first_name) LIKE '%s'";
      $args[] = $billing_first_name;
    }
    if ($billing_last_name !== '0' && $billing_last_name !== '%') {
      $where .= " AND LOWER(o.billing_last_name) LIKE '%s'";
      $args[] = $billing_last_name;
    }
    if ($billing_company !== '0' && $billing_company !== '%') {
      $where .= " AND LOWER(o.billing_company) LIKE '%s'";
      $args[] = $billing_company;
    }
    if ($shipping_first_name !== '0' && $shipping_first_name !== '%') {
      $where .= " AND LOWER(o.delivery_first_name) LIKE '%s'";
      $args[] = $shipping_first_name;
    }
    if ($shipping_last_name !== '0' && $shipping_last_name !== '%') {
      $where .= " AND LOWER(o.delivery_last_name) LIKE '%s'";
      $args[] = $shipping_last_name;
    }
    if ($shipping_company !== '0' && $shipping_company !== '%') {
      $where .= " AND LOWER(o.delivery_company) LIKE '%s'";
      $args[] = $shipping_company;
    }

    if ($start_date !== '0') {
      $where .= " AND o.created >= %d";
      $args[] = $start_date;
    }
    if ($end_date !== '0') {
      $where .= " AND o.created <= %d";
      $args[] = $end_date;
    }

    $sql = 'SELECT o.order_id, o.billing_first_name, o.billing_last_name, o.order_total, '
          . 'o.order_status, o.created, oh.method, os.title FROM {uc_orders} o LEFT OUTER JOIN {uc_order_quotes} oh ON o.order_id = oh.order_id LEFT JOIN {uc_order_statuses} os '
          . 'ON o.order_status = os.order_status_id WHERE o.order_status NOT IN ' . uc_order_status_list('specific', TRUE)
         . $where . ' ORDER BY o.created DESC';

    $output .= shippingeasy_admin($sql, $args, TRUE);
  }

  return $output;
}

// Include below functions only in case of specific paths.
if (drupal_match_path($_GET['q'], 'admin/store/orders') ||
    drupal_match_path($_GET['q'], 'admin/store/orders/view') ||
    drupal_match_path($_GET['q'], 'admin/store/orders/sort/*')) {

  /**
   * Creates a textfield box to select an order by ID on the order
   * overview screen.
   */
  function uc_order_select_form() {
    $form['order_id'] = array(
      '#type' => 'textfield',
      '#title' => t('View order number'),
      '#size' => 10,
      '#maxlength' => 10,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('View'),
      '#attributes' => array('style' => 'display: none;'),
    );

    return $form;
  }

  /**
   * Form submission handler for uc_order_select_form().
   */
  function uc_order_select_form_submit($form, &$form_state) {
    if (uc_order_exists($form_state['values']['order_id'])) {
      drupal_goto('admin/store/orders/' . $form_state['values']['order_id']);
    }
  }

  /**
   * Creates the order status select box on the order overview screen.
   */
  function uc_order_admin_sort_form() {
    $options = array('-1' => t('Active orders'));

    foreach (uc_order_status_list() as $status) {
      $options[$status['id']] = $status['title'];
    }

    $options['all'] = t('All orders');

    if (!isset($_SESSION['sort_status']) || is_null($_SESSION['sort_status'])) {
      $default_status = -1;
    }
    else {
      $default_status = $_SESSION['sort_status'];
    }

    $form['status'] = array(
      '#type' => 'select',
      '#title' => t('View by status'),
      '#options' => $options,
      '#default_value' => $default_status,
      '#attributes' => array('onchange' => 'this.form.submit();'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'View',
      '#attributes' => array('style' => 'display: none;'),
    );

    return $form;
  }

  /**
   * Form submission handler for uc_order_admin_sort_form().
   */
  function uc_order_admin_sort_form_submit($form, &$form_state) {
    if ($form_state['values']['status'] == '-1') {
      unset($_SESSION['sort_status']);
      drupal_goto('admin/store/orders');
    }
    else {
      $_SESSION['sort_status'] = $form_state['values']['status'];
      drupal_goto('admin/store/orders/sort/' . $form_state['values']['status']);
    }
  }
}

/**
 * Displays a table of all shipments and tracking numbers.
 */
function shippingeasy_tracking_numbers($user_id = NULL) {
  $header = array(
    array('data' => t('Shipping ID'), 'field' => 's.sid'),
    array('data' => t('Order ID'), 'field' => 's.order_id'),
    array('data' => t('Date'), 'field' => 's.changed', 'sort' => 'desc'),
    array('data' => t('Shipping Method'), 'field' => 's.shipping_method'),
    array('data' => t('Tracking Number'), 'field' => 's.tracking_number'),
  );

  $args = array();

  $sql = "SELECT s.sid, s.order_id, s.tracking_number, s.changed, s.shipping_method
  FROM uc_shipments s
    LEFT JOIN uc_orders o ON s.order_id = o.order_id
  WHERE o.uid = " . $user_id . " AND s.tracking_number IS NOT NULL ";

  $sql .= tablesort_sql($header);

  $rows = array();

  $result = pager_query($sql, variable_get('shippingeasy_number_displayed', 100), 0, NULL, $args);

  while ($order = db_fetch_object($result)) {
    $rows[] = array(
      'data' => array(
        array('data' => $order->sid),
        array('data' => $order->order_id),
        array(
          'data' => format_date($order->changed, 'custom', variable_get('uc_date_format_default', 'm/d/Y')),
          'align' => 'center',
        ),
        array('data' => $order->shipping_method),
        array('data' => $order->tracking_number),
      ),
      'id' => 'order-' . $order->sid,
    );
  }

  $output = theme('table', $header, $rows, array('class' => 'shippingeasy-table'));
  $output .= theme('pager', NULL, variable_get('shippingeasy_number_displayed', 100), 0);

  return $output;
}
