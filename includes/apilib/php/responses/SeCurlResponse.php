<?php
SeServiceUtils::checkInclude('SeServiceResponse');
/**
 * SeCurlResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents base class for all curl responses. It extends SeServiceResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlRequest.v.0.1
 */
class SeCurlResponse extends SeServiceResponse
{
  /**
   * Response result.
   *
   * @var string
   */
  protected $result;

  /**
   * Response httpCode.
   *
   * @var int
   */
  protected $httpCode;

  /**
   * Response contentType.
   *
   * @var string
   */
  protected $contentType;

  /**
   * Response downloadContentLength.
   *
   * @var floatl
   */
  protected $downloadContentLength;

  /**
   * Response preTransferTime.
   *
   * @var int
   */
  protected $preTransferTime;

  /**
   * Response startTransferTime.
   *
   * @var int
   */
  protected $startTransferTime;

  /**
   * Response connectTime.
   *
   * @var int
   */
  protected $connectTime;

  /**
   * Response nameLookupTime.
   *
   * @var int
   */
  protected $nameLookupTime;

  /**
   * Response speedDownload.
   *
   * @var float
   */
  protected $speedDownload;

  /**
   * Response totalTime.
   *
   * @var int
   */
  protected $totalTime;

  /**
   * Response requestSize.
   *
   * @var float
   */
  protected $requestSize;

  /**
   * Response headerSize.
   *
   * @var int
   */
  protected $headerSize;

  /**
   * Response responseUrl.
   *
   * @var string
   */
  protected $responseUrl;


  public function __construct()
  {
    parent::__construct();
  }

  /**
   * GetResult getter.
   *
   * @param
   *
   * @return result
   */
  public function getResult()
  {
    return $this->result;
  }

  /**
   * SetResult setter.
   *
   * @param result
   *
   * @return
   */
  public function setResult($result)
  {
    $this->result = $result;
  }

  /**
   * getHttpCode getter.
   *
   * @param
   *
   * @return httpCode
   */
  public function getHttpCode()
  {
    return $this->httpCode;
  }

  /**
   * SetHttpCode setter.
   *
   * @param code
   *
   * @return
   */
  public function setHttpCode($code)
  {
    $this->httpCode = $code;
  }

  /**
   * GetContentType getter.
   *
   * @param
   *
   * @return contentType
   */
  public function getContentType()
  {
    return $this->contentType;
  }

  /**
   * SetContentType setter.
   *
   * @param type
   *
   * @return
   */
  public function setContentType($type)
  {
    $this->contentType = $type;
  }

  /**
   * GetDownloadContentLength getter.
   *
   * @param
   *
   * @return downloadCOntentLength
   */
  public function getDownloadContentLength()
  {
    return $this->downloadContentLength;
  }

  /**
   * SetDownloadContentLength setter.
   *
   * @param length
   *
   * @return
   */
  public function setDownloadContentLength($length)
  {
    $this->downloadContentLength = $length;
  }

  /**
   * GetPreTransferTime getter.
   *
   * @param
   *
   * @return preTransferTime
   */
  public function getPreTransferTime()
  {
    return $this->preTransferTime;
  }

  /**
   * SetPreTransferTime setter.
   *
   * @param time
   *
   * @return
   */
  public function setPreTransferTime($time)
  {
    $this->preTransferTime = $time;
  }

  /**
   * GetStartTransferTime getter.
   *
   * @param
   *
   * @return startTransferTime
   */
  public function getStartTranfserTime()
  {
    return $this->startTransferTime;
  }

  /**
   * SetStartTransferTime setter.
   *
   * @param time
   *
   * @return
   */
  public function setStartTransferTime($time)
  {
    $this->startTransferTime = $time;
  }

  /**
   * GetConnectTime getter.
   *
   * @param
   *
   * @return connectTime
   */
  public function getConnectTime()
  {
    return $this->connectTime;
  }

  /**
   * SetConnectTime setter.
   *
   * @param time
   *
   * @return
   */
  public function setConnectTime($time)
  {
    $this->connectTime = $time;
  }

  /**
   * GetNameLookupTime getter.
   *
   * @param
   *
   * @return nameLookupTime
   */
  public function getNameLookupTime()
  {
    return $this->nameLookupTime;
  }

  /**
   * SetNameLookupTime setter.
   *
   * @param time
   *
   * @return
   */
  public function setNameLookupTime($time)
  {
    $this->nameLookupTime = $time;
  }

  /**
   * GetSpeedDownload getter.
   *
   * @param
   *
   * @return speedDownload
   */
  public function getSpeedDownload()
  {
    return $this->speedDownload;
  }

  /**
   * SetSpeedDownload setter.
   *
   * @param speed
   *
   * @return
   */
  public function setSpeedDownload($speed)
  {
    $this->speedDownload = $speed;
  }

  /**
   * GetTotalTime getter.
   *
   * @param
   *
   * @return totalTime
   */
  public function getTotalTime()
  {
    return $this->totalTime;
  }

  /**
   * SetTotalTime setter.
   *
   * @param time
   *
   * @return
   */
  public function setTotalTime($time)
  {
    $this->totalTime = $time;
  }

  /**
   * GetRequestSize getter.
   *
   * @param
   *
   * @return requestSize
   */
  public function getRequestSize()
  {
    return $this->requestSize;
  }

  /**
   * SetRequestSize setter.
   *
   * @param size
   *
   * @return
   */
  public function setRequestSize($size)
  {
    $this->requestSize = $size;
  }

  /**
   * GetHeaderSize getter.
   *
   * @param
   *
   * @return headerSize
   */
  public function getHeaderSize()
  {
    return $this->headerSize;
  }

  /**
   * SetHeaderSize setter.
   *
   * @param size
   *
   * @return
   */
  public function setHeaderSize($size)
  {
    $this->headerSize = $size;
  }

  /**
   * GetResponseUrl getter.
   *
   * @param
   *
   * @return responseUrl
   */
  public function getResponseUrl()
  {
    return $this->responseUrl;
  }

  /**
   * SetResponseUrl setter.
   *
   * @param url
   *
   * @return
   */
  public function setResponseUrl($url)
  {
    $this->responseUrl = $url;
  }

  /**
   * Function to parse cURL response headers.
   *
   * @param
   *
   * @return
   */
  protected function parseHeaders($headers)
  {
    // set curl response headers
    $this->setHttpCode($headers['http_code']);
    $this->setContentType($headers['content_type']);
    $this->setDownloadContentLength($headers['download_content_length']);
    $this->setPreTransferTime($headers['pretransfer_time']);
    $this->setStartTransferTime($headers['starttransfer_time']);
    $this->setConnectTime($headers['connect_time']);
    $this->setNameLookupTime($headers['namelookup_time']);
    $this->setSpeedDownload($headers['speed_download']);
    $this->setTotalTime($headers['total_time']);
    $this->setRequestSize($headers['request_size']);
    $this->setHeaderSize($headers['header_size']);
    $this->setResponseUrl($headers['url']);
  }

  /**
   * Function to determine outcome of the request.
   *
   * @param
   *
   * @return
   */
  protected function determineSuccess()
  {
    $this->parseHeaders($this->getHeaders());

    // check if api content type is json. if not throw server exception.
    if ($this->getContentType() == 'application/json')
    {
      $json = json_decode($this->getBody());
    }
    elseif ($this->getContentType() == 'application/pdf')
    {
      // do nothing at this point
    }
    else
    {
      throw new SeServerException('Response content type is not in correct format.');
    }

    if ($this->getHttpCode() >= 200 && $this->getHttpCode() < 300)
    {
      $this->setSuccessful(true);
    }
    else
    {
      $this->setSuccessful(false);

      $utils = new SeServiceUtils();
      $responseCodes = (array)$utils->getResponseCodes();

      if (array_key_exists($this->getHttpCode(), $responseCodes))
      {
        throw new SeRequestException($this->getHttpCode(), $responseCodes[$this->getHttpCode()]);
      }
      else
      {
        throw new SeRequestException($this->getHttpCode(), 'Unknown response code.');
      }
    }
  }
}
?>