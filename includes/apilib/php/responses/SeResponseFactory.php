<?php
SeServiceUtils::checkInclude(array('SeResourceName', 'SeServiceType', 'SeServerException'));
/**
 * SeResponseFactory.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation to determine which response should be instantiated and returned.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeResponseFactory.v.0.1
 */
class SeResponseFactory
{
  public static function create($resourceName, $serviceType=SeServiceType::Curl)
  {
    // class name concatenation
    $className = 'Se'.$serviceType.$resourceName.'Response';

    try
    {
      SeServiceUtils::checkInclude($className);

      return new $className();
    }
    catch (Exception $e)
    {
      throw new SeServerException($e->getMessage() . ' (' . $className . ').');
    }
  }
}
?>