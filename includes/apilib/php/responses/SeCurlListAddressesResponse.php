<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SeAddress'));
/**
 * SeCurlListAddressesResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of ListAddresses cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlListAddressesResponse.v.0.1
 */
class SeCurlListAddressesResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param headers
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    $json = json_decode($body);

    // initialize result
    $result = array();

    // if is array -> parse in loop, else return empty array
    if (is_array($json->address))
    {
      foreach($json->address as $item)
      {
        try
        {
          $dto = new SeAddress();

          if (isset($item->Name))
          {
            $dto->setName($item->Name);
          }

          if (isset($item->Description))
          {
            $dto->setDescription($item->Description);
          }

          if (isset($item->IsDefault))
          {
            $dto->setIsDefault($item->IsDefault);
          }

          if (isset($item->Id))
          {
            $dto->setId($item->Id);
          }

          if (isset($item->Type))
          {
            $dto->setType($item->Type);
          }

          if (isset($item->FirstName))
          {
            $dto->setFirstName($item->FirstName);
          }

          if (isset($item->LastName))
          {
            $dto->setLastName($item->LastName);
          }

          if (isset($item->CompanyName))
          {
            $dto->setCompanyName($item->CompanyName);
          }

          if (isset($item->Line1))
          {
            $dto->setLine1($item->Line1);
          }

          if (isset($item->Line2))
          {
            $dto->setLine2($item->Line2);
          }

          if (isset($item->PostalCode))
          {
            $dto->setPostalCode($item->PostalCode);
          }

          if (isset($item->City))
          {
            $dto->setCity($item->City);
          }

          if (isset($item->Country))
          {
            $dto->setCountry($item->Country);
          }

          if (isset($item->CountryDivisionId))
          {
            $dto->setCountryDivisionId($item->CountryDivisionId);
          }

          if (isset($item->TelephoneAreaCode))
          {
            $dto->setTelephoneAreaCode($item->TelephoneAreaCode);
          }

          if (isset($item->TelephoneNumber))
          {
            $dto->setTelephoneNumber($item->TelephoneNumber);
          }

          if (isset($item->EmailAddress))
          {
            $dto->setEmailAddress($item->EmailAddress);
          }

          if (isset($item->CreatedAt))
          {
            $dto->setCreatedAt($item->CreatedAt);
          }

          if (isset($item->AddressType))
          {
            $dto->setAddressType($item->AddressType);
          }

          if (isset($item->CountryDivision))
          {
            $dto->setCountryDivision($item->CountryDivision);
          }

          array_push($result, $dto);
        }
        catch (Exception $e)
        {
          $this->setSuccessful(false);

          throw new SeServerException($e->getMessage());
        }
      }

      $this->setResult($result);
    }
    else
    {
      $this->setResult($result);

      $this->setSuccessful(false);

      throw new SeServerException('Expected JSON format for this call is array, but array is not returned.');
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getAddresses()
  {
    return $this->getResult();
  }
}
?>