<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SePickup'));
/**
 * SeCurlCreatePickupResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of CreatePickup cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlCreatePickupResponse.v.0.1
 */
class SeCurlCreatePickupResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    $json = json_decode($body);

    // initialize result
    $result = array();

    try
    {
      foreach ($json->shipments as $key => $value)
      {
        $dto = new SePickup();

        if (isset($value->Id))
        {
          $dto->setId($value->Id);
        }

        if (isset($value->Status))
        {
          $dto->setStatus($value->Status);
        }

        if (isset($value->Message))
        {
          $dto->setMessage($value->Message);
        }

        if (isset($value->TrackingNumber))
        {
          $dto->setTrackingNumber($value->TrackingNumber);
        }

        if (isset($value->DocumentsUri))
        {
          $dto->setDocumentsUri($value->DocumentsUri);
        }

        $result[$key] = $dto;
      }

      $this->setResult($result);
    }
    catch (Exception $e)
    {
      throw new SeServerException('Error during parsing CreatePickup response.', null, $e);
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getCreatePickup()
  {
    return $this->getResult();
  }
}
?>