<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SeRate'));
/**
 * SeCurlGetRateResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of GetRate cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetRateResponse.v.0.1
 */
class SeCurlGetRateResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param headers
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    $json = json_decode($body);

    // initialize result
    $result = array();

    // if is array -> parse in loop, else return empty array
    if (is_array($json->rates))
    {
      foreach($json->rates as $item)
      {
        // populate dto
        try
        {
          $dto = new SeRate();

          $dto->setId($item->Id);
          $dto->setCurrencySymbol($item->CurrencySymbol);
          $dto->setCurrencyCode($item->CurrencyCode);
          $dto->setCurrencyValue($item->CurrencyValue);
          $dto->setCourierId($item->CourierId);
          $dto->setCourier($item->Courier);
          $dto->setServiceId($item->ServiceId);
          $dto->setServiceName($item->ServiceName);

          if (isset($item->InsuranceCurrency))
          {
            $dto->setInsuranceCurrency($item->InsuranceCurrency);
          }

          if (isset($item->InsuranceAmount))
          {
            $dto->setInsuranceAmount($item->InsuranceAmount);
          }

          if (isset($item->AdditionalInsuranceAvailable))
          {
            $dto->setAdditionalInsuranceAvailable($item->AdditionalInsuranceAvailable);
          }

          if (isset($item->AdditionalInsuranceCurrency))
          {
            $dto->setAdditionalInsuranceCurrency($item->AdditionalInsuranceCurrency);
          }

          if (isset($item->AdditionalInsuranceLimit))
          {
            $dto->setAdditionalInsuranceLimit($item->AdditionalInsuranceLimit);
          }

          $dto->setEta($item->Eta);
          $dto->setHumanEta($item->HumanEta);
          $dto->setPickupTimes($item->PickupTimes);
          $dto->setDropOffAvailable($item->DropOffAvailable);

          if (isset($item->AdditionalInsuranceAmounts))
          {
            $dto->setAdditionalInsuranceAmounts($item->AdditionalInsuranceAmounts);
          }

          if (isset($item->Discount))
          {
            $dto->setDiscount($item->Discount);
          }

          if (isset($json->meta->Date))
          {
            $dto->setMetaDate($json->meta->Date);
          }

          array_push($result, $dto);
        }
        catch (Exception $e)
        {
          $this->setSuccessful(false);

          throw new SeServerException($e->getMessage());
        }
      }

      $this->setResult($result);
    }
    else
    {
      $this->setResult($result);

      $this->setSuccessful(false);

      throw new SeServerException('Expected JSON format for this call is array, but array is not returned.');
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getRates()
  {
    return $this->getResult();
  }
}
?>