<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SeHsCode'));
/**
 * SeCurlMatchHsCodeResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of MatchHsCode cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlMatchHsCodeResponse.v.0.1
 */
class SeCurlMatchHsCodeResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param headers
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    $json = json_decode($body);

    // initialize result
    $result = array();

    // if is array -> parse in loop, else return empty array
    if (is_array($json->hsCodes))
    {
      foreach($json->hsCodes as $item)
      {
        // we need to extract category from description
        try
        {
          // parsing for categories
          $leafs = explode('%%%%%%%%%%%%%%% ', $item->Description);

          if (count($leafs) > 1)
          {
            $dto = new SeHsCode($item->Id, $leafs[0], $leafs[1]);
          }
          else
          {
            // no category
            $dto = new SeHsCode($item->Id, null, $leafs[0]);
          }

          array_push($result, $dto);
        }
        catch (Exception $e)
        {
          $this->setSuccessful(false);

          throw new SeServerException($e->getMessage());
        }
      }

      $this->setResult($result);
    }
    else
    {
      $this->setResult($result);

      $this->setSuccessful(false);

      throw new SeServerException('Expected JSON format for this call is array, but array is not returned.');
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getHsCodes()
  {
    return $this->getResult();
  }
}
?>