<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SePackageItem'));
/**
 * SeCurlGetPackageDetailsResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of GetPackageDetails cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetPackageDetailsResponse.v.0.1
 */
class SeCurlGetPackageDetailsResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param headers
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    $json = json_decode($body);

    try
    {
      $dto = new SePackageItem($json->package->Id, $json->package->Name, $json->package->Description,
                               $json->package->Weight, $json->package->Length,
                               $json->package->Width, $json->package->Height,
                               $json->package->IsMetric);

      $this->setResult($dto);
    }
    catch (Exception $e)
    {
      $this->setSuccessful(false);

      throw new SeServerException($e->getMessage());
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getPackage()
  {
    return $this->getResult();
  }
}
?>