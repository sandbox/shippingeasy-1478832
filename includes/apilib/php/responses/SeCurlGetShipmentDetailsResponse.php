<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SeShipmentItem', 'SePackage', 'SeAddress', 'SeDocument',
                                   'SePackageInquiryItem'));
/**
 * SeCurlGetShipmentDetailsResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of GetShipmentDetails cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetShipmentDetailsResponse.v.0.1
 */
class SeCurlGetShipmentDetailsResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param headers
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    $json = json_decode($body);

    try
    {
      $dto = new SeShipmentItem();

      if (isset($json->shipment->Id))
      {
        $dto->setId($json->shipment->Id);
      }

      if (isset($json->shipment->CourierId))
      {
        $dto->setCourierId($json->shipment->CourierId);
      }

      if (isset($json->shipment->RateId))
      {
        $dto->setRateId($json->shipment->RateId);
      }

      if (isset($json->shipment->PickupAt))
      {
        $dto->setPickupAt($json->shipment->PickupAt);
      }

      if (isset($json->shipment->PackageCount))
      {
        $dto->setPackageCount($json->shipment->PackageCount);
      }

      if (isset($json->shipment->Eta))
      {
        $dto->setEta($json->shipment->Eta);
      }

      if (isset($json->shipment->TrackingNumber))
      {
        $dto->setTrackingNumber($json->shipment->TrackingNumber);
      }

      if (isset($json->shipment->Status))
      {
        $dto->setStatus($json->shipment->Status);
      }

      if (isset($json->shipment->TotalAmount))
      {
        $dto->setTotalAmount($json->shipment->TotalAmount);
      }

      if (isset($json->shipment->Currency))
      {
        $dto->setCurrency($json->shipment->Currency);
      }

      if (isset($json->shipment->UserId))
      {
        $dto->setUserId($json->shipment->UserId);
      }

      if (isset($json->shipment->CourierName))
      {
        $dto->setCourierName($json->shipment->CourierName);
      }

      if (isset($json->shipment->CurrencySymbol))
      {
        $dto->setCurrencySymbol($json->shipment->CurrencySymbol);
      }

      if (isset($json->shipment->IsDropOff))
      {
        $dto->setIsDropOff($json->shipment->IsDropOff);
      }

      if (isset($json->shipment->Packages))
      {
        if (is_array($json->shipment->Packages))
        {
          foreach($json->shipment->Packages as $pack)
          {
            $pac = new SePackage($pack->Id, $pack->ValueAmount, $pack->ValueCurrency,
                                 null, null, null, null, null, null, false, $pack->IsMetric,
                                 '', null, null, null, array());

            if (isset($pack->Weight))
            {
              $pac->setWeight($pack->Weight);
            }

            if (isset($pack->Length))
            {
              $pac->setLength($pack->Length);
            }

            if (isset($pack->Width))
            {
              $pac->setWidth($pack->Width);
            }

            if (isset($pack->Height))
            {
              $pac->setHeight($pack->Height);
            }

            if (isset($pack->PredefinedSizeId))
            {
              $pac->setPredefinedSizeId($pack->PredefinedSizeId);
            }

            if (isset($pack->FlatRatePackageId))
            {
              $pac->setFlatRatePackageIdX($pack->FlatRatePackageId);
            }

            if (isset($pack->ContainsRestrictedItems))
            {
              $pac->setContainsRestrictedItems($pack->ContainsRestrictedItems);
            }

            if (isset($pack->PackageType))
            {
              $pac->setPackageType($pack->PackageType);
            }

            if (isset($pack->AdditionalInsuranceAmount))
            {
              $pac->setAdditionalInsuranceAmount($pack->AdditionalInsuranceAmount);
            }

            if (isset($pack->AdditionalInsuranceCurrency))
            {
              $pac->setAdditionalInsuranceCurrency($pack->AdditionalInsuranceCurrency);
            }

            if (isset($pack->HsCodes))
            {
              $pac->setHsCodes($pack->HsCodes);
            }

            if (isset($pack->Items))
            {
              if (is_array($pack->Items))
              {
                foreach($pack->Items as $it)
                {
                  $inqItem = new SePackageInquiryItem($it->Description, $it->SerialNo, $it->HsCode,
                                                      $it->UnitValue, $it->Currency,
                                                      null, $it->Quantity, $it->UnitOfMeasure,
                                                      '', '',
                                                      '');

                  if (isset($it->Comments))
                  {
                    $inqItem->setComments($it->Comments);
                  }

                  $pac->addItem($inqItem);
                }
              }
            }

            $dto->addPackage($pac);
          }
        }
      }

      if (isset($json->shipment->Address->Collection))
      {
        $addr1 = new SeAddress();

        $jsonAddr = $json->shipment->Address->Collection;

        $addr1->setId($jsonAddr->Id);
        $addr1->setFirstName($jsonAddr->FirstName);
        $addr1->setLastName($jsonAddr->LastName);
        $addr1->setEmailAddress($jsonAddr->EmailAddress);
        $addr1->setCompanyName($jsonAddr->CompanyName);
        $addr1->setLine1($jsonAddr->Line1);
        $addr1->setLine2($jsonAddr->Line2);
        $addr1->setPostalCode($jsonAddr->PostalCode);
        $addr1->setCity($jsonAddr->City);
        $addr1->setCountry($jsonAddr->Country);

        $dto->addAddress('Collection', $addr1);
      }

      if (isset($json->shipment->Address->Destination))
      {
        $addr1 = new SeAddress();

        $jsonAddr = $json->shipment->Address->Destination;

        $addr1->setId($jsonAddr->Id);
        $addr1->setFirstName($jsonAddr->FirstName);
        $addr1->setLastName($jsonAddr->LastName);
        $addr1->setEmailAddress($jsonAddr->EmailAddress);
        $addr1->setCompanyName($jsonAddr->CompanyName);
        $addr1->setLine1($jsonAddr->Line1);
        $addr1->setLine2($jsonAddr->Line2);
        $addr1->setPostalCode($jsonAddr->PostalCode);
        $addr1->setCity($jsonAddr->City);
        $addr1->setCountry($jsonAddr->Country);

        $dto->addAddress('Destination', $addr1);
      }

      if (isset($json->shipment->Documents))
      {
        foreach($json->shipment->Documents as $doc)
        {
          $doc1 = new SeDocument($doc->Id, $doc->Name, $doc->Type, $doc->MimeType, $doc->Url, $doc->FileName);

          $dto->addDocument($doc1);
        }
      }

      $this->setResult($dto);
    }
    catch (Exception $e)
    {
      $this->setSuccessful(false);

      throw new SeServerException($e->getMessage());
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getShipmentDetails()
  {
    return $this->getResult();
  }
}
?>