<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SeDocument'));
/**
 * SeCurlGetDocumentsResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of GetDocuments cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetDocumentsResponse.v.0.1
 */
class SeCurlGetDocumentsResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param headers
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    if ($this->getContentType() == 'application/pdf')
    {
      try
      {
        $this->setResult($this->getBody());
      }
      catch (Exception $e)
      {
        $this->setSuccessful(false);

        throw new SeServerException($e->getMessage());
      }
    }
    else
    {
      $json = json_decode($body);

      $result = array();

      try
      {
        foreach($json->documents as $key=>$value)
        {
          $result[$key] = array();

          foreach($value as $item)
          {
            $document = new SeDocument($item->Id, $item->Name, $item->Type, $item->MimeType, $item->Url,
                                       $item->FileName);

            array_push($result[$key], $document);
          }
        }

        $this->setResult($result);
      }
      catch (Exception $e)
      {
        $this->setSuccessful(false);

        throw new SeServerException($e->getMessage());
      }
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getDocuments()
  {
    return $this->getResult();
  }
}
?>