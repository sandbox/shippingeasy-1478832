<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SeAddress'));
/**
 * SeCurlgetAddressDetailsResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of GetAddressDetails cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetAddressDetailsResponse.v.0.1
 */
class SeCurlGetAddressDetailsResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param headers
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    $json = json_decode($body);

    try
    {
      $dto = new SeAddress();

      $dto->setId($json->address->Id);
      $dto->setName($json->address->Name);
      $dto->setDescription($json->address->Description);
      $dto->setIsDefault($json->address->IsDefault);
      $dto->setType($json->address->Type);
      $dto->setFirstName($json->address->FirstName);
      $dto->setLastName($json->address->LastName);
      $dto->setCompanyName($json->address->CompanyName);
      $dto->setLine1($json->address->Line1);
      $dto->setLine2($json->address->Line2);
      $dto->setPostalCode($json->address->PostalCode);
      $dto->setCity($json->address->City);
      $dto->setCountry($json->address->Country);
      $dto->setCountryDivisionId($json->address->CountryDivisionId);
      $dto->setTelephoneAreaCode($json->address->TelephoneAreaCode);
      $dto->setTelephoneNumber($json->address->TelephoneNumber);
      $dto->setEmailAddress($json->address->EmailAddress);
      $dto->setCreatedAt($json->address->CreatedAt);
      $dto->setAddressType($json->address->AddressType);
      $dto->setCountryDivision($json->address->CountryDivision);

      $this->setResult($dto);
    }
    catch (Exception $e)
    {
      $this->setSuccessful(false);

      throw new SeServerException($e->getMessage());
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getAddress()
  {
    return $this->getResult();
  }
}
?>