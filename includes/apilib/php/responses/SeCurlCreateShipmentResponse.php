<?php
SeServiceUtils::checkInclude(array('SeCurlResponse', 'SeShipment', 'SeAdditionalInsurance'));
/**
 * SeCurlCreateShipmentResponse.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlResponse
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of CreateShipment cURL response. Extends SeCurlResponse class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlCreateShipmentResponse.v.0.1
 */
class SeCurlCreateShipmentResponse extends SeCurlResponse
{
  /**
   * constructor. executes parent constructor too.
   *
   * @param
   *
   * @return
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * function for response body parsing. it parses json returned from api and sets parsed objects as response result.
   *
   * @param body
   *
   * @return
   */
  public function parseBody($body)
  {
    $json = json_decode($body);

    // initialize result
    $result = array();

    try
    {
      foreach ($json->shipments as $key => $value)
      {
        $dto = new SeShipment();

        $dto->setId($value->id);
        $dto->setUrl($value->url);

        if (isset($value->additionalInsurance))
        {
          $additionalInsurance = array();

          foreach($value->additionalInsurance as $item)
          {
            $additional = new SeAdditionalInsurance();

            $additional->setShipmentPackageId($item->ShipmentPackageId);
            $additional->setPackageName($item->PackageName);
            $additional->setRecordedShipmentId($item->RecordedShipmentId);

            array_push($additionalInsurance, $additional);
          }

          $dto->setAdditionalInsurance($additionalInsurance);
        }

        $result[$key] = $dto;
      }

      $this->setResult($result);
    }
    catch (Exception $e)
    {
      throw new SeServerException('Error during parsing CreateShipment response.', null, $e);
    }
  }

  /**
   * This function returns response result.
   *
   * @param
   *
   * @return result
   */
  public function getCreateShipment()
  {
    return $this->getResult();
  }
}
?>