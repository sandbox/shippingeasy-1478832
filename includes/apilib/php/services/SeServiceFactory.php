<?php
SeServiceUtils::checkInclude(array('SeServiceType', 'SeServerException'));
/**
 * SeServiceFactory.php.
 *
 * PHP Version 5.3.1
 *
 * @category  Request
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation to determine which service should be instantiated and returned.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeServiceFactory.v.0.1
 */
class SeServiceFactory
{
  public static function create($serviceType)
  {
    // class name concatenation
    $className = 'Se'.$serviceType.'Service';

    try
    {
      SeServiceUtils::checkInclude($className);

      return new $className();
    }
    catch (Exception $e)
    {
      throw new SeServerException($e->getMessage() . ' (' . $className . ').');
    }
  }
}
?>