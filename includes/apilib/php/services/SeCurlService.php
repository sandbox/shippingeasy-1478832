<?php
SeServiceUtils::checkInclude('SeWebService');
/**
 * SeCurlService.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeWebService
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents base class for curl service. It extends SeWebService class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlRequest.v.0.1
 */
class SeCurlService extends SeWebService
{
  protected function doExecute(&$request, &$response)
  {
    // initializing curl
    $ch = curl_init();

    // header control / should it be included or not
    if ($request->getIncludeHeader())
    {
      curl_setopt($ch, CURLOPT_HEADER, 1);
    }
    else
    {
      curl_setopt($ch, CURLOPT_HEADER, 0);
    }

    if (count($request->getHeaders()) > 0)
    {
      curl_setopt($ch, CURLOPT_HTTPHEADER, $request->generateHeaders());
    }

    // http method control. post, get, put, delete.
    if ($request->getHttpMethod() == 'POST')
    {
      curl_setopt($ch, CURLOPT_POST, 1);

      // set query string
      if (!is_null($request->getBody()))
      {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request->getBody());
      }
      else
      {
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request->getParameters(), '', '&'));
      }

      curl_setopt($ch, CURLOPT_URL, $request->getUrl());
    }

    if ($request->getHttpMethod() == 'GET')
    {
      curl_setopt($ch, CURLOPT_POST, 0);
      curl_setopt($ch, CURLOPT_HTTPGET, 1);
      curl_setopt($ch, CURLOPT_URL, $request->getUrl());
    }

    if ($request->getHttpMethod() == 'PUT')
    {
      //curl_setopt($ch, CURLOPT_HTTPGET, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      //curl_setopt($ch, CURLOPT_PUT, 0);

      $data = http_build_query($request->getParameters(), '', '&');
      $data = $data.'&method=PUT';

      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_URL, $request->getUrl());
    }

    if ($request->getHttpMethod() == 'DELETE')
    {
      curl_setopt($ch, CURLOPT_POST, 0);
      curl_setopt($ch, CURLOPT_HTTPGET, 0);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
      curl_setopt($ch, CURLOPT_URL, $request->getUrl());
    }

    // synchronous request, response to be returned in curl_exec()
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    // do not allow request to last more than X seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, $request->getMaxExecutionTime());

  /*
    if($this->useProxy)
    {
      curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
      curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
    }
  */
    // execute curl
    $body = curl_exec($ch);

    // check if error occured
    if ($body === false)
    {
      $errno = curl_errno($ch);

      if ($errno == 28)
      {
        throw new SeRequestException(28, 'Curl operation timed out. ' . curl_error($ch));
      }
    }

    // get response headers
    $headers = curl_getinfo($ch);

    // close curl
    curl_close($ch);

    // initialize response
    $response->init($headers, $body);

    // parse body only if response is success
    if ($response->getSuccessful())
    {
      $response->parseBody($body);
    }
  }

  protected function preExecute(&$request, &$response)
  {
    return true;
  }

  protected function postExecute(&$request, &$response)
  {

  }
}
?>