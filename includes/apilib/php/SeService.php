<?php
// define global constants
define('LOGGER_ENABLED', false); // set if logger is enabled
define('LOGGER_ABSOLUTE_FILEPATH', '/tmp'); // in this folder log files will be saved
define('ORIGIN', 'drupal'); // set origin which is using api library

// include Utils dependencies
require_once 'base/SeServiceUtils.php';

// include other dependencies
SeServiceUtils::checkInclude(array(
  'SeServiceType',
  'SeEnvironment',
  'SeServiceLoggerType',
  'SeResourceName',
  'SeServerException',
  'SeRequestException',
  'SeServiceFactory',
  'SeRequestFactory',
  'SeResponseFactory',
  'SeLoggerFactory',
  'SeServiceExecutor',
  'SeBaseBuilder',
  'SeGetRateBuilder',
  'SeAddressBuilder',
  'SeCreateShipmentBuilder',
  'SePackageItemBuilder',
  'SePackageBuilder',
  'SePackageInquiryBuilder',
  'SePackageInquiryItem',
  'SePackageInquiry',
  'SeRateInquiryAddress',
  'SeRateInquiry',
  'SeRateInquiryFlatRatePackageIdX',
  'SeRateInquiryPackageX',
  'SeRateInquiryPredefinedSizeIdX',
  'SePackage',
  'SeAddress',
  'SeTrackingInfo',
  'SeCourier',
  'SeCreateShipmentInquiry',
  'SeDocument',
  'SeHsCode',
  'SePackageItem',
  'SeRate',
  'SeShipment',
  'SeAdditionalInsurance',
  'SeShipmentItem',
  'SePickup',
  'SeServiceTypeItem',
  'SeExchangeRateItem'
  )
);

/**
 * SeService.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeService
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents starting point for communication.
 * This file is the only one which should be included as a library.
 * At the top of this file there are some defined parameters which should be altered to match your needs.
 * This class exposes all available requests and their constructors to the end user.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeService.v.0.1
 */
class SeService
{
  /**
   * Service type
   *
   * @var SeServiceType
   */
  private $serviceType;

  /**
   * Service instance
   *
   * @var SeWebService
   */
  private $service;

  /**
   * SeServiceExecutor instance
   *
   * @var SeServiceExecutor
   */
  private $executor;

  /**
   * SeServiceLogger instance
   *
   * @var SeServiceLogger
   */
  private $logger;

  /**
   * Gets ServiceExecutor
   *
   * @param
   *
   * @return SeServiceExecutor
   */
  private function getExecutor()
  {
    return $this->executor;
  }

  /**
   * Gets Service type
   *
   * @param
   *
   * @return SeServiceType
   */
  private function getServiceType()
  {
    return $this->serviceType;
  }

  /**
   * Gets Service instance
   *
   * @param
   *
   * @return SeWebService
   */
  private function getService()
  {
    return $this->service;
  }

  /**
   * Gets Logger instance
   *
   * @param
   *
   * @return SeServiceLogger
   */
  private function getLogger()
  {
    return $this->logger;
  }

  /**
   * constructor.
   *
   * @param $apiKey, $environment, $serviceType, $loggerType
   *
   * @return
   */
  public function __construct($apiKey,
                              $environment=SeEnvironment::Test,
                              $testMode=false,
                              $serviceType=SeServiceType::Curl,
                              $loggerType=SeServiceLoggerType::File)
  {
    // root url for the service
    SeServiceUtils::determineEntryPoint($environment);

    // apiKey for communication
    define("API_KEY", $apiKey);

    if ($testMode)
    {
      define("TEST_MODE", true);
    }
    else
    {
      define("TEST_MODE", false);
    }

    $this->serviceType = $serviceType;
    $this->service = SeServiceFactory::create($serviceType);
    $this->logger = SeLoggerFactory::create($loggerType);
    $this->executor = new SeServiceExecutor($this->getService(), $this->getLogger());
  }

  /*
   * Returns list of HS codes that match the submitted text.
   */
  public function matchHsCode($keyword, $suggested=null)
  {
    $argsNames = 'keyword,suggested';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::MatchHsCode, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::MatchHsCode, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getHsCodes();
  }

  /*
   * Returns list of courier services that can be used for delivery of specified shipment.
   */
  public function getRate(SeRateInquiry $seRateInquiry)
  {
    $args = SeServiceUtils::parseSeRateInquiry($seRateInquiry);

    $request = SeRequestFactory::create(SeResourceName::GetRate, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::GetRate, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getRates();
  }

  /*
   * Returns courier with the matching ID
   */
  public function getCourier($id)
  {
    $argsNames = 'Id';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::GetCourier, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::GetCourier, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getCourier();
  }

  /*
   * Creates new Address.
   */
  public function addAddress(SeAddress $seAddress)
  {
    $args = SeServiceUtils::parseSeAddress($seAddress);

    $request = SeRequestFactory::create(SeResourceName::AddAddress, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::AddAddress, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getAddressId();
  }

  /*
   * Updates Address.
   */
  public function updateAddress(SeAddress $seAddress, $id)
  {
    $args = SeServiceUtils::parseSeAddress($seAddress);
    $args['Id'] = $id;

    $request = SeRequestFactory::create(SeResourceName::UpdateAddress, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::UpdateAddress, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getAddressId();
  }

  /*
   * Deletes Address.
   */
  public function deleteAddress($id)
  {
    $argsNames = 'Id';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::DeleteAddress, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::DeleteAddress, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getAddressId();
  }

  /*
   * Returns all addresses entered by the user.
   */
  public function listAddresses()
  {
    $args = array();

    $request = SeRequestFactory::create(SeResourceName::ListAddresses, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::ListAddresses, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getAddresses();
  }

  /*
   * Returns single address by ID.
   */
  public function getAddressDetails($addressId)
  {
    $argsNames = 'Id';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::GetAddressDetails, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::GetAddressDetails, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getAddress();
  }

  /*
   * Returns tracking information based on tracking number.
   */
  public function getTrackingInfo($trackingNumber)
  {
    $argsNames = 'trackingNumber';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::GetTrackingInfo, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::GetTrackingInfo, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getTrackingInfo();
  }

  /*
   * Returns URIs of all shipment documents for the given shipment(s).
   */
  public function getDocuments(array $shipmentId, $merged=false)
  {
    $argsNames = 'shipmentId,merged';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);
    $args['shipmentId'] = implode($shipmentId, ',');

    $request = SeRequestFactory::create(SeResourceName::GetDocuments, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::GetDocuments, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getDocuments();
  }

  /*
   * Returns all packages entered by the user.
   */
  public function listPackages()
  {
    $args = array();

    $request = SeRequestFactory::create(SeResourceName::ListPackages, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::ListPackages, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getPackages();
  }

  /*
   * Returns single package by ID.
   */
  public function getPackageDetails($packageId)
  {
    $argsNames = 'Id';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::GetPackageDetails, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::GetPackageDetails, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getPackage();
  }

  /*
   * Deletes Package.
   */
  public function deletePackage($id)
  {
    $argsNames = 'Id';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::DeletePackage, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::DeletePackage, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getPackageId();
  }

  /*
   * Updates Package.
   */
  public function updatePackage(SePackageItem $sePackageItem, $id)
  {
    $args = SeServiceUtils::parseSePackageItem($sePackageItem);
    $args['Id'] = $id;

    $request = SeRequestFactory::create(SeResourceName::UpdatePackage, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::UpdatePackage, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getPackageId();
  }

  /*
   * Creates new Package.
   */
  public function addPackage(SePackageItem $sePackageItem)
  {
    $args = SeServiceUtils::parseSePackageItem($sePackageItem);

    $request = SeRequestFactory::create(SeResourceName::AddPackage, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::AddPackage, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getPackageId();
  }

  /**
   * Creates new shipment(s).
   *
   * @param array SeCreateShipmentInquiry $seCreateShipmentInquiry
   *
   * @return $response->getCreateShipment()
   */
  public function createShipment(array $seCreateShipmentInquiry)
  {
    $args = SeServiceUtils::parseSeCreateShipmentInquiry($seCreateShipmentInquiry);

    $request = SeRequestFactory::create(SeResourceName::CreateShipment, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::CreateShipment, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getCreateShipment();
  }

  /*
   * Returns all couriers.
   */
  public function listCouriers()
  {
    $args = array();

    $request = SeRequestFactory::create(SeResourceName::ListCouriers, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::ListCouriers, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getCouriers();
  }

  /*
   * Returns single shipment by ID.
   */
  public function getShipmentDetails($shipmentId)
  {
    $argsNames = 'Id';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::GetShipmentDetails, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::GetShipmentDetails, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getShipmentDetails();
  }

  /*
   * Changes status of specified shipment(s) ID's into pickup.
   */
  public function createPickup(array $shipmentIds)
  {
    $argsNames = 'shipments';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);
    $args['shipments'] = implode($shipmentIds, '-');

    $request = SeRequestFactory::create(SeResourceName::CreatePickup, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::CreatePickup, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getCreatePickup();
  }

  /*
   * Returns service types.
   */
  public function listServiceTypes($courierId=null, $perPage=null, $page=null)
  {
    $argsNames = 'CourierId,perPage,page';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::ListServiceTypes, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::ListServiceTypes, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getServiceTypes();
  }

  /*
   * Returns list of exchange rates.
   */
  public function listExchangeRates($latest=null, $perPage=null, $page=null)
  {
    $argsNames = 'Latest,perPage,page';
    $args = SeServiceUtils::getMethodParameters(func_get_args(), $argsNames);

    $request = SeRequestFactory::create(SeResourceName::ListExchangeRates, $this->getServiceType(), $args);
    $response = SeResponseFactory::create(SeResourceName::ListExchangeRates, $this->getServiceType());

    $this->getExecutor()->execute($request, $response);

    return $response->getExchangeRates();
  }
}
?>