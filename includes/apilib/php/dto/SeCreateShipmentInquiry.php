<?php
class SeCreateShipmentInquiry
{
  /**
   * This is an unique indentificator for single shipment
   *
   * @var string
   */
  protected $identificator;

  /**
   * $collectionAddress
   *
   * @var SeAddress
   */
  protected $collectionAddress;

  /**
   * $destinationAddress
   *
   * @var SeAddress
   */
  protected $destinationAddress;

  /**
    * $currency
    *
    * @var string
    */
  protected $currency;

  /**
    * MessageToBuyer
    *
    * @var string
    */
  protected $messageToBuyer;

  /**
    * PickupAt
    *
    * @var string
    */
  protected $pickupAt;

  /**
    * PickupCloseAt
    *
    * @var string
    */
  protected $pickupCloseAt;

  /**
    * RateId
    *
    * @var numeric
    */
  protected $rateId;

  /**
    * Courier
    *
    * @var string
    */
  protected $courier;

  /**
    * ServiceType
    *
    * @var string
    */
  protected $serviceType;

  /**
    * PickupDate
    *
    * @var string
    */
  protected $pickupDate;

  /**
    * DropOff
    *
    * @var boolean
    */
  protected $dropOff;

  /**
    * Est
    *
    * @var string
    */
  protected $est;

  /**
    * HumanEst
    *
    * @var string
    */
  protected $humanEst;

  /**
    * Packages
    *
    * @var array SeShipmentInquiryPackage
    */
  protected $packages = array();

  /**
    * Origin
    *
    * @var string
    */
  protected $origin;

  /**
    * InvoiceNumber
    *
    * @var string
    */
  protected $invoiceNumber;

  /**
    * InvoiceComment
    *
    * @var string
    */
  protected $invoiceComment;

  /**
   * Sets object's identificator
   *
   * @param $identificator
   *
   * @return
   */
  public function setIdentificator($identificator)
  {
    $this->identificator = $identificator;
  }

  /**
   * Returns object's identificator
   *
   * @param
   *
   * @return object's identificator
   */
  public function getIdentificator()
  {
    return $this->identificator;
  }

  /**
   * Sets object's collectionAddress
   *
   * @param SeAddress $collectionAddress
   *
   * @return
   */
  public function setCollectionAddress(SeAddress $collectionAddress)
  {
    $this->collectionAddress = $collectionAddress;
  }

  /**
   * Returns object's collectionAddress
   *
   * @param
   *
   * @return object's collectionAddress
   */
  public function getCollectionAddress()
  {
    return $this->collectionAddress;
  }

  /**
   * Sets object's $destinationAddress
   *
   * @param SeAddress $destinationAddress
   *
   * @return
   */
  public function setDestinationAddress(SeAddress $destinationAddress)
  {
    $this->destinationAddress = $destinationAddress;
  }

  /**
   * Returns object's destinationAddress
   *
   * @param
   *
   * @return object's destinationAddress
   */
  public function getDestinationAddress()
  {
    return $this->destinationAddress;
  }

  /**
   * Sets object's $packages
   *
   * @param $packages
   *
   * @return
   */
  public function setPackages($packages)
  {
    $this->packages = $packages;
  }

  /**
   * Returns object's packages
   *
   * @param
   *
   * @return object's packages
   */
  public function getPackages()
  {
    return $this->packages;
  }

  /**
   * Sets object's $currency
   *
   * @param $currency
   *
   * @return
   */
  public function setCurrency($currency)
  {
    $this->currency = $currency;
  }

  /**
   * Returns object's currency
   *
   * @param
   *
   * @return object's currency
   */
  public function getCurrency()
  {
    return $this->currency;
  }

  /**
   * Sets object's $messageToBuyer
   *
   * @param $messageToBuyer
   *
   * @return
   */
  public function setMessageToBuyer($messageToBuyer)
  {
    $this->messageToBuyer = $messageToBuyer;
  }

  /**
   * Returns object's messageToBuyer
   *
   * @param
   *
   * @return object's messageToBuyer
   */
  public function getMessageToBuyer()
  {
    return $this->messageToBuyer;
  }

  /**
   * Sets object's $pickupDate
   *
   * @param $pickupDate
   *
   * @return
   */
  public function setPickupDate($pickupDate)
  {
    $this->pickupDate = $pickupDate;
  }

  /**
   * Returns object's pickupDate
   *
   * @param
   *
   * @return object's pickupDate
   */
  public function getPickupDate()
  {
    return $this->pickupDate;
  }

  /**
   * Sets object's $pickupAt
   *
   * @param $pickupAt
   *
   * @return
   */
  public function setPickupAt($pickupAt)
  {
    $this->pickupAt = $pickupAt;
  }

  /**
   * Returns object's pickupAt
   *
   * @param
   *
   * @return object's pickupAt
   */
  public function getPickupAt()
  {
    return $this->pickupAt;
  }

  /**
   * Sets object's $pickupCloseAt
   *
   * @param $pickupCloseAt
   *
   * @return
   */
  public function setPickupCloseAt($pickupCloseAt)
  {
    $this->pickupCloseAt = $pickupCloseAt;
  }

  /**
   * Returns object's pickupCloseAt
   *
   * @param
   *
   * @return object's pickupCloseAt
   */
  public function getPickupCloseAt()
  {
    return $this->pickupCloseAt;
  }

  /**
   * Sets object's $rateId
   *
   * @param $rateId
   *
   * @return
   */
  public function setRateId($rateId)
  {
    $this->rateId = $rateId;
  }

  /**
   * Returns object's rateId
   *
   * @param
   *
   * @return object's rateId
   */
  public function getRateId()
  {
    return $this->rateId;
  }

  /**
   * Sets object's $courier
   *
   * @param $courier
   *
   * @return
   */
  public function setCourier($courier)
  {
    $this->courier = $courier;
  }

  /**
   * Returns object's courier
   *
   * @param
   *
   * @return object's courier
   */
  public function getCourier()
  {
    return $this->courier;
  }

  /**
   * Sets object's $serviceType
   *
   * @param $serviceType
   *
   * @return
   */
  public function setServiceType($serviceType)
  {
    $this->serviceType = $serviceType;
  }

  /**
   * Returns object's serviceType
   *
   * @param
   *
   * @return object's serviceType
   */
  public function getServiceType()
  {
    return $this->serviceType;
  }

  /**
   * Sets object's $dropOff
   *
   * @param $dropOff
   *
   * @return
   */
  public function setDropOff($dropOff)
  {
    $this->dropOff = $dropOff;
  }

  /**
   * Returns object's dropOff
   *
   * @param
   *
   * @return object's dropOff
   */
  public function getDropOff()
  {
    return $this->dropOff;
  }

  /**
   * Sets object's $est
   *
   * @param $est
   *
   * @return
   */
  public function setEst($est)
  {
    $this->est = $est;
  }

  /**
   * Returns object's est
   *
   * @param
   *
   * @return object's est
   */
  public function getEst()
  {
    return $this->est;
  }

  /**
   * Sets object's $humanEst
   *
   * @param $humanEst
   *
   * @return
   */
  public function setHumanEst($humanEst)
  {
    $this->humanEst = $humanEst;
  }

  /**
   * Returns object's humanEst
   *
   * @param
   *
   * @return object's humanEst
   */
  public function getHumanEst()
  {
    return $this->humanEst;
  }

  /**
   * Sets object's $origin
   *
   * @param $origin
   *
   * @return
   */
  public function setOrigin($origin)
  {
    $this->origin = $origin;
  }

  /**
   * Returns object's origin
   *
   * @param
   *
   * @return object's origin
   */
  public function getOrigin()
  {
    return $this->origin;
  }

  /**
   * Sets object's $invoiceNumber
   *
   * @param $invoiceNumber
   *
   * @return
   */
  public function setInvoiceNumber($invoiceNumber)
  {
    $this->invoiceNumber = $invoiceNumber;
  }

  /**
   * Returns object's invoiceNumber
   *
   * @param
   *
   * @return object's invoiceNumber
   */
  public function getInvoiceNumber()
  {
    return $this->invoiceNumber;
  }

  /**
   * Sets object's $invoiceComment
   *
   * @param $invoiceComment
   *
   * @return
   */
  public function setInvoiceComment($invoiceComment)
  {
    $this->invoiceComment = $invoiceComment;
  }

  /**
   * Returns object's invoiceComment
   *
   * @param
   *
   * @return object's invoiceComment
   */
  public function getInvoiceComment()
  {
    return $this->invoiceComment;
  }

  /**
   * Object's constructor.
   */
  public function __construct($identificator, $collectionAddress, $destinationAddress, $currency,
                              $messageToBuyer=null, $pickupAt, $pickupCloseAt,
                              $rateId, $courier, $serviceType, $pickupDate,
                              $dropOff=null, $est, $humanEst, $packages,
                              $origin, $invoiceNumber=null, $invoiceComment=null)
  {
    $this->setIdentificator($identificator);
    $this->setCollectionAddress($collectionAddress);
    $this->setDestinationAddress($destinationAddress);
    $this->setCurrency($currency);
    $this->setMessageToBuyer($messageToBuyer);
    $this->setPickupAt($pickupAt);
    $this->setPickupCloseAt($pickupCloseAt);
    $this->setRateId($rateId);
    $this->setCourier($courier);
    $this->setServiceType($serviceType);
    $this->setPickupDate($pickupDate);
    $this->setDropOff($dropOff);
    $this->setEst($est);
    $this->setHumanEst($humanEst);
    $this->setPackages($packages);
    $this->setOrigin($origin);
    $this->setInvoiceNumber($invoiceNumber);
    $this->setInvoiceComment($invoiceComment);
  }
}
?>