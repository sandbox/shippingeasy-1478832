<?php
class SeRateInquiryAddress
{
  /**
   * $country
   *
   * @var string
   */
  protected $country;

  /**
   * $city
   *
   * @var string
   */
  protected $city;

  /**
   * $zip
   *
   * @var string
   */
  protected $zip;

  /**
   * Sets object's $country
   *
   * @param $country
   *
   * @return
   */
  public function setCountry($country)
  {
    $this->country = $country;
  }

  /**
   * Returns object's country
   *
   * @param
   *
   * @return object's country
   */
  public function getCountry()
  {
    return $this->country;
  }

  /**
   * Sets object's $city
   *
   * @param $city
   *
   * @return
   */
  public function setCity($city)
  {
    $this->city = $city;
  }

  /**
   * Returns object's city
   *
   * @param
   *
   * @return object's city
   */
  public function getCity()
  {
    return $this->city;
  }

  /**
   * Sets object's $zip
   *
   * @param $zip
   *
   * @return
   */
  public function setZip($zip)
  {
    $this->zip = $zip;
  }

  /**
   * Returns object's zip
   *
   * @param
   *
   * @return object's zip
   */
  public function getZip()
  {
    return $this->zip;
  }

  /**
   * Object's constructor.
   */
  public function __construct($country, $city, $zip)
  {
    $this->setCountry($country);
    $this->setCity($city);
    $this->setZip($zip);
  }
}
?>