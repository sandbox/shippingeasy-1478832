<?php
/**
 * SeCourier.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one Courier item
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCourier.v.0.1
 */
class SeCourier
{
  /**
   * Id of the returned item.
   *
   * @var integer
   */
  protected $Id;

  /**
   * Name of the courier.
   *
   * @var string
   */
  protected $Name;

  /**
   * Returns object's Id
   *
   * @param
   *
   * @return object's Id
   */
  public function getId()
  {
    return $this->Id;
  }

  /**
   * Sets object's Id
   *
   * @param $id
   *
   * @return
   */
  public function setId($Id)
  {
    $this->Id = $Id;
  }

  /**
   * Returns object's Name
   *
   * @param
   *
   * @return object's Name
   */
  public function getName()
  {
    return $this->Name;
  }

  /**
   * Sets object's Name
   *
   * @param $Name
   *
   * @return
   */
  public function setName($Name)
  {
    $this->Name = $Name;
  }

  /**
   * object's constructor
   *
   * @param $Id, $Name
   *
   * @return
   */
  public function __construct($Id, $Name)
  {
    $this->setId($Id);
    $this->setName($Name);
  }
}
?>