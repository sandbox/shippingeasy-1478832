<?php
class SeAddressInquiry
{
  /**
   * name
   *
   * @var string
   */
  protected $name;

  /**
   * description
   *
   * @var string
   */
  protected $description;

  /**
    * isDefault
    *
    * @var boolean
    */
  protected $isDefault;

  /**
    * addressType
    *
    * @var string
    */
  protected $addressType;

  /**
    * type
    *
    * @var string
    */
  protected $type;

  /**
    * firstName
    *
    * @var string
    */
  protected $firstName;

  /**
    * lastName
    *
    * @var string
    */
  protected $lastName;

  /**
    * companyName
    *
    * @var string
    */
  protected $companyName;

  /**
    * line1
    *
    * @var string
    */
  protected $line1;

  /**
    * line2
    *
    * @var string
    */
  protected $line2;

  /**
    * postalCode
    *
    * @var string
    */
  protected $postalCode;

  /**
    * city
    *
    * @var string
    */
  protected $city;

  /**
    * country
    *
    * @var string
    */
  protected $country;

  /**
    * telephoneAreaCode
    *
    * @var string
    */
  protected $telephoneAreaCode;

  /**
    * telephoneNumber
    *
    * @var string
    */
  protected $telephoneNumber;

  /**
    * emailAddress
    *
    * @var string
    */
  protected $emailAddress;

  /**
    * postalCode
    *
    * @var string
    */
  protected $countryDivision;

  /**
    * countryDivisionId
    *
    * @var string
    */
  protected $countryDivisionId;

  /**
   * Sets object's name
   *
   * @param $name
   *
   * @return
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Returns object's name
   *
   * @param
   *
   * @return object's name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Sets object's $description
   *
   * @param $description
   *
   * @return
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * Returns object's description
   *
   * @param
   *
   * @return object's description
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Sets object's isDefault
   *
   * @param $isDefault
   *
   * @return
   */
  public function setIsDefault($isDefault)
  {
    $this->isDefault = $isDefault;
  }

  /**
   * Returns object's isDefault
   *
   * @param
   *
   * @return object's isDefault
   */
  public function getIsDefault()
  {
    return $this->isDefault;
  }

  /**
   * Sets object's $addressType
   *
   * @param $addressType
   *
   * @return
   */
  public function setAddressType($addressType)
  {
    $this->addressType = $addressType;
  }

  /**
   * Returns object's $addressType
   *
   * @param
   *
   * @return object's $addressType
   */
  public function getAddressType()
  {
    return $this->addressType;
  }

  /**
   * Sets object's type
   *
   * @param $type
   *
   * @return
   */
  public function setType($type)
  {
    $this->type = $type;
  }

  /**
   * Returns object's $type
   *
   * @param
   *
   * @return object's $type
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Sets object's $firstName
   *
   * @param $firstName
   *
   * @return
   */
  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;
  }

  /**
   * Returns object's firstName
   *
   * @param
   *
   * @return object's firstName
   */
  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
   * Sets object's $lastName
   *
   * @param $lastName
   *
   * @return
   */
  public function setLastName($lastName)
  {
    $this->lastName = $lastName;
  }

  /**
   * Returns object's $lastName
   *
   * @param
   *
   * @return object's $lastName
   */
  public function getLastName()
  {
    return $this->lastName;
  }

  /**
   * Sets object's $companyName
   *
   * @param $companyName
   *
   * @return
   */
  public function setCompanyName($companyName)
  {
    $this->companyName = $companyName;
  }

  /**
   * Returns object's $companyName
   *
   * @param
   *
   * @return object's $companyName
   */
  public function getCompanyName()
  {
    return $this->companyName;
  }

  /**
   * Sets object's $line1
   *
   * @param $line1
   *
   * @return
   */
  public function setLine1($line1)
  {
    $this->line1 = $line1;
  }

  /**
   * Returns object's line1
   *
   * @param
   *
   * @return object's line1
   */
  public function getLine1()
  {
    return $this->line1;
  }

  /**
   * Sets object's $line2
   *
   * @param $line2
   *
   * @return
   */
  public function setLine2($line2)
  {
    $this->line2 = $line2;
  }

  /**
   * Returns object's line2
   *
   * @param
   *
   * @return object's line2
   */
  public function getLine2()
  {
    return $this->line2;
  }

  /**
   * Sets object's $postalCode
   *
   * @param $postalCode
   *
   * @return
   */
  public function setPostalCode($postalCode)
  {
    $this->postalCode = $postalCode;
  }

  /**
   * Returns object's postalCode
   *
   * @param
   *
   * @return object's postalCode
   */
  public function getPostalCode()
  {
    return $this->postalCode;
  }

  /**
   * Sets object's $city
   *
   * @param $city
   *
   * @return
   */
  public function setCity($city)
  {
    $this->city = $city;
  }

  /**
   * Returns object's $city
   *
   * @param
   *
   * @return object's $city
   */
  public function getCity()
  {
    return $this->city;
  }

  /**
   * Sets object's $country
   *
   * @param $country
   *
   * @return
   */
  public function setCountry($country)
  {
    $this->country = $country;
  }

  /**
   * Returns object's $country
   *
   * @param
   *
   * @return object's $country
   */
  public function getCountry()
  {
    return $this->country;
  }

  /**
   * Sets object's $telephoneAreaCode
   *
   * @param $telephoneAreaCode
   *
   * @return
   */
  public function setTelephoneAreaCode($telephoneAreaCode)
  {
    $this->telephoneAreaCode = $telephoneAreaCode;
  }

  /**
   * Returns object's $telephoneAreaCode
   *
   * @param
   *
   * @return object's $telephoneAreaCode
   */
  public function getTelephoneAreaCode()
  {
    return $this->telephoneAreaCode;
  }

  /**
   * Sets object's $telephoneNumber
   *
   * @param $telephoneNumber
   *
   * @return
   */
  public function setTelephoneNumber($telephoneNumber)
  {
    $this->telephoneNumber = $telephoneNumber;
  }

  /**
   * Returns object's telephoneNumber
   *
   * @param
   *
   * @return object's telephoneNumber
   */
  public function getTelephoneNumber()
  {
    return $this->telephoneNumber;
  }

  /**
   * Sets object's $emailAddress
   *
   * @param $emailAddress
   *
   * @return
   */
  public function setEmailAddress($emailAddress)
  {
    $this->emailAddress = $emailAddress;
  }

  /**
   * Returns object's $emailAddress
   *
   * @param
   *
   * @return object's $emailAddress
   */
  public function getEmailAddress()
  {
    return $this->emailAddress;
  }

  /**
   * Sets object's $countryDivision
   *
   * @param $countryDivision
   *
   * @return
   */
  public function setCountryDivision($countryDivision)
  {
    $this->countryDivision = $countryDivision;
  }

  /**
   * Returns object's countryDivision
   *
   * @param
   *
   * @return object's countryDivision
   */
  public function getCountryDivision()
  {
    return $this->countryDivision;
  }

  /**
   * Sets object's $countryDivisionId
   *
   * @param $countryDivisionId
   *
   * @return
   */
  public function setCountryDivisionId($countryDivisionId)
  {
    $this->countryDivisionId = $countryDivisionId;
  }

  /**
   * Returns object's countryDivisionId
   *
   * @param
   *
   * @return object's countryDivisionId
   */
  public function getCountryDivisionId()
  {
    return $this->countryDivisionId;
  }

  /**
   * Object's constructor.
   */
  public function __construct($name=null, $description=null, $isDefault=null,
                             $addressType=null, $type, $firstName=null,
                             $lastName=null, $companyName=null, $line1=null, $line2=null,
                             $postalCode=null, $city=null, $country=null,
                             $telephoneAreaCode=null, $telephoneNumber=null,
                             $emailAddress=null, $countryDivision=null, $countryDivisionId=null)
  {
    $this->setName($name);
    $this->setDescription($description);
    $this->setIsDefault($isDefault);
    $this->setAddressType($addressType);
    $this->setType($type);
    $this->setFirstName($firstName);
    $this->setLastName($lastName);
    $this->setCompanyName($companyName);
    $this->setLine1($line1);
    $this->setLine2($line2);
    $this->setPostalCode($postalCode);
    $this->setCity($city);
    $this->setCountry($country);
    $this->setTelephoneAreaCode($telephoneAreaCode);
    $this->setTelephoneNumber($telephoneNumber);
    $this->setEmailAddress($emailAddress);
    $this->setCountryDivision($countryDivision);
    $this->setCountryDivisionId($countryDivisionId);
  }
}
?>