<?php
class SeRateInquiryPackageX
{
  /**
   * $name
   *
   * @var string
   */
  protected $name;

  /**
   * $size
   *
   * @var string
   */
  protected $size;

  /**
   * Sets object's $name
   *
   * @param $name
   *
   * @return
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Returns object's name
   *
   * @param
   *
   * @return object's name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Sets object's $size
   *
   * @param $size
   *
   * @return
   */
  public function setSize($size)
  {
    $this->size = $size;
  }

  /**
   * Returns object's size
   *
   * @param
   *
   * @return object's size
   */
  public function getSize()
  {
    return $this->size;
  }

  /**
   * Object's constructor.
   */
  public function __construct($name, $size)
  {
    $this->setName($name);
    $this->setSize($size);
  }
}
?>