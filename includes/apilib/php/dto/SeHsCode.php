<?php
/**
 * SeHsCode.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one HsCode item
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeHsCode.v.0.1
 */
class SeHsCode
{
  /**
   * Id of the returned item.
   *
   * @var integer
   */
  protected $Id;

  /**
   * Category in which returned item belongs to.
   *
   * @var string
   */
  protected $Category;

  /**
   * Description of the returned item. This property is initialized only if description exist
   * in returned array to avoid notice php errors.
   *
   * @var string
   */
  protected $Description;

  /**
   * Returns object's Id
   *
   * @param
   *
   * @return object's Id
   */
  public function getId()
  {
    return $this->Id;
  }

  /**
   * Sets object's Id
   *
   * @param $id
   *
   * @return
   */
  public function setId($Id)
  {
    $this->Id = $Id;
  }

  /**
   * Returns object's Category
   *
   * @param
   *
   * @return object's Category
   */
  public function getCategory()
  {
    return $this->Category;
  }

  /**
   * Sets object's Category
   *
   * @param $Category
   *
   * @return
   */
  public function setCategory($Category)
  {
    $this->Category = $Category;
  }

  /**
   * Returns object's Description
   *
   * @param
   *
   * @return object's Description
   */
  public function getDescription()
  {
    return $this->Description;
  }

  /**
   * Sets object's Description
   *
   * @param $Description
   *
   * @return
   */
  public function setDescription($Description)
  {
    $this->Description = $Description;
  }

  /**
   * object's constructor
   *
   * @param $Id, $Category, $Description
   *
   * @return
   */
  public function __construct($Id, $Category=null, $Description=null)
  {
    $this->setId($Id);
    $this->setCategory($Category);
    $this->setDescription($Description);
  }
}
?>