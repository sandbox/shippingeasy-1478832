<?php
/**
 * SeShipment.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one item in request result array
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeShipment.v.0.1
 */
class SeShipment
{
  /**
   * Shipment id.
   *
   * @var integer
   */
  protected $id;

  /**
   * url of the shipment
   *
   * @var string
   */
  protected $url;

  /**
   * Additional insurance
   *
   * @var string
   */
  protected $additionalInsurance = array();

  /**
   * Returns object's Id
   *
   * @param
   *
   * @return object's Id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Sets object's id
   *
   * @param $id
   *
   * @return
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Returns object's Url
   *
   * @param
   *
   * @return object's url
   */
  public function getUrl()
  {
    return $this->url;
  }

  /**
   * Sets object's Url
   *
   * @param $url
   *
   * @return
   */
  public function setUrl($url)
  {
    $this->url = $url;
  }

  /**
   * Returns object's additionalInsurance
   *
   * @param
   *
   * @return object's additionalInsurance
   */
  public function getAdditionalInsurance()
  {
    return $this->additionalInsurance;
  }

  /**
   * Sets object's additionalInsurance
   *
   * @param $additionalInsurance
   *
   * @return
   */
  public function setAdditionalInsurance($additionalInsurance)
  {
    $this->additionalInsurance = $additionalInsurance;
  }

  /**
   * object's constructor
   *
   * @param $id, $url, $additionalInsurance
   *
   * @return
   */
  public function __construct($id=null, $url=null, $additionalInsurance=array())
  {
    $this->setId($id);
    $this->setUrl($url);
    $this->setAdditionalInsurance($additionalInsurance);
  }
}
?>