<?php
/**
 * SeTrackingInfo.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one TrackingInfo item
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeTrackingInfo.v.0.1
 */
class SeTrackingInfo
{
  /**
   * Timestamp of the returned item.
   *
   * @var date
   */
  protected $timestamp;

  /**
   * Description
   *
   * @var string
   */
  protected $description;

  /**
   * Location
   *
   * @var string
   */
  protected $location;

  /**
   * Returns object's timestamp
   *
   * @param
   *
   * @return object's timestamp
   */
  public function getTimestamp()
  {
    return $this->timestamp;
  }

  /**
   * Sets object's timestamp
   *
   * @param timestamp
   *
   * @return
   */
  public function setTimestamp($timestamp)
  {
    $this->timestamp = $timestamp;
  }

  /**
   * Returns object's location
   *
   * @param
   *
   * @return object's location
   */
  public function getLocation()
  {
    return $this->location;
  }

  /**
   * Sets object's location
   *
   * @param location
   *
   * @return
   */
  public function setLocation($location)
  {
    $this->location = $location;
  }

  /**
   * Returns object's description
   *
   * @param
   *
   * @return object's description
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Sets object's description
   *
   * @param $description
   *
   * @return
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * object's constructor
   *
   * @param $Id, $Category, $Description
   *
   * @return
   */
  public function __construct($timestamp, $description, $location)
  {
    $this->setTimestamp($timestamp);
    $this->setDescription($description);
    $this->setLocation($location);
  }
}
?>