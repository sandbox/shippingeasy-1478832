<?php
/**
 * SePackageItem.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one PackageItem
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SePackageItem.v.0.1
 */
class SePackageItem
{
  /**
   * Id of the package.
   *
   * @var numeric
   */
  protected $id;

  /**
   * Name of the package.
   *
   * @var string
   */
  protected $name;

  /**
   * Description of the package.
   *
   * @var string
   */
  protected $description;

  /**
   * Weight of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $weight;

  /**
   * Length of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $length;

  /**
   * Width of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $width;

  /**
   * Height of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $height;

  /**
   * Indicates if metric system of units is used.
   *
   * @var boolean
   */
  protected $isMetric;

  /**
   * Returns object's id
   *
   * @param
   *
   * @return object's id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Sets object's id
   *
   * @param $id
   *
   * @return
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Returns object's name
   *
   * @param
   *
   * @return object's name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Sets object's name
   *
   * @param $name
   *
   * @return
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Returns object's description
   *
   * @param
   *
   * @return object's description
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Sets object's description
   *
   * @param $description
   *
   * @return
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * Returns object's weight
   *
   * @param
   *
   * @return object's weight
   */
  public function getWeight()
  {
    return $this->weight;
  }

  /**
   * Sets object's weight
   *
   * @param $weight
   *
   * @return
   */
  public function setWeight($weight)
  {
    $this->weight = $weight;
  }

  /**
   * Returns object's length
   *
   * @param
   *
   * @return object's length
   */
  public function getLength()
  {
    return $this->length;
  }

  /**
   * Sets object's length
   *
   * @param $length
   *
   * @return
   */
  public function setLength($length)
  {
    $this->length = $length;
  }

  /**
   * Returns object's width
   *
   * @param
   *
   * @return object's width
   */
  public function getWidth()
  {
    return $this->width;
  }

  /**
   * Sets object's width
   *
   * @param $width
   *
   * @return
   */
  public function setWidth($width)
  {
    $this->width = $width;
  }

  /**
   * Returns object's height
   *
   * @param
   *
   * @return object's height
   */
  public function getHeight()
  {
    return $this->height;
  }

  /**
   * Sets object's height
   *
   * @param $height
   *
   * @return
   */
  public function setHeight($height)
  {
    $this->height = $height;
  }

  /**
   * Returns object's isMetric
   *
   * @param
   *
   * @return object's isMetric
   */
  public function getIsMetric()
  {
    return $this->isMetric;
  }

  /**
   * Sets object's isMetric
   *
   * @param $isMetric
   *
   * @return
   */
  public function setIsMetric($isMetric)
  {
    $this->isMetric = $isMetric;
  }

  /**
   * object's constructor
   *
   * @param $name, $description, $weight, $length, $width, $height,
   *        $isMetric
   *
   * @return
   */
  public function __construct($id=null, $name, $description=null, $weight, $length, $width,
                              $height, $isMetric)
  {
    $this->setId($id);
    $this->setName($name);
    $this->setDescription($description);
    $this->setWeight($weight);
    $this->setLength($length);
    $this->setWidth($width);
    $this->setHeight($height);
    $this->setIsMetric($isMetric);
  }
}
?>