<?php
/**
 * SePickup.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one Pickup
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SePickup.v.0.1
 */
class SePickup
{
  /**
   * Id of the pickup.
   *
   * @var numeric
   */
  protected $id;

  /**
   * Status of the pickup.
   *
   * @var string
   */
  protected $status;

  /**
   * Message of the pickup.
   *
   * @var string
   */
  protected $message;

  /**
   * TrackingNumber of the pickup.
   *
   * @var string
   */
  protected $trackingNumber;

  /**
   * DocumentsUri of the pickup.
   *
   * @var string
   */
  protected $documentsUri;

  /**
   * Returns object's id
   *
   * @param
   *
   * @return object's id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Sets object's id
   *
   * @param $id
   *
   * @return
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Returns object's status
   *
   * @param
   *
   * @return object's status
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Sets object's status
   *
   * @param $status
   *
   * @return
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * Returns object's message
   *
   * @param
   *
   * @return object's message
   */
  public function getMessage()
  {
    return $this->message;
  }

  /**
   * Sets object's message
   *
   * @param $message
   *
   * @return
   */
  public function setMessage($message)
  {
    $this->message = $message;
  }

  /**
   * Returns object's trackingNumber
   *
   * @param
   *
   * @return object's trackingNumber
   */
  public function getTrackingNumber()
  {
    return $this->trackingNumber;
  }

  /**
   * Sets object's trackingNumber
   *
   * @param $trackingNumber
   *
   * @return
   */
  public function setTrackingNumber($trackingNumber)
  {
    $this->trackingNumber = $trackingNumber;
  }

  /**
   * Returns object's documentsUri
   *
   * @param
   *
   * @return object's documentsUri
   */
  public function getDocumentsUri()
  {
    return $this->documentsUri;
  }

  /**
   * Sets object's documentsUri
   *
   * @param $documentsUri
   *
   * @return
   */
  public function setDocumentsUri($documentsUri)
  {
    $this->documentsUri = $documentsUri;
  }

  /**
   * object's constructor
   *
   * @param $id, $status, $message, $trackingNumber, $documentsUri
   *
   * @return
   */
  public function __construct($id=null, $status=null, $message=null, $trackingNumber=null, $documentsUri=null)
  {
    $this->setId($id);
    $this->setStatus($status);
    $this->setMessage($message);
    $this->setTrackingNumber($trackingNumber);
    $this->setDocumentsUri($documentsUri);
  }
}
?>