<?php
class SePackageInquiryItem
{
  /**
   * description
   *
   * @var string
   */
  protected $description;

  /**
   * serialNumber
   *
   * @var boolean
   */
  protected $serialNumber;

  /**
    * hsCode
    *
    * @var string
    */
  protected $hsCode;

  /**
    * valueAmount
    *
    * @var numeric
    */
  protected $valueAmount;

  /**
    * valueCurrency
    *
    * @var string
    */
  protected $valueCurrency;

  /**
    * comments
    *
    * @var string
    */
  protected $comments;

  /**
    * quantity
    *
    * @var numeric
    */
  protected $quantity;

  /**
    * unitType
    *
    * @var string
    */
  protected $unitType;

  /**
    * manufactureCountry
    *
    * @var string
    */
  protected $manufactureCountry;

  /**
    * originalId
    *
    * @var numeric
    */
  protected $originalId;

  /**
    * originalTransactionId
    *
    * @var numeric
    */
  protected $originalTransactionId;

  /**
   * Sets object's $description
   *
   * @param $description
   *
   * @return
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * Returns object's description
   *
   * @param
   *
   * @return object's description
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Sets object's $serialNumber
   *
   * @param $serialNumber
   *
   * @return
   */
  public function setSerialNumber($serialNumber)
  {
    $this->serialNumber = $serialNumber;
  }

  /**
   * Returns object's serialNumber
   *
   * @param
   *
   * @return object's serialNumber
   */
  public function getSerialNumber()
  {
    return $this->serialNumber;
  }

  /**
   * Sets object's $hsCode
   *
   * @param $hsCode
   *
   * @return
   */
  public function setHsCode($hsCode)
  {
    $this->hsCode = $hsCode;
  }

  /**
   * Returns object's hsCode
   *
   * @param
   *
   * @return object's hsCode
   */
  public function getHsCode()
  {
    return $this->hsCode;
  }

  /**
   * Sets object's $valueAmount
   *
   * @param $valueAmount
   *
   * @return
   */
  public function setValueAmount($valueAmount)
  {
    $this->valueAmount = $valueAmount;
  }

  /**
   * Returns object's valueAmount
   *
   * @param
   *
   * @return object's valueAmount
   */
  public function getValueAmount()
  {
    return $this->valueAmount;
  }

  /**
   * Sets object's $valueCurrency
   *
   * @param $valueCurrency
   *
   * @return
   */
  public function setValueCurrency($valueCurrency)
  {
    $this->valueCurrency = $valueCurrency;
  }

  /**
   * Returns object's valueCurrency
   *
   * @param
   *
   * @return object's valueCurrency
   */
  public function getValueCurrency()
  {
    return $this->valueCurrency;
  }

  /**
   * Sets object's $comments
   *
   * @param $comments
   *
   * @return
   */
  public function setComments($comments)
  {
    $this->comments = $comments;
  }

  /**
   * Returns object's comments
   *
   * @param
   *
   * @return object's comments
   */
  public function getComments()
  {
    return $this->comments;
  }

  /**
   * Sets object's $quantity
   *
   * @param $quantity
   *
   * @return
   */
  public function setQuantity($quantity)
  {
    $this->quantity = $quantity;
  }

  /**
   * Returns object's quantity
   *
   * @param
   *
   * @return object's quantity
   */
  public function getQuantity()
  {
    return $this->quantity;
  }

  /**
   * Sets object's $unitType
   *
   * @param $unitType
   *
   * @return
   */
  public function setUnitType($unitType)
  {
    $this->unitType = $unitType;
  }

  /**
   * Returns object's unitType
   *
   * @param
   *
   * @return object's unitType
   */
  public function getUnitType()
  {
    return $this->unitType;
  }

  /**
   * Sets object's $manufactureCountry
   *
   * @param $manufactureCountry
   *
   * @return
   */
  public function setManufactureCountry($manufactureCountry)
  {
    $this->manufactureCountry = $manufactureCountry;
  }

  /**
   * Returns object's manufactureCountry
   *
   * @param
   *
   * @return object's manufactureCountry
   */
  public function getManufactureCountry()
  {
    return $this->manufactureCountry;
  }

  /**
   * Sets object's $originalId
   *
   * @param $originalId
   *
   * @return
   */
  public function setOriginalId($originalId)
  {
    $this->originalId = $originalId;
  }

  /**
   * Returns object's originalId
   *
   * @param
   *
   * @return object's originalId
   */
  public function getOriginalId()
  {
    return $this->originalId;
  }

  /**
   * Sets object's $originalTransacationId
   *
   * @param $originalTransactionId
   *
   * @return
   */
  public function setOriginalTransactionId($originalTransactionId)
  {
    $this->originalTransactionId = $originalTransactionId;
  }

  /**
   * Returns object's originalTransactionid
   *
   * @param
   *
   * @return object's originalTransactionId
   */
  public function getOriginalTransactionId()
  {
    return $this->originalTransactionId;
  }

  /**
   * object's constructor
   *
   * @param $description, $serialNumber, $hsCode, $valueAmount, $valueCurrency, $comments,
   *        $quantity, $unitType, $manufactureCountry,
   *        $originalId, $originalTransactionId
   *
   * @return
   */
  public function __construct($description, $serialNumber, $hsCode, $valueAmount, $valueCurrency,
                              $comments=null, $quantity, $unitType,
                              $manufactureCountry, $originalId, $originalTransactionId)
  {
    $this->setDescription($description);
    $this->setSerialNumber($serialNumber);
    $this->setHsCode($hsCode);
    $this->setValueAmount($valueAmount);
    $this->setValueCurrency($valueCurrency);
    $this->setComments($comments);
    $this->setQuantity($quantity);
    $this->setUnitType($unitType);
    $this->setManufactureCountry($manufactureCountry);
    $this->setOriginalId($originalId);
    $this->setOriginalTransactionId($originalTransactionId);
  }
}
?>