<?php
class SeRateInquiry
{
  /**
   * collectionAddress
   *
   * @var array SeRateInquiryAddress
   */
  protected $collectionAddress;

  /**
   * destinationAddress
   *
   * @var array SeRateInquiryAddress
   */
  protected $destinationAddress;

  /**
    * collectionUserAddressId
    *
    * @var numeric optional
    */
  protected $collectionUserAddressId;

  /**
    * destinationUserAddressId
    *
    * @var numeric optional
    */
  protected $destinationUserAddressId;

  /**
    * date
    *
    * @var string
    */
  protected $date;

  /**
    * courierId
    *
    * @var numeric
    */
  protected $courierId;

  /**
    * serviceId
    *
    * @var numeric
    */
  protected $serviceId;

  /**
    * pickupOnly
    *
    * @var boolean optional
    */
  protected $pickupOnly=false;

  /**
    * dropOffOnly
    *
    * @var boolean optional
    */
  protected $dropOffOnly=false;

  /**
    * cheapest
    *
    * @var boolean optional
    */
  protected $cheapest=false;

  /**
    * longest
    *
    * @var boolean optional
    */
  protected $longest=false;

  /**
    * shortest
    *
    * @var boolean optional
    */
  protected $shortest=false;

  /**
    * signatureConfirmation
    *
    * @var boolean optional
    */
  protected $signatureConfirmation;

  /**
    * today
    *
    * @var numeric
    */
  protected $today;

  /**
    * currency
    *
    * @var string optional
    */
  protected $currency;

  /**
    * residential
    *
    * @var boolean optional
    */
  protected $residential;

  /**
    * packageX
    *
    * @var array optional
    */
  protected $packageX = array();

  /**
    * flatRatePackageIdX
    *
    * @var array optional
    */
  protected $flatRatePackageIdX = array();

  /**
    * predefinedSizeIdX
    *
    * @var array optional
    */
  protected $predefinedSizeIdX = array();

  /**
    * ignoreWorkingHours
    *
    * @var boolean optional
    */
  protected $ignoreWorkingHours=false;

  /**
    * auth
    *
    * @var boolean optional
    */
  protected $auth=false;

  /**
   * Sets object's collectionAddress
   *
   * @param $collectionAddress
   *
   * @return
   */
  public function setCollectionAddress($collectionAddress)
  {
    $this->collectionAddress = $collectionAddress;
  }

  /**
   * Returns object's collectionAddress
   *
   * @param
   *
   * @return object's collectionAddress
   */
  public function getCollectionAddress()
  {
    return $this->collectionAddress;
  }

  /**
   * Sets object's $destinationAddress
   *
   * @param $destinationAddress
   *
   * @return
   */
  public function setDestinationAddress($destinationAddress)
  {
    $this->destinationAddress = $destinationAddress;
  }

  /**
   * Returns object's destinationAddress
   *
   * @param
   *
   * @return object's destinationAddress
   */
  public function getDestinationAddress()
  {
    return $this->destinationAddress;
  }

  /**
   * Sets object's $collectionUserAddressId
   *
   * @param $collectionUserAddressId
   *
   * @return
   */
  public function setCollectionUserAddressId($collectionUserAddressId)
  {
    $this->collectionUserAddressId = $collectionUserAddressId;
  }

  /**
   * Returns object's collectionUserAddressId
   *
   * @param
   *
   * @return object's collectionUserAddressId
   */
  public function getCollectionUserAddressId()
  {
    return $this->collectionUserAddressId;
  }

  /**
   * Sets object's $destinationUserAddressId
   *
   * @param $destinationUserAddressId
   *
   * @return
   */
  public function setDestinationUserAddressId($destinationUserAddressId)
  {
    $this->destinationUserAddressId = $destinationUserAddressId;
  }

  /**
   * Returns object's destinationUserAddressId
   *
   * @param
   *
   * @return object's destinationUserAddressId
   */
  public function getDestinationUserAddressId()
  {
    return $this->destinationUserAddressId;
  }

  /**
   * Sets object's $date
   *
   * @param $date
   *
   * @return
   */
  public function setDate($date)
  {
    $this->date = $date;
  }

  /**
   * Returns object's date
   *
   * @param
   *
   * @return object's date
   */
  public function getDate()
  {
    return $this->date;
  }

  /**
   * Sets object's $courierId
   *
   * @param $courierId
   *
   * @return
   */
  public function setCourierId($courierId)
  {
    $this->courierId = $courierId;
  }

  /**
   * Returns object's courierId
   *
   * @param
   *
   * @return object's courierId
   */
  public function getCourierId()
  {
    return $this->courierId;
  }

  /**
   * Sets object's $serviceId
   *
   * @param $serviceId
   *
   * @return
   */
  public function setServiceId($serviceId)
  {
    $this->serviceId = $serviceId;
  }

  /**
   * Returns object's serviceId
   *
   * @param
   *
   * @return object's serviceId
   */
  public function getServiceId()
  {
    return $this->serviceId;
  }

  /**
   * Sets object's $signatureConfirmation
   *
   * @param $signatureConfirmation
   *
   * @return
   */
  public function setSignatureConfirmation($signatureConfirmation)
  {
    $this->signatureConfirmation = $signatureConfirmation;
  }

  /**
   * Returns object's signatureConfirmation
   *
   * @param
   *
   * @return object's signatureConfirmation
   */
  public function getSignatureConfirmation()
  {
    return $this->signatureConfirmation;
  }

  /**
   * Sets object's $pickupOnly
   *
   * @param $pickupOnly
   *
   * @return
   */
  public function setPickupOnly($pickupOnly)
  {
    $this->pickupOnly = $pickupOnly;
  }

  /**
   * Returns object's pickupOnly
   *
   * @param
   *
   * @return object's pickupOnly
   */
  public function getPickupOnly()
  {
    return $this->pickupOnly;
  }

  /**
   * Sets object's $dropOffOnly
   *
   * @param $dropOffOnly
   *
   * @return
   */
  public function setDropOffOnly($dropOffOnly)
  {
    $this->dropOffOnly = $dropOffOnly;
  }

  /**
   * Returns object's dropOffOnly
   *
   * @param
   *
   * @return object's dropOffOnly
   */
  public function getDropOffOnly()
  {
    return $this->dropOffOnly;
  }

  /**
   * Sets object's $shortest
   *
   * @param $shortest
   *
   * @return
   */
  public function setShortest($shortest)
  {
    $this->shortest = $shortest;
  }

  /**
   * Returns object's shortest
   *
   * @param
   *
   * @return object's shortest
   */
  public function getShortest()
  {
    return $this->shortest;
  }

  /**
   * Sets object's $cheapest
   *
   * @param $cheapest
   *
   * @return
   */
  public function setCheapest($cheapest)
  {
    $this->cheapest = $cheapest;
  }

  /**
   * Returns object's cheapest
   *
   * @param
   *
   * @return object's cheapest
   */
  public function getCheapest()
  {
    return $this->cheapest;
  }

  /**
   * Sets object's $longest
   *
   * @param $longest
   *
   * @return
   */
  public function setLongest($longest)
  {
    $this->longest = $longest;
  }

  /**
   * Returns object's longest
   *
   * @param
   *
   * @return object's longest
   */
  public function getLongest()
  {
    return $this->longest;
  }

  /**
   * Sets object's $today
   *
   * @param $today
   *
   * @return
   */
  public function setToday($today)
  {
    $this->today = $today;
  }

  /**
   * Returns object's today
   *
   * @param
   *
   * @return object's today
   */
  public function getToday()
  {
    return $this->today;
  }

  /**
   * Sets object's $currency
   *
   * @param $currency
   *
   * @return
   */
  public function setCurrency($currency)
  {
    $this->currency = $currency;
  }

  /**
   * Returns object's currency
   *
   * @param
   *
   * @return object's currency
   */
  public function getCurrency()
  {
    return $this->currency;
  }

  /**
   * Sets object's $residential
   *
   * @param $residential
   *
   * @return
   */
  public function setResidential($residential)
  {
    $this->residential = $residential;
  }

  /**
   * Returns object's residential
   *
   * @param
   *
   * @return object's residential
   */
  public function getResidential()
  {
    return $this->residential;
  }

  /**
   * Sets object's $packageX
   *
   * @param $packageX
   *
   * @return
   */
  public function setPackageX($packageX)
  {
    $this->packageX = $packageX;
  }

  /**
   * Returns object's packageX
   *
   * @param
   *
   * @return object's packageX
   */
  public function getPackageX()
  {
    return $this->packageX;
  }

  /**
   * Sets object's $flatRatePackageIdX
   *
   * @param $flatRatePackageIdX
   *
   * @return
   */
  public function setFlatRatePackageIdX($flatRatePackageIdX)
  {
    $this->flatRatePackageIdX = $flatRatePackageIdX;
  }

  /**
   * Returns object's flatRatePackageIdX
   *
   * @param
   *
   * @return object's flatRatePackageIdX
   */
  public function getFlatRatePackageIdX()
  {
    return $this->flatRatePackageIdX;
  }

  /**
   * Sets object's $predefinedSizeIdX
   *
   * @param $predefinedSizeIdX
   *
   * @return
   */
  public function setPredefinedSizeIdX($predefinedSizeIdX)
  {
    $this->predefinedSizeIdX = $predefinedSizeIdX;
  }

  /**
   * Returns object's predefinedSizeIdX
   *
   * @param
   *
   * @return object's predefinedSizeIdX
   */
  public function getPredefinedSizeIdX()
  {
    return $this->predefinedSizeIdX;
  }

  /**
   * Sets object's $ignoreWorkingHours
   *
   * @param $ignoreWorkingHours
   *
   * @return
   */
  public function setIgnoreWorkingHours($ignoreWorkingHours)
  {
    $this->ignoreWorkingHours = $ignoreWorkingHours;
  }

  /**
   * Returns object's ignoreWorkingHours
   *
   * @param
   *
   * @return object's ignoreWorkingHours
   */
  public function getIgnoreWorkingHours()
  {
    return $this->ignoreWorkingHours;
  }

  /**
   * Sets object's $auth
   *
   * @param $auth
   *
   * @return
   */
  public function setAuth($auth)
  {
    $this->auth = $auth;
  }

  /**
   * Returns object's auth
   *
   * @param
   *
   * @return object's auth
   */
  public function getAuth()
  {
    return $this->auth;
  }

  /**
   * Object's constructor.
   */
  public function __construct($collectionAddress, $destinationAddress, $collectionUserAddressId=null,
                             $destinationUserAddressId=null, $date, $signatureConfirmation=null,
                             $today, $residential=null, $packageX=null, $flatRatePackageIdX=null,
                             $predefinedSizeIdX=null, $courierId=null, $serviceId=null, $currency=null,
                             $pickupOnly=false, $dropOffOnly=false, $cheapest=false, $shortest=false,
                             $longest=false, $ignoreWorkingHours=false, $auth=false)
  {
    $this->setCollectionAddress($collectionAddress);
    $this->setDestinationAddress($destinationAddress);
    $this->setCollectionUserAddressId($collectionUserAddressId);
    $this->setDestinationUserAddressId($destinationUserAddressId);
    $this->setDate($date);
    $this->setSignatureConfirmation($signatureConfirmation);
    $this->setToday($today);
    $this->setResidential($residential);
    $this->setPackageX($packageX);
    $this->setFlatRatePackageIdX($flatRatePackageIdX);
    $this->setPredefinedSizeIdX($predefinedSizeIdX);
    $this->setCourierId($courierId);
    $this->setServiceId($serviceId);
    $this->setCurrency($currency);
    $this->setPickupOnly($pickupOnly);
    $this->setDropOffOnly($dropOffOnly);
    $this->setCheapest($cheapest);
    $this->setShortest($shortest);
    $this->setLongest($longest);
    $this->setIgnoreWorkingHours($ignoreWorkingHours);
    $this->setAuth($auth);
  }
}
?>