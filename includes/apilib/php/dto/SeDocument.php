<?php
/**
 * SeDocument.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one Document item
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeDocument.v.0.1
 */
class SeDocument
{
  /**
   * Id of the returned item.
   *
   * @var integer
   */
  protected $id;

  /**
   * Name of the returned item.
   *
   * @var string
   */
  protected $name;

  /**
   * Type of the returned item.
   *
   * @var string
   */
  protected $type;

  /**
   * MimeType of the returned item.
   *
   * @var string
   */
  protected $mimeType;

  /**
   * Url of the returned item.
   *
   * @var string
   */
  protected $url;

  /**
   * FileName of the returned item.
   *
   * @var string
   */
  protected $fileName;

  /**
   * Returns object's Id
   *
   * @param
   *
   * @return object's Id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Sets object's Id
   *
   * @param $id
   *
   * @return
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Returns object's name
   *
   * @param
   *
   * @return object's name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Sets object's Name
   *
   * @param $Name
   *
   * @return
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Returns object's Type
   *
   * @param
   *
   * @return object's type
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Sets object's Type
   *
   * @param $type
   *
   * @return
   */
  public function setType($type)
  {
    $this->type = $type;
  }

  /**
   * Returns object's MimeType
   *
   * @param
   *
   * @return object's MimeType
   */
  public function getMimeType()
  {
    return $this->mimeType;
  }

  /**
   * Sets object's MimeType
   *
   * @param $mimeType
   *
   * @return
   */
  public function setMimeType($mimeType)
  {
    $this->mimeType = $mimeType;
  }

  /**
   * Returns object's Url
   *
   * @param
   *
   * @return object's url
   */
  public function getUrl()
  {
    return $this->url;
  }

  /**
   * Sets object's Url
   *
   * @param $url
   *
   * @return
   */
  public function setUrl($url)
  {
    $this->url = $url;
  }

  /**
   * Returns object's FileName
   *
   * @param
   *
   * @return object's FileName
   */
  public function getFileName()
  {
    return $this->fileName;
  }

  /**
   * Sets object's FileName
   *
   * @param $fileName
   *
   * @return
   */
  public function setFileName($fileName)
  {
    $this->fileName = $fileName;
  }

  /**
   * object's constructor
   *
   * @param $Id, $Name
   *
   * @return
   */
  public function __construct($id, $name, $type, $mimeType, $url, $fileName)
  {
    $this->setId($id);
    $this->setName($name);
    $this->setType($type);
    $this->setMimeType($mimeType);
    $this->setUrl($url);
    $this->setFileName($fileName);
  }
}
?>