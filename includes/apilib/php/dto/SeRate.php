<?php
/**
 * SeRate.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one item in request result array
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeRate.v.0.1
 */
class SeRate
{
  /**
   * Rate ID.
   *
   * @var integer
   */
  protected $Id;

  /**
   * Graphic symbol used as a shorthand for a currency's name.
   *
   * @var string
   */
  protected $CurrencySymbol;

  /**
   * A three-digit code number assigned to each currency.
   *
   * @var string
   */
  protected $CurrencyCode;

  /**
   * Total shipping cost.
   *
   * @var float
   */
  protected $CurrencyValue;

  /**
   * Courier ID.
   *
   * @var integer
   */
  protected $CourierId;

  /**
   * Courier name.
   *
   * @var string
   */
  protected $Courier;

  /**
   * Service ID.
   *
   * @var integer
   */
  protected $ServiceId;

  /**
   * Service name, e.g. standard overnight.
   *
   * @var string
   */
  protected $ServiceName;

  /**
   * Currency code.
   *
   * @var string
   */
  protected $InsuranceCurrency;

  /**
   * Amount of included insurance.
   *
   * @var float
   */
  protected $InsuranceAmount;

  /**
   * Indicates availability of additional insurance.
   *
   * @var bool
   */
  protected $AdditionalInsuranceAvailable;

  /**
   * Currency code. Related to additional insurance.
   *
   * @var string
   */
  protected $AdditionalInsuranceCurrency;

  /**
   * Limit of additional insurance.
   *
   * @var integer
   */
  protected $AdditionalInsuranceLimit;

  /**
   * List of additional insurance amounts.
   *
   * @var array
   */
  protected $AdditionalInsuranceAmounts;

  /**
   * Estimated date and time of delivery.
   *
   * @var timestamp
   */
  protected $Eta;

  /**
   * Human readable estimated date and time of delivery.
   *
   * @var string
   */
  protected $HumanEta;

  /**
   * An array of pickup times.
   *
   * @var array
   */
  protected $PickupTimes;

  /**
   * Indicates whether package drop-off at couriers is available e.g. post office for USPS.
   *
   * @var bool
   */
  protected $DropOffAvailable;

  /**
   * Indicates whether package has a discount.
   *
   * @var bool
   */
  protected $Discount;

  /**
   * Date returned in meta.
   *
   * @var string
   */
  protected $MetaDate;

  /**
   * Returns object's Id
   *
   * @param
   *
   * @return object's Id
   */
  public function getId()
  {
    return $this->Id;
  }

  /**
   * Sets object's Id
   *
   * @param $id
   *
   * @return
   */
  public function setId($Id)
  {
    $this->Id = $Id;
  }

  /**
   * Returns object's CurrencySymbol
   *
   * @param
   *
   * @return object's CurrencySymbol
   */
  public function getCurrencySymbol()
  {
    return $this->CurrencySymbol;
  }

  /**
   * Sets object's CurrencySymbol
   *
   * @param $CurrencySymbol
   *
   * @return
   */
  public function setCurrencySymbol($CurrencySymbol)
  {
    $this->CurrencySymbol = $CurrencySymbol;
  }

  /**
   * Returns object's CurrencyCode
   *
   * @param
   *
   * @return object's CurrencyCode
   */
  public function getCurrencyCode()
  {
    return $this->CurrencyCode;
  }

  /**
   * Sets object's CurrencyCode
   *
   * @param $CurrencyCode
   *
   * @return
   */
  public function setCurrencyCode($CurrencyCode)
  {
    $this->CurrencyCode = $CurrencyCode;
  }

  /**
   * Returns object's CurrencyValue
   *
   * @param
   *
   * @return object's CurrencyValue
   */
  public function getCurrencyValue()
  {
    return $this->CurrencyValue;
  }

  /**
   * Sets object's CurrencyValue
   *
   * @param $CurrencyValue
   *
   * @return
   */
  public function setCurrencyValue($CurrencyValue)
  {
    $this->CurrencyValue = $CurrencyValue;
  }

  /**
   * Returns object's CourierId
   *
   * @param
   *
   * @return object's CourierId
   */
  public function getCourierId()
  {
    return $this->CourierId;
  }

  /**
   * Sets object's CourierId
   *
   * @param $CourierId
   *
   * @return
   */
  public function setCourierId($CourierId)
  {
    $this->CourierId = $CourierId;
  }

  /**
   * Returns object's Courier
   *
   * @param
   *
   * @return object's Courier
   */
  public function getCourier()
  {
    return $this->Courier;
  }

  /**
   * Sets object's Courier
   *
   * @param $Courier
   *
   * @return
   */
  public function setCourier($Courier)
  {
    $this->Courier = $Courier;
  }

  /**
   * Returns object's ServiceId
   *
   * @param
   *
   * @return object's ServiceId
   */
  public function getServiceId()
  {
    return $this->ServiceId;
  }

  /**
   * Sets object's ServiceId
   *
   * @param $ServiceId
   *
   * @return
   */
  public function setServiceId($ServiceId)
  {
    $this->ServiceId = $ServiceId;
  }

  /**
   * Returns object's ServiceName
   *
   * @param
   *
   * @return object's ServiceName
   */
  public function getServiceName()
  {
    return $this->ServiceName;
  }

  /**
   * Sets object's ServiceName
   *
   * @param $ServiceName
   *
   * @return
   */
  public function setServiceName($ServiceName)
  {
    $this->ServiceName = $ServiceName;
  }

  /**
   * Returns object's InsuranceCurrency
   *
   * @param
   *
   * @return object's InsuranceCurrency
   */
  public function getInsuranceCurrency()
  {
    return $this->InsuranceCurrency;
  }

  /**
   * Sets object's InsuranceCurrency
   *
   * @param $InsuranceCurrency
   *
   * @return
   */
  public function setInsuranceCurrency($InsuranceCurrency)
  {
    $this->InsuranceCurrency = $InsuranceCurrency;
  }

  /**
   * Returns object's InsuranceAmount
   *
   * @param
   *
   * @return object's InsuranceAmount
   */
  public function getInsuranceAmount()
  {
    return $this->InsuranceAmount;
  }

  /**
   * Sets object's InsuranceAmount
   *
   * @param $InsuranceAmount
   *
   * @return
   */
  public function setInsuranceAmount($InsuranceAmount)
  {
    $this->InsuranceAmount = $InsuranceAmount;
  }

  /**
   * Returns object's AdditionalInsuranceAvailable
   *
   * @param
   *
   * @return object's AdditionalInsuranceAvailable
   */
  public function getAdditionalInsuranceAvailable()
  {
    return $this->AdditionalInsuranceAvailable;
  }

  /**
   * Sets object's AdditionalInsuranceAvailable
   *
   * @param $AdditionalInsuranceAvailable
   *
   * @return
   */
  public function setAdditionalInsuranceAvailable($AdditionalInsuranceAvailable)
  {
    $this->AdditionalInsuranceAvailable = $AdditionalInsuranceAvailable;
  }

  /**
   * Returns object's AdditionalInsuranceCurrency
   *
   * @param
   *
   * @return object's AdditionalInsuranceCurrency
   */
  public function getAdditionalInsuranceCurrency()
  {
    return $this->AdditionalInsuranceCurrency;
  }

  /**
   * Sets object's AdditionalInsuranceCurrency
   *
   * @param $AdditionalInsuranceCurrency
   *
   * @return
   */
  public function setAdditionalInsuranceCurrency($AdditionalInsuranceCurrency)
  {
    $this->AdditionalInsuranceCurrency = $AdditionalInsuranceCurrency;
  }

  /**
   * Returns object's AdditionalInsuranceLimit
   *
   * @param
   *
   * @return object's AdditionalInsuranceLimit
   */
  public function getAdditionalInsuranceLimit()
  {
    return $this->AdditionalInsuranceLimit;
  }

  /**
   * Sets object's AdditionalInsuranceLimit
   *
   * @param $AdditionalInsuranceLimit
   *
   * @return
   */
  public function setAdditionalInsuranceLimit($AdditionalInsuranceLimit)
  {
    $this->AdditionalInsuranceLimit = $AdditionalInsuranceLimit;
  }

  /**
   * Returns object's AdditionalInsuranceAmounts
   *
   * @param
   *
   * @return object's AdditionalInsuranceAmounts
   */
  public function getAdditionalInsuranceAmounts()
  {
    return $this->AdditionalInsuranceAmounts;
  }

  /**
   * Sets object's AdditionalInsuranceAmounts
   *
   * @param $AdditionalInsuranceAmounts
   *
   * @return
   */
  public function setAdditionalInsuranceAmounts($AdditionalInsuranceAmounts)
  {
    $this->AdditionalInsuranceAmounts = $AdditionalInsuranceAmounts;
  }

  /**
   * Returns object's Eta
   *
   * @param
   *
   * @return object's Eta
   */
  public function getEta()
  {
    return $this->Eta;
  }

  /**
   * Sets object's Eta
   *
   * @param $Eta
   *
   * @return
   */
  public function setEta($Eta)
  {
    $this->Eta = $Eta;
  }

  /**
   * Returns object's HumanEta
   *
   * @param
   *
   * @return object's HumanEta
   */
  public function getHumanEta()
  {
    return $this->HumanEta;
  }

  /**
   * Sets object's HumanEta
   *
   * @param $HumanEta
   *
   * @return
   */
  public function setHumanEta($HumanEta)
  {
    $this->HumanEta = $HumanEta;
  }

  /**
   * Returns object's PickupTimes
   *
   * @param
   *
   * @return object's PickupTimes
   */
  public function getPickupTimes()
  {
    return $this->PickupTimes;
  }

  /**
   * Sets object's PickupTimes
   *
   * @param $PickupTimes
   *
   * @return
   */
  public function setPickupTimes($PickupTimes)
  {
    $this->PickupTimes = $PickupTimes;
  }

  /**
   * Returns object's DropOffAvailable
   *
   * @param
   *
   * @return object's DropOffAvailable
   */
  public function getDropOffAvailable()
  {
    return $this->DropOffAvailable;
  }

  /**
   * Sets object's DropOffAvailable
   *
   * @param $DropOffAvailable
   *
   * @return
   */
  public function setDropOffAvailable($DropOffAvailable)
  {
    $this->DropOffAvailable = $DropOffAvailable;
  }

  /**
   * Returns object's Discount
   *
   * @param
   *
   * @return object's Discount
   */
  public function getDiscount()
  {
    return $this->Discount;
  }

  /**
   * Sets object's Discount
   *
   * @param $Discount
   *
   * @return
   */
  public function setDiscount($Discount)
  {
    $this->Discount = $Discount;
  }

  /**
   * Returns object's MetaDate
   *
   * @param
   *
   * @return object's MetaDate
   */
  public function getMetaDate()
  {
    return $this->MetaDate;
  }

  /**
   * Sets object's MetaDate
   *
   * @param $metaDate
   *
   * @return
   */
  public function setMetaDate($metaDate)
  {
    $this->MetaDate = $metaDate;
  }

  /**
   * Gets difference in days between Meta date and Eta date
   *
   * @param
   *
   * @return integer
   */
  public function getEtaDays()
  {
    if (is_null($this->getEta()) || is_null($this->getMetaDate()))
    {
      return 0;
    }

    $datetime1 = date_create($this->getMetaDate());
    $datetime2 = date_create(date('Y-m-d', strtotime($this->getEta())));
    $interval = date_diff($datetime1, $datetime2);

    return $interval->format('%a');
  }

  /**
   * object's constructor
   *
   * @param $Id, $CurrencySymbol, $CurrencyCode, $CurrencyValue, $CourierId, $Courier, $ServiceId, $ServiceName,
   *        $InsuranceCurrency, $InsuranceAmount, $AdditionalInsuranceAvailable, $AdditionalInsuranceCurrency,
   *        $AdditionalInsuranceLimit, $Eta, $HumanEta, $PickupTimes, $DropOffAvailable
   *
   * @return
   */
  public function __construct($Id=null, $CurrencySymbol=null, $CurrencyCode=null, $CurrencyValue=null,
                              $CourierId=null, $Courier=null, $ServiceId=null, $ServiceName=null,
                              $InsuranceCurrency=null, $InsuranceAmount=null,
                              $AdditionalInsuranceAvailable=null, $AdditionalInsuranceCurrency=null,
                              $AdditionalInsuranceLimit=null, $Eta=null, $HumanEta=null, $PickupTimes=null,
                              $DropOffAvailable=null, $Discount=null, $MetaDate=null)
  {
    $this->setId($Id);
    $this->setCurrencySymbol($CurrencySymbol);
    $this->setCurrencyCode($CurrencyCode);
    $this->setCurrencyValue($CurrencyValue);
    $this->setCourierId($CourierId);
    $this->setCourier($Courier);
    $this->setServiceId($ServiceId);
    $this->setServiceName($ServiceName);
    $this->setInsuranceCurrency($InsuranceCurrency);
    $this->setInsuranceAmount($InsuranceAmount);
    $this->setAdditionalInsuranceAvailable($AdditionalInsuranceAvailable);
    $this->setAdditionalInsuranceCurrency($AdditionalInsuranceCurrency);
    $this->setAdditionalInsuranceLimit($AdditionalInsuranceLimit);
    $this->setEta($Eta);
    $this->setHumanEta($HumanEta);
    $this->setPickupTimes($PickupTimes);
    $this->setDropOffAvailable($DropOffAvailable);
    $this->setDiscount($Discount);
    $this->setMetaDate($MetaDate);
  }
}
?>