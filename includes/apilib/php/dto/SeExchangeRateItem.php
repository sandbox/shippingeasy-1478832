<?php
/**
 * SeExchangeRateItem.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one ExchangeRateItem item.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeExchangeRateItem.v.0.1
 */
class SeExchangeRateItem
{
  /**
   * Currency being sold.
   *
   * @var string
   */
  protected $currencyFrom;

  /**
   * Currency being purchased.
   *
   * @var string
   */
  protected $currencyTo;

  /**
   * Exchange rate.
   *
   * @var numeric with 4 decimal places
   */
  protected $rate;

  /**
   * Time when was this rate generated.
   *
   * @var timestamp without time zone
   */
  protected $createdAt;

  /**
   * Returns object's currencyFrom
   *
   * @param
   *
   * @return object's currencyFrom
   */
  public function getCurrencyFrom()
  {
    return $this->currencyFrom;
  }

  /**
   * Sets object's currencyFrom
   *
   * @param $currencyFrom
   *
   * @return
   */
  public function setCurrencyFrom($currencyFrom)
  {
    $this->currencyFrom = $currencyFrom;
  }

  /**
   * Returns object's currencyTo
   *
   * @param
   *
   * @return object's currencyTo
   */
  public function getCurrencyTo()
  {
    return $this->currencyTo;
  }

  /**
   * Sets object's currencyTo
   *
   * @param $currencyTo
   *
   * @return
   */
  public function setCurrencyTo($currencyTo)
  {
    $this->currencyTo = $currencyTo;
  }

  /**
   * Returns object's rate
   *
   * @param
   *
   * @return object's rate
   */
  public function getRate()
  {
    return $this->rate;
  }

  /**
   * Sets object's rate
   *
   * @param $rate
   *
   * @return
   */
  public function setRate($rate)
  {
    $this->rate = $rate;
  }

  /**
   * Returns object's createdAt
   *
   * @param
   *
   * @return object's createdAt
   */
  public function getCreatedAt()
  {
    return $this->createdAt;
  }

  /**
   * Sets object's createdAt
   *
   * @param $createdAt
   *
   * @return
   */
  public function setCreatedAt($createdAt)
  {
    $this->createdAt = $createdAt;
  }

  /**
   * object's constructor
   *
   * @param $currencyFrom, $currencyTo, $rate, $createdAt
   *
   * @return
   */
  public function __construct($currencyFrom=null, $currencyTo=null, $rate=null, $createdAt=null)
  {
    $this->setCurrencyFrom($currencyFrom);
    $this->setCurrencyTo($currencyTo);
    $this->setRate($rate);
    $this->setCreatedAt($createdAt);
  }
}
?>