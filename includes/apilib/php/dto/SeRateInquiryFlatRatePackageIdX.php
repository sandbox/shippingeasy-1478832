<?php
class SeRateInquiryFlatRatePackageIdX
{
  /**
   * $name
   *
   * @var string
   */
  protected $name;

  /**
   * $value
   *
   * @var numeric
   */
  protected $value;

  /**
   * Sets object's $name
   *
   * @param $name
   *
   * @return
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Returns object's name
   *
   * @param
   *
   * @return object's name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Sets object's $value
   *
   * @param $value
   *
   * @return
   */
  public function setValue($value)
  {
    $this->value = $value;
  }

  /**
   * Returns object's value
   *
   * @param
   *
   * @return object's value
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Object's constructor.
   */
  public function __construct($name, $value)
  {
    $this->setName($name);
    $this->setValue($value);
  }
}
?>