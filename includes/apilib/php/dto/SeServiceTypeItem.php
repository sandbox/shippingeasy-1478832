<?php
/**
 * SeServiceTypeItem.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one ServiceTypeItem item.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeServiceTypeItem.v.0.1
 */
class SeServiceTypeItem
{
  /**
   * Service type id.
   *
   * @var numeric
   */
  protected $id;

  /**
   * Id of the courier that offers the service.
   *
   * @var numeric
   */
  protected $courierId;

  /**
   * Service type code.
   *
   * @var string
   */
  protected $code;

  /**
   * Service type name.
   *
   * @var string
   */
  protected $name;

  /**
   * Returns object's id
   *
   * @param
   *
   * @return object's id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Sets object's id
   *
   * @param $id
   *
   * @return
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Returns object's courierId
   *
   * @param
   *
   * @return object's courierId
   */
  public function getCourierId()
  {
    return $this->courierId;
  }

  /**
   * Sets object's courierId
   *
   * @param $courierId
   *
   * @return
   */
  public function setCourierId($courierId)
  {
    $this->courierId = $courierId;
  }

  /**
   * Returns object's code
   *
   * @param
   *
   * @return object's code
   */
  public function getCode()
  {
    return $this->code;
  }

  /**
   * Sets object's code
   *
   * @param $code
   *
   * @return
   */
  public function setCode($code)
  {
    $this->code = $code;
  }

  /**
   * Returns object's name
   *
   * @param
   *
   * @return object's name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Sets object's name
   *
   * @param $name
   *
   * @return
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * object's constructor
   *
   * @param $id, $courierId, $code, $name
   *
   * @return
   */
  public function __construct($id=null, $courierId=null, $code=null, $name=null)
  {
    $this->setId($id);
    $this->setCourierId($courierId);
    $this->setCode($code);
    $this->setName($name);
  }
}
?>