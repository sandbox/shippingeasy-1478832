<?php
/**
 * SePackageInquiry.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents inquiry object for Package
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SePackageInquiry.v.0.1
 */
class SePackageInquiry
{
  /**
   * Value of the package.
   *
   * @var numeric
   */
  protected $valueAmount;

  /**
   * Currency in which the package value is expressed.
   *
   * @var string
   */
  protected $valueCurrency;

  /**
   * Weight of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $weight;

  /**
   * Length of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $length;

  /**
   * Width of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $width;

  /**
   * Height of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $height;

  /**
   * Id of the standard size package. This parameter is used when standard size package is selected and
   * Type of the selected standard size package is predefined.
   *
   * @var numeric
   */
  protected $predefinedSizeId;

  /**
   * Id of the standard size package. This parameter is used when standard size package is selected and
   * Type of the selected standard size package is flatRate.
   *
   * @var string
   */
  protected $flatRatePackageIdX;

  /**
   * Indicates if the package contains restricted items.
   *
   * @var boolean
   */
  protected $containsRestrictedItems;

  /**
   * Indicates if metric system of units is used.
   *
   * @var boolean
   */
  protected $isMetric;

  /**
   * Package type as returned by packageType API.
   *
   * @var string
   */
  protected $packageType;

  /**
   * Amount of additional insurance. This parameter is used when user selects additional insurance.
   *
   * @var string
   */
  protected $additionalInsuranceAmount;

  /**
   * Currency of the additional insurance. This parameter is used when user selects additional insurance.
   *
   * @var string
   */
  protected $additionalInsuranceCurrency;

  /**
   * This is an array of items contained in a package. The Items array can contain any number of items.
   * This parameter is optional.
   *
   * @var array SePackageInquiryItem
   */
  protected $items = array();

  /**
   * Returns object's valueAmount
   *
   * @param
   *
   * @return object's valueAmount
   */
  public function getValueAmount()
  {
    return $this->valueAmount;
  }

  /**
   * Sets object's valueAmount
   *
   * @param $valueAmount
   *
   * @return
   */
  public function setValueAmount($valueAmount)
  {
    $this->valueAmount = $valueAmount;
  }

  /**
   * Returns object's valueCurrency
   *
   * @param
   *
   * @return object's valueCurrency
   */
  public function getValueCurrency()
  {
    return $this->valueCurrency;
  }

  /**
   * Sets object's valueCurrency
   *
   * @param $valueCurrency
   *
   * @return
   */
  public function setValueCurrency($valueCurrency)
  {
    $this->valueCurrency = $valueCurrency;
  }

  /**
   * Returns object's weight
   *
   * @param
   *
   * @return object's weight
   */
  public function getWeight()
  {
    return $this->weight;
  }

  /**
   * Sets object's weight
   *
   * @param $weight
   *
   * @return
   */
  public function setWeight($weight)
  {
    $this->weight = $weight;
  }

  /**
   * Returns object's length
   *
   * @param
   *
   * @return object's length
   */
  public function getLength()
  {
    return $this->length;
  }

  /**
   * Sets object's length
   *
   * @param $length
   *
   * @return
   */
  public function setLength($length)
  {
    $this->length = $length;
  }

  /**
   * Returns object's width
   *
   * @param
   *
   * @return object's width
   */
  public function getWidth()
  {
    return $this->width;
  }

  /**
   * Sets object's width
   *
   * @param $width
   *
   * @return
   */
  public function setWidth($width)
  {
    $this->width = $width;
  }

  /**
   * Returns object's height
   *
   * @param
   *
   * @return object's height
   */
  public function getHeight()
  {
    return $this->height;
  }

  /**
   * Sets object's height
   *
   * @param $height
   *
   * @return
   */
  public function setHeight($height)
  {
    $this->height = $height;
  }

  /**
   * Returns object's predefinedSizeId
   *
   * @param
   *
   * @return object's predefinedSizeId
   */
  public function getPredefinedSizeId()
  {
    return $this->predefinedSizeId;
  }

  /**
   * Sets object's predefinedSizeId
   *
   * @param $predefinedSizeId
   *
   * @return
   */
  public function setPredefinedSizeId($predefinedSizeId)
  {
    $this->predefinedSizeId = $predefinedSizeId;
  }

  /**
   * Returns object's flatRatePackageIdX
   *
   * @param
   *
   * @return object's flatRatePackageIdX
   */
  public function getFlatRatePackageIdX()
  {
    return $this->flatRatePackageIdX;
  }

  /**
   * Sets object's flatRatePackageIdX
   *
   * @param $flatRatePackageIdX
   *
   * @return
   */
  public function setFlatRatePackageIdX($flatRatePackageIdX)
  {
    $this->flatRatePackageIdX = $flatRatePackageIdX;
  }

  /**
   * Returns object's containsRestrictedItems
   *
   * @param
   *
   * @return object's containsRestrictedItems
   */
  public function getContainsRestrictedItems()
  {
    return $this->containsRestrictedItems;
  }

  /**
   * Sets object's containsRestrictedItems
   *
   * @param $containsRestrictedItems
   *
   * @return
   */
  public function setContainsRestrictedItems($containsRestrictedItems)
  {
    $this->containsRestrictedItems = $containsRestrictedItems;
  }

  /**
   * Returns object's isMetric
   *
   * @param
   *
   * @return object's isMetric
   */
  public function getIsMetric()
  {
    return $this->isMetric;
  }

  /**
   * Sets object's isMetric
   *
   * @param $isMetric
   *
   * @return
   */
  public function setIsMetric($isMetric)
  {
    $this->isMetric = $isMetric;
  }

  /**
   * Returns object's packageType
   *
   * @param
   *
   * @return object's packageType
   */
  public function getPackageType()
  {
    return $this->packageType;
  }

  /**
   * Sets object's packageType
   *
   * @param $packageType
   *
   * @return
   */
  public function setPackageType($packageType)
  {
    $this->packageType = $packageType;
  }

  /**
   * Returns object's additionalInsuranceAmount
   *
   * @param
   *
   * @return object's additionalInsuranceAmount
   */
  public function getAdditionalInsuranceAmount()
  {
    return $this->additionalInsuranceAmount;
  }

  /**
   * Sets object's additionalInsuranceAmount
   *
   * @param $additionalInsuranceAmount
   *
   * @return
   */
  public function setAdditionalInsuranceAmount($additionalInsuranceAmount)
  {
    $this->additionalInsuranceAmount = $additionalInsuranceAmount;
  }

  /**
   * Returns object's additionalInsuranceCurrency
   *
   * @param
   *
   * @return object's additionalInsuranceCurrency
   */
  public function getAdditionalInsuranceCurrency()
  {
    return $this->additionalInsuranceCurrency;
  }

  /**
   * Sets object's additionalInsuranceCurrency
   *
   * @param $additionalInsuranceCurrency
   *
   * @return
   */
  public function setAdditionalInsuranceCurrency($additionalInsuranceCurrency)
  {
    $this->additionalInsuranceCurrency = $additionalInsuranceCurrency;
  }

  /**
   * Returns object's items
   *
   * @param
   *
   * @return object's items
   */
  public function getItems()
  {
    return $this->items;
  }

  /**
   * Sets object's items
   *
   * @param $items
   *
   * @return
   */
  public function setItems($items)
  {
    $this->items = $items;
  }

  /**
   * object's constructor
   *
   * @param $valueAmount, $valueCurrency, $weight, $length, $width, $height, $predefinedSizeId,
   *        $flatRatePackageIdX, $containsRestrictedItems, $isMetric, $packageType,
   *        $additionalInsuranceAmount, $additionalInsuranceCurrency, $items
   *
   * @return
   */
  public function __construct($valueAmount, $valueCurrency, $weight=null, $length=null, $width=null,
                              $height=null, $predefinedSizeId=null, $flatRatePackageIdX=null,
                              $containsRestrictedItems, $isMetric, $packageType, $additionalInsuranceAmount=null,
                              $additionalInsuranceCurrency=null, $items=null)
  {
    $this->setValueAmount($valueAmount);
    $this->setValueCurrency($valueCurrency);
    $this->setWeight($weight);
    $this->setLength($length);
    $this->setWidth($width);
    $this->setHeight($height);
    $this->setPredefinedSizeId($predefinedSizeId);
    $this->setFlatRatePackageIdX($flatRatePackageIdX);
    $this->setContainsRestrictedItems($containsRestrictedItems);
    $this->setIsMetric($isMetric);
    $this->setPackageType($packageType);
    $this->setAdditionalInsuranceAmount($additionalInsuranceAmount);
    $this->setAdditionalInsuranceCurrency($additionalInsuranceCurrency);
    $this->setItems($items);
  }
}
?>