<?php
/**
 * SeAdditionalInsurance.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one item in request result array
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeAdditionalInsurance.v.0.1
 */
class SeAdditionalInsurance
{
  /**
   * Shipment package id.
   *
   * @var integer
   */
  protected $shipmentPackageId;

  /**
   * package name
   *
   * @var string
   */
  protected $packageName;

  /**
   * recorded shipment id
   *
   * @var integer
   */
  protected $recordedShipmentId;

  /**
   * Returns object's shipmentPackageId
   *
   * @param
   *
   * @return object's shipmentPackageId
   */
  public function getShipmentPackageId()
  {
    return $this->shipmentPackageId;
  }

  /**
   * Sets object's shipmentPackageId
   *
   * @param $shipmentPackageId
   *
   * @return
   */
  public function setShipmentPackageId($shipmentPackageId)
  {
    $this->shipmentPackageId = $shipmentPackageId;
  }

  /**
   * Returns object's packageName
   *
   * @param
   *
   * @return object's packageName
   */
  public function getPackageName()
  {
    return $this->packageName;
  }

  /**
   * Sets object's packageName
   *
   * @param $packageName
   *
   * @return
   */
  public function setPackageName($packageName)
  {
    $this->packageName = $packageName;
  }

  /**
   * Returns object's recordedShipmentId
   *
   * @param
   *
   * @return object's recordedShipmentId
   */
  public function getRecordedShipmentId()
  {
    return $this->recordedShipmentId;
  }

  /**
   * Sets object's recordedShipmentId
   *
   * @param $recordedShipmentId
   *
   * @return
   */
  public function setRecordedShipmentId($recordedShipmentId)
  {
    $this->recordedShipmentId = $recordedShipmentId;
  }

  /**
   * object's constructor
   *
   * @param $shipmentPackageId, $packageName, $recordedShipmentId
   *
   * @return
   */
  public function __construct($shipmentPackageId=null, $packageName=null, $recordedShipmentId=null)
  {
    $this->setShipmentPackageId($shipmentPackageId);
    $this->setPackageName($packageName);
    $this->setRecordedShipmentId($recordedShipmentId);
  }
}
?>