<?php
/**
 * SeShipmentItem.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one item in request result array
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeShipmentItem.v.0.1
 */
class SeShipmentItem
{
  /**
   * Shipment id.
   *
   * @var integer
   */
  protected $id;

  /**
   * Courier Id
   *
   * @var integer
   */
  protected $courierId;

  /**
   * Rate Id
   *
   * @var integer
   */
  protected $rateId;

  /**
   * pickupAt
   *
   * @var string
   */
  protected $pickupAt;

  /**
   * packageCount
   *
   * @var integer
   */
  protected $packageCount;

  /**
   * Eta
   *
   * @var string
   */
  protected $eta;

  /**
   * trackingNumber
   *
   * @var string
   */
  protected $trackingNumber;

  /**
   * status
   *
   * @var string
   */
  protected $status;

  /**
   * totalAmount
   *
   * @var numeric
   */
  protected $totalAmount;

  /**
   * currency
   *
   * @var string
   */
  protected $currency;

  /**
   * userId
   *
   * @var integer
   */
  protected $userId;

  /**
   * courierName
   *
   * @var string
   */
  protected $courierName;

  /**
   * currencySymbol
   *
   * @var string
   */
  protected $currencySymbol;

  /**
   * isDropOff
   *
   * @var boolean
   */
  protected $isDropOff;

  /**
   * packages
   *
   * @var array
   */
  protected $packages=array();

  /**
   * address
   *
   * @var array
   */
  protected $address=array();

  /**
   * documents
   *
   * @var array
   */
  protected $documents;

  /**
   * Returns object's Id
   *
   * @param
   *
   * @return object's Id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Sets object's id
   *
   * @param $id
   *
   * @return
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Returns object's courierId
   *
   * @param
   *
   * @return object's courierId
   */
  public function getCourierId()
  {
    return $this->courierId;
  }

  /**
   * Sets object's courierId
   *
   * @param $courierId
   *
   * @return
   */
  public function setCourierId($courierId)
  {
    $this->courierId = $courierId;
  }

  /**
   * Returns object's rateId
   *
   * @param
   *
   * @return object's rateId
   */
  public function getRateId()
  {
    return $this->rateId;
  }

  /**
   * Sets object's rateId
   *
   * @param $rateId
   *
   * @return
   */
  public function setRateId($rateId)
  {
    $this->rateId = $rateId;
  }

  /**
   * Returns object's pickupAt
   *
   * @param
   *
   * @return object's pickupAt
   */
  public function getPickupAt()
  {
    return $this->pickupAt;
  }

  /**
   * Sets object's pickupAt
   *
   * @param $pickupAt
   *
   * @return
   */
  public function setPickupAt($pickupAt)
  {
    $this->pickupAt = $pickupAt;
  }

  /**
   * Returns object's packageCount
   *
   * @param
   *
   * @return object's packageCount
   */
  public function getPackageCount()
  {
    return $this->packageCount;
  }

  /**
   * Sets object's packageCount
   *
   * @param $packageCount
   *
   * @return
   */
  public function setPackageCount($packageCount)
  {
    $this->packageCount = $packageCount;
  }

  /**
   * Returns object's eta
   *
   * @param
   *
   * @return object's eta
   */
  public function getEta()
  {
    return $this->eta;
  }

  /**
   * Sets object's eta
   *
   * @param $eta
   *
   * @return
   */
  public function setEta($eta)
  {
    $this->eta = $eta;
  }

  /**
   * Returns object's trackingNumber
   *
   * @param
   *
   * @return object's trackingNumber
   */
  public function getTrackingNumber()
  {
    return $this->trackingNumber;
  }

  /**
   * Sets object's trackingNumber
   *
   * @param $trackingNumber
   *
   * @return
   */
  public function setTrackingNumber($trackingNumber)
  {
    $this->trackingNumber = $trackingNumber;
  }

  /**
   * Returns object's status
   *
   * @param
   *
   * @return object's status
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Sets object's status
   *
   * @param $status
   *
   * @return
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * Returns object's totalAmount
   *
   * @param
   *
   * @return object's totalAmount
   */
  public function getTotalAmount()
  {
    return $this->totalAmount;
  }

  /**
   * Sets object's totalAmount
   *
   * @param $totalAmount
   *
   * @return
   */
  public function setTotalAmount($totalAmount)
  {
    $this->totalAmount = $totalAmount;
  }

  /**
   * Returns object's currency
   *
   * @param
   *
   * @return object's rateId
   */
  public function getCurrency()
  {
    return $this->currency;
  }

  /**
   * Sets object's currency
   *
   * @param $currency
   *
   * @return
   */
  public function setCurrency($currency)
  {
    $this->currency = $currency;
  }

  /**
   * Returns object's userId
   *
   * @param
   *
   * @return object's userId
   */
  public function getUserId()
  {
    return $this->userId;
  }

  /**
   * Sets object's userId
   *
   * @param $userId
   *
   * @return
   */
  public function setUserId($userId)
  {
    $this->userId = $userId;
  }

  /**
   * Returns object's courierName
   *
   * @param
   *
   * @return object's courierName
   */
  public function getCourierName()
  {
    return $this->courierName;
  }

  /**
   * Sets object's courierName
   *
   * @param $courierName
   *
   * @return
   */
  public function setCourierName($courierName)
  {
    $this->courierName = $courierName;
  }

  /**
   * Returns object's currencySymbol
   *
   * @param
   *
   * @return object's currencySymbol
   */
  public function getCurrencySymbol()
  {
    return $this->currencySymbol;
  }

  /**
   * Sets object's currencySymbol
   *
   * @param $currencySymbol
   *
   * @return
   */
  public function setCurrencySymbol($currencySymbol)
  {
    $this->currencySymbol = $currencySymbol;
  }

  /**
   * Returns object's isDropOff
   *
   * @param
   *
   * @return object's isDropOff
   */
  public function getIsDropOff()
  {
    return $this->isDropOff;
  }

  /**
   * Sets object's isDropOff
   *
   * @param $isDropOff
   *
   * @return
   */
  public function setIsDropOff($isDropOff)
  {
    $this->isDropOff = $isDropOff;
  }

  /**
   * Returns object's packages
   *
   * @param
   *
   * @return object's packages
   */
  public function getPackages()
  {
    return $this->packages;
  }

  /**
   * Sets object's packages
   *
   * @param $packages
   *
   * @return
   */
  public function setPackages($packages)
  {
    $this->packages = $packages;
  }

  /**
   * Adds single package object
   *
   * @param SePackage $package
   *
   * @return
   */
  public function addPackage(SePackage $package)
  {
    array_push($this->packages, $package);
  }

  /**
   * Adds single address object
   *
   * @param $type, SeAddress $address
   *
   * @return
   */
  public function addAddress($type, SeAddress $address)
  {
    $this->address[$type] = $address;
  }

  /**
   * Adds single document object
   *
   * @param SeDocument $document
   *
   * @return
   */
  public function addDocument(SeDocument $document)
  {
    array_push($this->documents, $document);
  }

  /**
   * Returns object's address
   *
   * @param
   *
   * @return object's address
   */
  public function getAddress()
  {
    return $this->address;
  }

  /**
   * Sets object's address
   *
   * @param $address
   *
   * @return
   */
  public function setAddress($address)
  {
    $this->address = $address;
  }

  /**
   * Returns object's documents
   *
   * @param
   *
   * @return object's documents
   */
  public function getDocuments()
  {
    return $this->documents;
  }

  /**
   * Sets object's documents
   *
   * @param $documents
   *
   * @return
   */
  public function setDocuments($documents)
  {
    $this->documents = $documents;
  }

  /**
   * object's constructor
   */
  public function __construct($id=null, $courierId=null, $rateId=null, $pickupAt=null, $packageCount=null,
                              $eta=null, $trackingNumber=null, $status=null, $totalAmount=null,
                              $currency=null, $userId=null, $courierName=null, $currencySymbol=null,
                              $isDropOff=null, $packages=array(), $address=array(), $documents=array())
  {
    $this->setId($id);
    $this->setCourierId($courierId);
    $this->setRateId($rateId);
    $this->setPickupAt($pickupAt);
    $this->setPackageCount($packageCount);
    $this->setEta($eta);
    $this->setTrackingNumber($trackingNumber);
    $this->setStatus($status);
    $this->setTotalAmount($totalAmount);
    $this->setCurrency($currency);
    $this->setUserId($userId);
    $this->setCourierName($courierName);
    $this->setCurrencySymbol($currencySymbol);
    $this->setIsDropOff($isDropOff);
    $this->setPackages($packages);
    $this->setAddress($address);
    $this->setDocuments($documents);
  }
}
?>