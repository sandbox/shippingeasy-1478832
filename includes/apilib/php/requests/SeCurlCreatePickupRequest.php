<?php
SeServiceUtils::checkInclude('SeCurlRequest');

/**
 * SeCurlCreatePickupRequest.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlRequest
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * Changes status of specified shipment(s) into pickup.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlCreatePickupRequest.v.0.1
 */
class SeCurlCreatePickupRequest extends SeCurlRequest
{
  /*
   * Constructor.
   */
  public function __construct($args)
  {
    foreach($args as $arg)
    {
      SeServiceUtils::checkMandatoryParams(
        array(
          'shipments'
        ),
        $arg
      );
    }

    // prepopulate values in constructor
    $this->setHttpMethod('POST');
    $this->setResourceName('pickup');
    $this->setParameters($args);

    // this function is in class SeCurlRequest.
    // if this function is called before Http Method is specifically set, GET will be used by default!!!
    $this->setUrl($this->generateUrl());

    parent::__construct();
  }

  // TODO: this function is for later implementations
  public function build()
  {

  }
}
?>