<?php
SeServiceUtils::checkInclude('SeCurlRequest');
/**
 * SeCurlListExchangeRatesRequest.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlRequest
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of ListExchangeRates request over cURL. Extends SeCurlRequest class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlListExchangeRatesRequest.v.0.1
 */
class SeCurlListExchangeRatesRequest extends SeCurlRequest
{
  // constructor.
  public function __construct($args)
  {
    SeServiceUtils::checkMandatoryParams(
      array(
      ),
      $args
    );

    if (isset($args['Latest']))
    {
      // if suggested parameter is not true, throw server exception because it is the only value allowed.
      if ($args['Latest'] !== true)
      {
        throw new SeServerException('Parameter \'Latest\' must be TRUE if used.');
      }
    }

    // prepopulate values in constructor
    $this->setHttpMethod('GET');
    $this->setResourceName('exchangeRate');
    $this->setParameters($args);

    // this function is in class SeCurlRequest.
    // if this function is called before Http Method is specifically set, GET will be used by default!!!
    $this->setUrl($this->generateUrl());

    parent::__construct();
  }

  // TODO: this function is for later implementations
  public function build()
  {

  }
}
?>