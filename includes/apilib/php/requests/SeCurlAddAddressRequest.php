<?php
SeServiceUtils::checkInclude('SeCurlRequest');
/**
 * SeCurlAddAddressRequest.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlRequest
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * Creates new Address.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlAddAddressRequest.v.0.1
 */
class SeCurlAddAddressRequest extends SeCurlRequest
{
  /*
   * Constructor.
   */
  public function __construct($args)
  {
    SeServiceUtils::checkMandatoryParams(
      array(
      ),
      $args
    );

    // prepopulate values in constructor
    $this->setHttpMethod('POST');
    $this->setResourceName('address');
    $this->setParameters($args);
    $this->setAuth(true);

    // this function is in class SeCurlRequest.
    // if this function is called before Http Method is specifically set, GET will be used by default!!!
    $this->setUrl($this->generateUrl());

    parent::__construct();
  }

  // TODO: this function is for later implementations
  public function build()
  {

  }
}
?>