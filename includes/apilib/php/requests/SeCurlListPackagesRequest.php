<?php
SeServiceUtils::checkInclude('SeCurlRequest');
/**
 * SeCurlListPackagesRequest.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlRequest
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of ListPackages request over cURL. Extends SeCurlRequest class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlListPackagesRequest.v.0.1
 */
class SeCurlListPackagesRequest extends SeCurlRequest
{
  // constructor.
  public function __construct($args)
  {
    SeServiceUtils::checkMandatoryParams(
      array(
      ),
      $args
    );

    // prepopulate values in constructor
    $this->setHttpMethod('GET');
    $this->setResourceName('package');
    $this->setParameters($args);
    $this->setAuth(true);

    // this function is in class SeCurlRequest.
    // if this function is called before Http Method is specifically set, GET will be used by default!!!
    $this->setUrl($this->generateUrl());

    parent::__construct();
  }

  // TODO: this function is for later implementations
  public function build()
  {

  }
}
?>