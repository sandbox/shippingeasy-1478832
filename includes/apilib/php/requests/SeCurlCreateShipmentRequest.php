<?php
SeServiceUtils::checkInclude('SeCurlRequest');

/**
 * SeCurlCreateShipmentRequest.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlRequest
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * Creates new shipment(s).
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlCreateShipmentRequest.v.0.1
 */
class SeCurlCreateShipmentRequest extends SeCurlRequest
{
  /*
   * Constructor.
   * CollectionAddress,DestinationAddress,Packages,Currency,PickupDate,PickupAt,PickupCloseAt,RateId,Courier,
   * ServiceType,Est,HumanEst,Origin parameters are mandatory.
   */
  public function __construct($args)
  {
    foreach($args as $arg)
    {
      SeServiceUtils::checkMandatoryParams(
        array(
          'CollectionAddress',
          'DestinationAddress',
          'Packages',
          'Currency',
          'PickupDate',
          'PickupAt',
          'PickupCloseAt',
          'RateId',
          'Courier',
          'ServiceType',
          'Est',
          'HumanEst',
          'Origin'
        ),
        $arg
      );
    }

    // prepopulate values in constructor
    $this->setHttpMethod('POST');
    $this->setResourceName('shipment');
    $this->setParameters($args);
    $this->setMaxExecutionTime(60);
    $this->setBody(json_encode($this->getParameters()));
    $this->setAuth(true);

    $this->addHeaderItem('Content-Type', 'application/json');
    //$this->addHeaderItem('Content-Length', strlen($this->getBody()));

    // this function is in class SeCurlRequest.
    // if this function is called before Http Method is specifically set, GET will be used by default!!!
    $this->setUrl($this->generateUrl());

    parent::__construct();
  }

  // TODO: this function is for later implementations
  public function build()
  {

  }
}
?>