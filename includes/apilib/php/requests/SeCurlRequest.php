<?php
SeServiceUtils::checkInclude('SeServiceRequest');
/**
 * SeCurlRequest.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeRequest
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents base class for all curl requests. It extends SeServiceRequest class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlRequest.v.0.1
 */
class SeCurlRequest extends SeServiceRequest
{
  /**
   * resourceName for request. It is a request URL extension after the base endpoint url.
   *
   * @var string
   */
  protected $resourceName;

  /**
   * Url for request.
   *
   * @var string
   */
  protected $url;

  /**
   * HTTP Method for request (POST, GET etc...). GET by default.
   *
   * @var string
   */
  protected $httpMethod = 'GET';

  /**
   * This property is used to determine whether headers should be used in curl request. FALSE by default.
   *
   * @var bool
   */
  protected $includeHeader = false;

  /**
   * This property is used to store parameters for curl request http method.
   *
   * @var array
   */
  protected $parameters = array();

  /**
   * Timeout for request in seconds. 10 seconds by default.
   *
   * @var integer
   */
  protected $maxExecutionTime = 10;

  /**
   * Switch to decide whether to use http_build_query to concatenate standard parameters in URL,
   * or to use different logic. This switch is used in generateUrl() method.
   *
   * @var boolean
   */
  protected $shouldBuildParameters = true;

  /**
   * TODO: This property is used to handle authorization.
   *
   * @var array
   */
  protected $auth = false;

  /**
   * Resource name setter.
   *
   * @param resourceName
   *
   * @return
   */
  public function setResourceName($resourceName)
  {
    $this->resourceName = $resourceName;
  }

  /**
   * Resource name getter.
   *
   * @param
   *
   * @return resourceName
   */
  public function getResourceName()
  {
    return $this->resourceName;
  }

  /**
   * Url setter.
   *
   * @param url
   *
   * @return
   */
  public function setUrl($url)
  {
    $this->url = $url;
  }

  /**
   * Url getter.
   *
   * @param
   *
   * @return Url
   */
  public function getUrl()
  {
    return $this->url;
  }

  /**
   * Http Method setter.
   *
   * @param httpMethod
   *
   * @return
   */
  public function setHttpMethod($httpMethod)
  {
    $this->httpMethod = $httpMethod;
  }

  /**
   * Http Method getter.
   *
   * @param
   *
   * @return httpMethod
   */
  public function getHttpMethod()
  {
    return $this->httpMethod;
  }

  /**
   * Url parameters setter.
   *
   * @param name, value
   *
   * @return
   */
  public function setUrlParam($name, $value)
  {
    $this->parameters[$name] = $value;
  }

  /**
   * Parameters array setter.
   *
   * @param parameters
   *
   * @return
   */
  public function setParameters($parameters)
  {
    $this->parameters = $parameters;
  }

  /**
   * Url parameters getter.
   *
   * @param
   *
   * @return parameters
   */
  public function getParameters()
  {
    return $this->parameters;
  }

  /**
   * Include Header setter.
   *
   * @param include
   *
   * @return
   */
  public function setIncludeHeader($include)
  {
    $this->includeHeader = $include;
  }

  /**
   * Include Header getter.
   *
   * @param
   *
   * @return includeHeader
   */
  public function getIncludeHeader()
  {
    return $this->includeHeader;
  }

  /**
   * maxExecutionTime setter.
   *
   * @param timeout
   *
   * @return
   */
  public function setMaxExecutionTime($timeout)
  {
    $this->maxExecutionTime = $timeout;
  }

  /**
   * maxExecutionTime getter.
   *
   * @param
   *
   * @return maxExecutionTime
   */
  public function getMaxExecutionTime()
  {
    return $this->maxExecutionTime;
  }

  /**
   * shouldBuildParameters setter.
   *
   * @param shouldBuildParameters
   *
   * @return
   */
  public function setShouldBuildParameters($shouldBuildParameters)
  {
    $this->shouldBuildParameters = $shouldBuildParameters;
  }

  /**
   * shouldBuildParameters getter.
   *
   * @param
   *
   * @return shouldBuildParameters
   */
  public function getShouldBuildParameters()
  {
    return $this->shouldBuildParameters;
  }

  /**
   * auth setter.
   *
   * @param auth
   *
   * @return
   */
  public function setAuth($auth)
  {
    $this->auth = $auth;
  }

  /**
   * auth getter.
   *
   * @param
   *
   * @return auth
   */
  public function getAuth()
  {
    return $this->auth;
  }

  /**
   * this function generates absolute Url for each curl request.
   * if this function is called before Http Method is specifically set, GET will be used by default!!!
   *
   * @param array $params
   *
   * @return url
   */
  public function generateUrl($params=null)
  {
    // set starting Url result
    $result = ENTRYPOINT_BASEPATH.$this->getResourceName();

    if ($this->getShouldBuildParameters())
    {
      if (count($this->getParameters()) > 0 && $this->getHttpMethod() == 'GET')
      {
        $result = $result . '?' . http_build_query($this->getParameters(), '', '&');
      }
    }
    else
    {
      if (count($this->getParameters()) > 0 && ($this->getHttpMethod() == 'GET' ||
                                                $this->getHttpMethod() == 'PUT' ||
                                                $this->getHttpMethod() == 'DELETE'))
      {
        if (is_null($params))
        {
          foreach($this->getParameters() as $key=>$value)
          {
            $result = $result . '/' . $value;
          }
        }
        else
        {
          $parameters = $this->getParameters();

          foreach($params as $param)
          {
            if (isset($parameters[$param]))
            {
              $result = $result . '/' . $parameters[$param];
            }
          }
        }
      }
    }

    return $result;
  }

  /**
   * This function generates headers for curl request.
   *
   * This function must be called after setHeaders() is called, or headers will not be generated.
   *
   * @param
   *
   * @return array
   */
  public function generateHeaders()
  {
    $result = array();

    if (count($this->getHeaders()) > 0)
    {
      foreach ($this->getHeaders() as $key=>$value)
      {
        $resultItem = $key . ': ' . $value;

        array_push($result, $resultItem);
      }
    }

    return $result;
  }

  public function __construct()
  {
    if ($this->getAuth())
    {
      $this->addHeaderItem('X-ShippingEasy-API-Key', API_KEY);
    }

    // this header item determines which application is using apilib
    // it should be implemented in header of each request
    if (trim(ORIGIN) != '')
    {
      $this->addHeaderItem('X-ShippingEasy-Channel', ORIGIN);
    }

    // this header item determines which application is using apilib
    // it should be implemented in header of each request
    if (TEST_MODE)
    {
      $this->addHeaderItem('X-ShippingEasy-TestMode', 1);
    }
  }
}
?>