<?php
SeServiceUtils::checkInclude(array('SeResourceName', 'SeServiceType', 'SeServerException'));
/**
 * SeRequestFactory.php.
 *
 * PHP Version 5.3.1
 *
 * @category  Request
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation to determine which request should be instantiated and returned.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeRequestFactory.v.0.1
 */
class SeRequestFactory
{
  public static function create($resourceName, $serviceType=SeServiceType::Curl, $args)
  {
    // class name concatenation
    $className = 'Se'.$serviceType.$resourceName.'Request';

    try
    {
      SeServiceUtils::checkInclude($className);

      return new $className($args);
    }
    catch (Exception $e)
    {
      throw new SeServerException($e->getMessage() . ' (' . $className . ').');
    }
  }
}
?>