<?php
SeServiceUtils::checkInclude('SeCurlRequest');
/**
 * SeCurlGetRateRequest.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlRequest
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * Returns list of courier services that can be used for delivery of specified shipment.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetRateRequest.v.0.1
 */
class SeCurlGetRateRequest extends SeCurlRequest
{
  /*
   * Constructor.
   *
   * CollectionCountry, CollectionCity, CollectionZip, DestinationCountry,
   * DestinationCity, DestinationZip parameters are mandatory.
   *
   * PackageX, FlatRatePackageIdX, PredefinedSizeIdX are arrays which must be parsed and removed
   * from args array after parsing.
   */
  public function __construct($args)
  {
    SeServiceUtils::checkMandatoryParams(
      array(
        'CollectionCountry',
        'CollectionCity',
        'CollectionZip',
        'DestinationCountry',
        'DestinationCity',
        'DestinationZip',
        'Date',
        'Today'
      ),
      $args
    );

    // prepopulate values in constructor
    $this->setHttpMethod('GET');
    $this->setResourceName('rate');
    $this->setParameters($args);
    $this->setMaxExecutionTime(60);

    if (isset($args['Auth']))
    {
      $this->setAuth(true);
    }

    // this function is in class SeCurlRequest.
    // if this function is called before Http Method is specifically set, GET will be used by default!!!
    $this->setUrl($this->generateUrl());

    parent::__construct();
  }

  // TODO: this function is for later implementations
  public function build()
  {

  }
}
?>