<?php
SeServiceUtils::checkInclude('SeCurlRequest');
/**
 * SeCurlGetCourierRequest.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeCurlRequest
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of GetCourier request over cURL. Extends SeCurlRequest class.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetCourierRequest.v.0.1
 */
class SeCurlGetCourierRequest extends SeCurlRequest
{
  // constructor. Id parameter is mandatory.
  public function __construct($args)
  {
    SeServiceUtils::checkMandatoryParams(
      array(
        'Id'
      ),
      $args
    );

    // if keyword parameter is empty string, throw server exception.
    if (trim($args['Id']) == '')
    {
      throw new SeServerException('Id cannot be empty string.');
    }

    // prepopulate values in constructor
    $this->setHttpMethod('GET');
    $this->setResourceName('courier');
    $this->setParameters($args);
    $this->setShouldBuildParameters(false);

    // this function is in class SeCurlRequest.
    // if this function is called before Http Method is specifically set, GET will be used by default!!!
    $this->setUrl($this->generateUrl());

    parent::__construct();
  }

  // TODO: this function is for later implementations
  public function build()
  {

  }
}
?>