<?php
SeServiceUtils::checkInclude(array(
  'SeBaseBuilder',
  'SePackageInquiry'
  )
);

/**
 * SePackageBuilder.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents builder for SePackageInquiry
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SePackageBuilder.v.0.1
 */
class SePackageBuilder extends SeBaseBuilder
{
  /**
   * Value of the package.
   *
   * @var numeric
   */
  protected $valueAmount;

  /**
   * Currency in which the package value is expressed.
   *
   * @var string
   */
  protected $valueCurrency;

  /**
   * Weight of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $weight;

  /**
   * Length of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $length;

  /**
   * Width of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $width;

  /**
   * Height of the package. This parameter is used for custom size packages.
   *
   * @var string
   */
  protected $height;

  /**
   * Id of the standard size package. This parameter is used when standard size package is selected and
   * Type of the selected standard size package is predefined.
   *
   * @var numeric
   */
  protected $predefinedSizeId;

  /**
   * Id of the standard size package. This parameter is used when standard size package is selected and
   * Type of the selected standard size package is flatRate.
   *
   * @var string
   */
  protected $flatRatePackageIdX;

  /**
   * Indicates if the package contains restricted items.
   *
   * @var boolean
   */
  protected $containsRestrictedItems;

  /**
   * Indicates if metric system of units is used.
   *
   * @var boolean
   */
  protected $isMetric;

  /**
   * Package type as returned by packageType API.
   *
   * @var string
   */
  protected $packageType;

  /**
   * Amount of additional insurance. This parameter is used when user selects additional insurance.
   *
   * @var string
   */
  protected $additionalInsuranceAmount;

  /**
   * Currency of the additional insurance. This parameter is used when user selects additional insurance.
   *
   * @var string
   */
  protected $additionalInsuranceCurrency;

  /**
   * This is an array of items contained in a package. The Items array can contain any number of items.
   * This parameter is optional.
   *
   * @var array SePackageInquiryItem
   */
  protected $items = array();

  /**
   * Create details from SeRateInquiry package object
   *
   * @param mixed $package
   * @return SeCreateShipmentBuilder
   */
  public function fromRateInquiryPackage($package)
  {
    switch (get_class($package))
    {
      case 'SeRateInquiryPredefinedSizeIdX':
        $this->predefinedSizeId = $package->getValue();
      break;
      case 'SeRateInquiryPackageX':
        $items = explode('x', $package->getSize());

        $this->weight = $items[0];
        $this->height = $items[1];
        $this->width = $items[2];

        $last = explode('-', $items[3]);
        $this->length = $last[0];

        if ($last[1] == 'metric')
        {
          $this->isMetric = true;
        }
        else
        {
          $this->isMetric = false;
        }
      break;
      case 'SeRateInquiryFlatRatePackageIdX':
        $this->flatRatePackageIdX = $package->getValue();
      break;
    }

    return $this;
  }

  /**
   * add Value
   *
   * @param $amount, $currency
   * @return SeCreateShipmentBuilder
   */
  public function addValueAmountAndCurrency($amount, $currency)
  {
    $this->valueAmount = $amount;
    $this->valueCurrency = $currency;

    return $this;
  }

  /*
   * add ValueAmount
   *
   * @param $valueAmount
   *
   * @return SePackageBuilder
   */
  public function addValueAmount($valueAmount)
  {
    $this->valueAmount = $valueAmount;

    return $this;
  }

  /**
   * add ValueCurrency
   *
   * @param $valueCurrency
   *
   * @return SePackageBuilder
   */
  public function addValueCurrency($valueCurrency)
  {
    $this->valueCurrency = $valueCurrency;

    return $this;
  }

  /**
   * add Weight
   *
   * @param $weight
   *
   * @return SePackageBuilder
   */
  public function addWeight($weight)
  {
    $this->weight = $weight;

    return $this;
  }

  /**
   * add Length
   *
   * @param $length
   *
   * @return SePackageBuilder
   */
  public function addLength($length)
  {
    $this->length = $length;

    return $this;
  }

  /**
   * add Width
   *
   * @param $width
   *
   * @return SePackageBuilder
   */
  public function addWidth($width)
  {
    $this->width = $width;

    return $this;
  }

  /**
   * add Height
   *
   * @param $height
   *
   * @return SePackageBuilder
   */
  public function addHeight($height)
  {
    $this->height = $height;

    return $this;
  }

  /**
   * add PredefinedSizeId
   *
   * @param $predefinedSizeId
   *
   * @return SePackageBuilder
   */
  public function addPredefinedSizeId($predefinedSizeId)
  {
    $this->predefinedSizeId = $predefinedSizeId;

    return $this;
  }

  /**
   * add FlatRatePackageIdX
   *
   * @param $flatRatePackageIdX
   *
   * @return SePackageBuilder
   */
  public function addFlatRatePackageIdX($flatRatePackageIdX)
  {
    $this->flatRatePackageIdX = $flatRatePackageIdX;

    return $this;
  }

  /**
   * add containsRestrictedItems
   *
   * @param $containsRestrictedItems
   *
   * @return SePackageBuilder
   */
  public function addContainsRestrictedItems($containsRestrictedItems)
  {
    $this->containsRestrictedItems = $containsRestrictedItems;

    return $this;
  }

  /**
   * add IsMetric
   *
   * @param $isMetric
   *
   * @return SePackageBuilder
   */
  public function addIsMetric($isMetric)
  {
    $this->isMetric = $isMetric;

    return $this;
  }

  /**
   * add PackageType
   *
   * @param $packageType
   *
   * @return SePackageBuilder
   */
  public function addPackageType($packageType)
  {
    $this->packageType = $packageType;

    return $this;
  }

  /**
   * add AdditionalInsuranceAmount
   *
   * @param $additionalInsuranceAmount
   *
   * @return SePackageBuilder
   */
  public function addAdditionalInsuranceAmount($additionalInsuranceAmount)
  {
    $this->additionalInsuranceAmount = $additionalInsuranceAmount;

    return $this;
  }

  /**
   * add AdditionalInsuranceCurrency
   *
   * @param $additionalInsuranceCurrency
   *
   * @return SePackageBuilder
   */
  public function addAdditionalInsuranceCurrency($additionalInsuranceCurrency)
  {
    $this->additionalInsuranceCurrency = $additionalInsuranceCurrency;

    return $this;
  }

  /**
   * add Items
   *
   * @param SePackageInquiryItem $item
   *
   * @return SePackageBuilder
   */
  public function addItem(SePackageInquiryItem $item)
  {
    array_push($this->items, $item);

    return $this;
  }

  /**
   * Create SePackageInquiry
   *
   * @return SePackageInquiry
   */
  public function build()
  {
    return new SePackageInquiry($this->valueAmount, $this->valueCurrency, $this->weight,
                             $this->length, $this->width, $this->height, $this->predefinedSizeId,
                             $this->flatRatePackageIdX, $this->containsRestrictedItems,
                             $this->isMetric, $this->packageType, $this->additionalInsuranceAmount,
                             $this->additionalInsuranceCurrency, $this->items
    );
  }
}
?>