<?php
SeServiceUtils::checkInclude(array(
  'SeBaseBuilder',
  'SeAddress'
  )
);

class SeAddressBuilder extends SeBaseBuilder
{
  /**
   * name
   *
   * @var string
   */
  protected $name;

  /**
   * description
   *
   * @var string
   */
  protected $description;

  /**
    * isDefault
    *
    * @var boolean
    */
  protected $isDefault;

  /**
    * addressType
    *
    * @var string
    */
  protected $addressType;

  /**
    * type
    *
    * @var string
    */
  protected $type;

  /**
    * firstName
    *
    * @var string
    */
  protected $firstName;

  /**
    * lastName
    *
    * @var string
    */
  protected $lastName;

  /**
    * companyName
    *
    * @var string
    */
  protected $companyName;

  /**
    * line1
    *
    * @var string
    */
  protected $line1;

  /**
    * line2
    *
    * @var string
    */
  protected $line2;

  /**
    * postalCode
    *
    * @var string
    */
  protected $postalCode;

  /**
    * city
    *
    * @var string
    */
  protected $city;

  /**
    * country
    *
    * @var string
    */
  protected $country;

  /**
    * telephoneAreaCode
    *
    * @var string
    */
  protected $telephoneAreaCode;

  /**
    * telephoneNumber
    *
    * @var string
    */
  protected $telephoneNumber;

  /**
    * emailAddress
    *
    * @var string
    */
  protected $emailAddress;

  /**
    * countryDivision
    *
    * @var string
    */
  protected $countryDivision;

  /**
    * countryDivisionId
    *
    * @var string
    */
  protected $countryDivisionId;

  /**
    * isResidential
    *
    * @var boolean
    */
  protected $isResidential;

  /**
   * Create name
   *
   * @param $name
   * @return SeAddressBuilder
   */
  public function addName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Create details from SeRateInquiryAddress object
   *
   * @param SeRateInquiryAddress $seRateInquiryAddress
   * @return SeCreateShipmentBuilder
   */
  public function fromRateInquiryAddress(SeRateInquiryAddress $seRateInquiryAddress)
  {
    $this->country = $seRateInquiryAddress->getCountry();
    $this->city = $seRateInquiryAddress->getCity();
    $this->postalCode = $seRateInquiryAddress->getZip();

    return $this;
  }

  /**
   * Create description
   *
   * @param $description
   * @return SeAddressBuilder
   */
  public function addDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * Create isDefault
   *
   * @param $isDefault
   * @return SeAddressBuilder
   */
  public function addIsDefault($isDefault)
  {
    $this->isDefault = $isDefault;

    return $this;
  }

  /**
   * Create addressType
   *
   * @param $addressType
   * @return SeAddressBuilder
   */
  public function addAddressType($addressType)
  {
    $this->addressType = $addressType;

    return $this;
  }

  /**
   * Create type
   *
   * @param $type
   * @return SeAddressBuilder
   */
  public function addType($type)
  {
    $this->type = $type;

    return $this;
  }

  /**
   * Create firstName
   *
   * @param $firstName
   * @return SeAddressBuilder
   */
  public function addFirstName($firstName)
  {
    $this->firstName = $firstName;

    return $this;
  }

  /**
   * Create lastName
   *
   * @param $lastName
   * @return SeAddressBuilder
   */
  public function addLastName($lastName)
  {
    $this->lastName = $lastName;

    return $this;
  }

  /**
   * Create fullName
   *
   * @param $fullName
   * @return SeAddressBuilder
   */
  public function addFullName($firstName, $lastName)
  {
    $this->firstName = $firstName;
    $this->lastName = $lastName;

    return $this;
  }

  /**
   * Create companyName
   *
   * @param $companyName
   * @return SeAddressBuilder
   */
  public function addCompanyName($companyName)
  {
    $this->companyName = $companyName;

    return $this;
  }

  /**
   * Create line1
   *
   * @param $line1
   * @return SeAddressBuilder
   */
  public function addLine1($line1)
  {
    $this->line1 = $line1;

    return $this;
  }

  /**
   * Create line2
   *
   * @param $line2
   * @return SeAddressBuilder
   */
  public function addLine2($line2)
  {
    $this->line2 = $line2;

    return $this;
  }

  /**
   * Create addressLines
   *
   * @param $line1, $line2
   * @return SeAddressBuilder
   */
  public function addAddressLines($line1, $line2=null)
  {
    $this->line1 = $line1;

    if(!is_null($line2))
    {
      $this->line2 = $line2;
    }

    return $this;
  }

  /**
   * Create userAddress
   *
   * @param $line1, $line2, $countryDivision
   * @return SeAddressBuilder
   */
  public function addUserAddress($line1, $line2=null, $countryDivision=null)
  {
    $this->line1 = $line1;

    if(!is_null($line2))
    {
      $this->line2 = $line2;
    }

    if(!is_null($countryDivision))
    {
      $this->countryDivision = $countryDivision;
    }

    return $this;
  }

  /**
   * Create postalCode
   *
   * @param $postalCode
   * @return SeAddressBuilder
   */
  public function addPostalCode($postalCode)
  {
    $this->postalCode = $postalCode;

    return $this;
  }

  /**
   * Create postalCode
   *
   * @param $postalCode
   * @return SeAddressBuilder
   */
  public function addCity($city)
  {
    $this->city = $city;

    return $this;
  }

  /**
   * Create country
   *
   * @param $country
   * @return SeAddressBuilder
   */
  public function addCountry($country)
  {
    $this->country = $country;

    return $this;
  }

  /**
   * Create telephoneAreaCode
   *
   * @param $telephoneAreaCode
   * @return SeAddressBuilder
   */
  public function addTelephoneAreaCode($telephoneAreaCode)
  {
    $this->telephoneAreaCode = $telephoneAreaCode;

    return $this;
  }

  /**
   * Create telephoneNumber
   *
   * @param $telephoneNumber
   * @return SeAddressBuilder
   */
  public function addTelephoneNumber($telephoneNumber)
  {
    $this->telephoneNumber = $telephoneNumber;

    return $this;
  }

  /**
   * Create telephone
   *
   * @param $telephoneAreaCode, $telephoneNumber
   * @return SeAddressBuilder
   */
  public function addTelephone($telephoneAreaCode, $telephoneNumber)
  {
    $this->telephoneAreaCode = $telephoneAreaCode;
    $this->telephoneNumber = $telephoneNumber;

    return $this;
  }

  /**
   * Create emailAddress
   *
   * @param $emailAddress
   * @return SeAddressBuilder
   */
  public function addEmailAddress($emailAddress)
  {
    $this->emailAddress = $emailAddress;

    return $this;
  }

  /**
   * Create countryDivision
   *
   * @param $countryDivision
   * @return SeAddressBuilder
   */
  public function addCountryDivision($countryDivision)
  {
    $this->countryDivision = $countryDivision;

    return $this;
  }

  /**
   * Create countryDivisionId
   *
   * @param $countryDivisionId
   * @return SeAddressBuilder
   */
  public function addCountryDivisionId($countryDivisionId)
  {
    $this->countryDivisionId = $countryDivisionId;

    return $this;
  }

  /**
   * Create isResidential
   *
   * @param $isResidential
   * @return SeAddressBuilder
   */
  public function addIsResidential($isResidential)
  {
    $this->isResidential = $isResidential;

    return $this;
  }

  /**
   * Create SeAddress
   *
   * @return SeAddress
   */
  public function build()
  {
    return new SeAddress(null, $this->name, $this->description, $this->isDefault,
                         $this->addressType, $this->type, $this->firstName,
                         $this->lastName, $this->companyName, $this->line1, $this->line2,
                         $this->postalCode, $this->city, $this->country,
                         $this->telephoneAreaCode, $this->telephoneNumber,
                         $this->emailAddress, $this->countryDivision, $this->countryDivisionId,
                         null, $this->isResidential
    );
  }
}
?>