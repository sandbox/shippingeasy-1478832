<?php
SeServiceUtils::checkInclude(array(
  'SeBaseBuilder',
  'SePackageInquiryItem'
  )
);

class SePackageItemBuilder extends SeBaseBuilder
{
  /**
   * description
   *
   * @var string
   */
  protected $description;

  /**
   * serialNumber
   *
   * @var boolean
   */
  protected $serialNumber;

  /**
    * hsCode
    *
    * @var string
    */
  protected $hsCode;

  /**
    * valueAmount
    *
    * @var numeric
    */
  protected $valueAmount;

  /**
    * valueCurrency
    *
    * @var string
    */
  protected $valueCurrency;

  /**
    * comments
    *
    * @var string
    */
  protected $comments;

  /**
    * quantity
    *
    * @var numeric
    */
  protected $quantity;

  /**
    * unitType
    *
    * @var string
    */
  protected $unitType;

  /**
    * manufactureCountry
    *
    * @var string
    */
  protected $manufactureCountry;

  /**
    * originalId
    *
    * @var numeric
    */
  protected $originalId;

  /**
    * originalTransactionId
    *
    * @var numeric
    */
  protected $originalTransactionId;

  /**
   * Create serialNumber
   *
   * @param $serialNumber
   * @return SePackageItemBuilder
   */
  public function addSerialNumber($serialNumber)
  {
    $this->serialNumber = $serialNumber;

    return $this;
  }

  /**
   * Create description
   *
   * @param $description
   * @return SePackageItemBuilder
   */
  public function addDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * Create hsCode
   *
   * @param $hsCode
   * @return SePackageItemBuilder
   */
  public function addHsCode($hsCode)
  {
    $this->hsCode = $hsCode;

    return $this;
  }

  /**
   * Create valueAmount
   *
   * @param $valueAmount
   * @return SePackageItemBuilder
   */
  public function addValueAmount($valueAmount)
  {
    $this->valueAmount = $valueAmount;

    return $this;
  }

  /**
   * Create valueCurrency
   *
   * @param $calueCurrency
   * @return SePackageItemBuilder
   */
  public function addValueCurrency($valueCurrency)
  {
    $this->valueCurrency = $valueCurrency;

    return $this;
  }

  /**
   * Create valueAmountAndCurrency
   *
   * @param $valueAmount, $valueCurrency
   * @return SePackageItemBuilder
   */
  public function addValueAmountAndCurrency($valueAmount, $valueCurrency)
  {
    $this->valueAmount = $valueAmount;
    $this->valueCurrency = $valueCurrency;

    return $this;
  }

  /**
   * Create comments
   *
   * @param $comments
   * @return SePackageItemBuilder
   */
  public function addComments($comments)
  {
    $this->comments = $comments;

    return $this;
  }

  /**
   * Create quantity
   *
   * @param $quantity
   * @return SePackageItemBuilder
   */
  public function addQuantity($quantity)
  {
    $this->quantity = $quantity;

    return $this;
  }

  /**
   * Create unitType
   *
   * @param $unitType
   * @return SePackageItemBuilder
   */
  public function addUnitType($unitType)
  {
    $this->unitType = $unitType;

    return $this;
  }

  /**
   * Create manufactureCountry
   *
   * @param $manufactureCountry
   * @return SePackageItemBuilder
   */
  public function addManufactureCountry($manufactureCountry)
  {
    $this->manufactureCountry = $manufactureCountry;

    return $this;
  }

  /**
   * Create originalId
   *
   * @param $originalId
   * @return SePackageItemBuilder
   */
  public function addOriginalId($originalId)
  {
    $this->originalId = $originalId;

    return $this;
  }

  /**
   * Create originalTransactionId
   *
   * @param $originalTransactionId
   * @return SePackageItemBuilder
   */
  public function addOriginalTransactionId($originalTransactionId)
  {
    $this->originalTransactionId = $originalTransactionId;

    return $this;
  }

 /**
   * Create SePackageItemInquiry
   *
   * @return SePackageItemInquiry
   */
  public function build()
  {
    return new SePackageInquiryItem($this->description, $this->serialNumber, $this->hsCode,
                                    $this->valueAmount, $this->valueCurrency, $this->comments,
                                    $this->quantity, $this->unitType, $this->manufactureCountry,
                                    $this->originalId, $this->originalTransactionId
    );
  }
}
?>