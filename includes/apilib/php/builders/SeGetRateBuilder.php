<?php
SeServiceUtils::checkInclude(array(
  'SeBaseBuilder',
  'SeRateInquiryAddress',
  'SeRateInquiry',
  'SeRateInquiryFlatRatePackageIdX',
  'SeRateInquiryPackageX',
  'SeRateInquiryPredefinedSizeIdX',
  )
);

class SeGetRateBuilder extends SeBaseBuilder
{
  /**
   * collectionAddress
   *
   * @var SeRateInquiryAddress
   */
  protected $collectionAddress;

  /**
   * destinationAddress
   *
   * @var SeRateInquiryAddress
   */
  protected $destinationAddress;

  /**
    * collectionUserAddressId
    *
    * @var numeric optional
    */
  protected $collectionUserAddressId;

  /**
    * destinationUserAddressId
    *
    * @var numeric optional
    */
  protected $destinationUserAddressId;

  /**
    * date
    *
    * @var string
    */
  protected $date;

  /**
    * courierId
    *
    * @var numeric
    */
  protected $courierId;

  /**
    * serviceId
    *
    * @var numeric
    */
  protected $serviceId;

  /**
    * pickupOnly
    *
    * @var boolean optional
    */
  protected $pickupOnly=false;

  /**
    * dropOffOnly
    *
    * @var boolean optional
    */
  protected $dropOffOnly=false;

  /**
    * cheapest
    *
    * @var boolean optional
    */
  protected $cheapest=false;

  /**
    * longest
    *
    * @var boolean optional
    */
  protected $longest=false;

  /**
    * shortest
    *
    * @var boolean optional
    */
  protected $shortest=false;

  /**
    * ignoreWorkingHours
    *
    * @var boolean optional
    */
  protected $ignoreWorkingHours=false;

  /**
    * signatureConfirmation
    *
    * @var boolean optional
    */
  protected $signatureConfirmation;

  /**
    * today
    *
    * @var numeric
    */
  protected $today;

  /**
    * currency
    *
    * @var string optional
    */
  protected $currency;

  /**
    * residential
    *
    * @var boolean optional
    */
  protected $residential;

  /**
    * packageX
    *
    * @var array optional
    */
  protected $packageX = array();

  /**
    * flatRatePackageIdX
    *
    * @var array optional
    */
  protected $flatRatePackageIdX = array();

  /**
    * predefinedSizeIdX
    *
    * @var array optional
    */
  protected $predefinedSizeIdX = array();

  /**
    * auth
    *
    * @var boolean optional
    */
  protected $auth=false;

  /**
   * Create collectionAddress
   *
   * @param SeRateInquiryAddress $collectionAddress
   * @return SeGetRateBuilder
   */
  public function addCollectionAddress(SeRateInquiryAddress $collectionAddress)
  {
    $this->collectionAddress = $collectionAddress;

    return $this;
  }

  /**
   * Create destinationAddress
   *
   * @param SeRateInquiryAddress $destinationAddress
   * @return SeGetRateBuilder
   */
  public function addDestinationAddress(SeRateInquiryAddress $destinationAddress)
  {
    $this->destinationAddress = $destinationAddress;

    return $this;
  }

  /**
   * Create collectionUserAddressId
   *
   * @param $collectionUserAddressId
   * @return SeGetRateBuilder
   */
  public function addCollectionUserAddressId($collectionUserAddressId)
  {
    $this->collectionUserAddressId = $collectionUserAddressId;

    return $this;
  }

  /**
   * Create destinationUserAddressId
   *
   * @param $destinationUserAddressId
   * @return SeGetRateBuilder
   */
  public function addDestinationUserAddressId($destinationUserAddressId)
  {
    $this->destinationUserAddressId = $destinationUserAddressId;

    return $this;
  }

  /**
   * Create date
   *
   * @param $date
   * @return SeGetRateBuilder
   */
  public function addDate($date)
  {
    $this->date = $date;

    return $this;
  }

  /**
   * Create courierId
   *
   * @param $courierId
   * @return SeGetRateBuilder
   */
  public function addCourierId($courierId)
  {
    $this->courierId = $courierId;

    return $this;
  }

  /**
   * Create serviceId
   *
   * @param $serviceId
   * @return SeGetRateBuilder
   */
  public function addServiceId($serviceId)
  {
    $this->serviceId = $serviceId;

    return $this;
  }

  /**
   * Create pickupOnly
   *
   * @param
   * @return SeGetRateBuilder
   */
  public function addPickupOnly()
  {
    $this->pickupOnly = true;

    return $this;
  }

  /**
   * Create dropOffOnly
   *
   * @param
   * @return SeGetRateBuilder
   */
  public function addDropOffOnly()
  {
    $this->dropOffOnly = true;

    return $this;
  }

  /**
   * Create cheapest
   *
   * @param
   * @return SeGetRateBuilder
   */
  public function addCheapest()
  {
    $this->cheapest = true;

    return $this;
  }

  /**
   * Create shortest
   *
   * @param
   * @return SeGetRateBuilder
   */
  public function addShortest()
  {
    $this->shortest = true;

    return $this;
  }

  /**
   * Create longest
   *
   * @param
   * @return SeGetRateBuilder
   */
  public function addLongest()
  {
    $this->longest = true;

    return $this;
  }

  /**
   * Create ignoreWorkingHours
   *
   * @param
   * @return SeGetRateBuilder
   */
  public function addIgnoreWorkingHours()
  {
    $this->ignoreWorkingHours = true;

    return $this;
  }

  /**
   * Create currency
   *
   * @param $currency
   * @return SeGetRateBuilder
   */
  public function addCurrency($currency)
  {
    $this->currency = $currency;

    return $this;
  }

  /**
   * Create auth
   *
   * @param
   * @return SeGetRateBuilder
   */
  public function addAuth()
  {
    $this->auth = true;

    return $this;
  }

  /**
   * Create flags
   *
   * @param $courierId, $serviceId, $pickupOnly, $dropOffOnly, $cheapest, $shortest, $longest
   * @return SeGetRateBuilder
   */
  public function addFlags($courierId=null, $serviceId=null, $pickupOnly=false, $dropOffOnly=false,
                           $cheapest=false, $shortest=false, $longest=false)
  {
    $this->courierId = $courierId;
    $this->serviceId = $serviceId;
    $this->pickupOnly = $pickupOnly;
    $this->dropOffOnly = $dropOffOnly;
    $this->cheapest = $cheapest;
    $this->shortest = $shortest;
    $this->longest = $longest;

    return $this;
  }

  /**
   * Create signatureConfirmation
   *
   * @param $signatureConfirmation
   * @return SeGetRateBuilder
   */
  public function addSignatureConfirmation($signatureConfirmation)
  {
    $this->signatureConfirmation = $signatureConfirmation;

    return $this;
  }

  /**
   * Create today
   *
   * @param $today
   * @return SeGetRateBuilder
   */
  public function addToday($today)
  {
    $this->today = $today;

    return $this;
  }

  /**
   * Create dateAndToday
   *
   * @param $date, $today
   * @return SeGetRateBuilder
   */
  public function addDateAndToday($date, $today)
  {
    $this->date = $date;
    $this->today = $today;

    return $this;
  }

  /**
   * Create residential
   *
   * @param $residential
   * @return SeGetRateBuilder
   */
  public function addResidential($residential)
  {
    $this->residential = $residential;

    return $this;
  }

  /**
   * Create packageX
   *
   * @param SeRateInquiryPackageX $packageX
   * @return SeGetRateBuilder
   */
  public function addPackageX(SeRateInquiryPackageX $packageX)
  {
    array_push($this->packageX, $packageX);

    return $this;
  }

  /**
   * Create flatRatePackageIdX
   *
   * @param SeRateInquiryFlatRatePackageIdX $flatRatePackageIdX
   * @return SeGetRateBuilder
   */
  public function addFlatRatePackageIdX(SeRateInquiryFlatRatePackageIdX $flatRatePackageIdX)
  {
    array_push($this->flatRatePackageIdX, $flatRatePackageIdX);

    return $this;
  }

  /**
   * Create predefinedSizeIdX
   *
   * @param SeRateInquiryPredefinedSizeIdX $predefinedSizeIdX
   * @return SeGetRateBuilder
   */
  public function addPredefinedSizeIdX(SeRateInquiryPredefinedSizeIdX $predefinedSizeIdX)
  {
    array_push($this->predefinedSizeIdX, $predefinedSizeIdX);

    return $this;
  }

  /**
   * Create SeRateInquiry
   *
   * @return SeRateInquiry
   */
  public function build()
  {
    return new SeRateInquiry($this->collectionAddress, $this->destinationAddress, $this->collectionUserAddressId,
                             $this->destinationUserAddressId, $this->date, $this->signatureConfirmation,
                             $this->today, $this->residential, $this->packageX, $this->flatRatePackageIdX,
                             $this->predefinedSizeIdX, $this->courierId, $this->serviceId, $this->currency,
                             $this->pickupOnly, $this->dropOffOnly, $this->cheapest, $this->shortest,
                             $this->longest, $this->ignoreWorkingHours, $this->auth
    );
  }
}
?>