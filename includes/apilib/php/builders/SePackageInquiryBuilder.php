<?php
SeServiceUtils::checkInclude(array(
  'SeBaseBuilder',
  'SePackageItem'
  )
);

class SePackageInquiryBuilder extends SeBaseBuilder
{
  /**
   * name
   *
   * @var string
   */
  protected $name;

  /**
   * description
   *
   * @var string
   */
  protected $description;

  /**
    *  weight
    *
    * @var numeric
    */
  protected $weight;

  /**
    * length
    *
    * @var numeric
    */
  protected $length;

  /**
    * width
    *
    * @var numeric
    */
  protected $width;

  /**
    * height
    *
    * @var numeric
    */
  protected $height;

  /**
    * isMetric
    *
    * @var boolean
    */
  protected $isMetric;

  /**
   * Create name
   *
   * @param $name
   * @return SeAddressBuilder
   */
  public function addName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Create description
   *
   * @param $description
   * @return SeAddressBuilder
   */
  public function addDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * Create weight
   *
   * @param $weight
   * @return SePackageInquiryBuilder
   */
  public function addWeight($weight)
  {
    $this->weight = $weight;

    return $this;
  }

  /**
   * Create length
   *
   * @param $length
   * @return SePackageItemBuilder
   */
  public function addLength($length)
  {
    $this->length = $length;

    return $this;
  }

  /**
   * Create width
   *
   * @param $width
   * @return SePackageInquiryBuilder
   */
  public function addWidth($width)
  {
    $this->width = $width;

    return $this;
  }

  /**
   * Create height
   *
   * @param $height
   * @return SePackageInquiryBuilder
   */
  public function addHeight($height)
  {
    $this->height = $height;

    return $this;
  }

  /**
   * Create isMetric
   *
   * @param $isMetric
   * @return SePackageInquiryBuilder
   */
  public function addIsMetric($isMetric)
  {
    $this->isMetric = $isMetric;

    return $this;
  }

  /**
   * Create SePackageItem
   *
   * @return SePackageItem
   */
  public function build()
  {
    return new SePackageItem(null, $this->name, $this->description, $this->weight,
                         $this->length, $this->width, $this->height,
                         $this->isMetric
    );
  }
}
?>