<?php
SeServiceUtils::checkInclude(array(
  'SeBaseBuilder',
  'SeCreateShipmentInquiry'
  )
);

class SeCreateShipmentBuilder extends SeBaseBuilder
{
  /**
   * $rate
   *
   * @var SeRate
   */
  private $rate;

  /**
   * $identificator
   *
   * @var string
   */
  protected $identificator;

  /**
   * $collectionAddress
   *
   * @var SeAddress
   */
  protected $collectionAddress;

  /**
   * $destinationAddress
   *
   * @var SeAddress
   */
  protected $destinationAddress;

  /**
    * $currency
    *
    * @var string
    */
  protected $currency;

  /**
    * MessageToBuyer
    *
    * @var string
    */
  protected $messageToBuyer;

  /**
    * PickupAt
    *
    * @var string
    */
  protected $pickupAt;

  /**
    * PickupCloseAt
    *
    * @var string
    */
  protected $pickupCloseAt;

  /**
    * RateId
    *
    * @var numeric
    */
  protected $rateId;

  /**
    * Courier
    *
    * @var string
    */
  protected $courier;

  /**
    * ServiceType
    *
    * @var string
    */
  protected $serviceType;

  /**
    * PickupDate
    *
    * @var string
    */
  protected $pickupDate;

  /**
    * DropOff
    *
    * @var boolean
    */
  protected $dropOff;

  /**
    * Est
    *
    * @var string
    */
  protected $est;

  /**
    * HumanEst
    *
    * @var string
    */
  protected $humanEst;

  /**
    * Packages
    *
    * @var array SeShipmentInquiryPackage
    */
  protected $packages = array();

  /**
    * Origin
    *
    * @var string
    */
  protected $origin = 'api';

  /**
    * InvoiceNumber
    *
    * @var string
    */
  protected $invoiceNumber;

  /**
    * InvoiceComment
    *
    * @var string
    */
  protected $invoiceComment;

  /**
   * Create details from SeRate object
   *
   * @param SeRate $seRate
   * @return SeCreateShipmentBuilder
   */
  public function fromRate(SeRate $seRate)
  {
    $this->rate = $seRate;

    $this->rateId = $seRate->getId();
    $this->courier = $seRate->getCourier();
    $this->currency = $seRate->getCurrencyCode();
    $this->serviceType = $seRate->getServiceName();
    $this->humanEst = $seRate->getHumanEta();
    $this->est = $seRate->getEta();

    return $this;
  }

  /**
   * Create $identificator
   *
   * @param string $identificator
   * @return SeCreateShipmentBuilder
   */
  public function addIdentificator($identificator)
  {
    $this->identificator = $identificator;

    return $this;
  }

  /**
   * Create $collectionAddress
   *
   * @param SeAddress $collectionAddress
   * @return SeCreateShipmentBuilder
   */
  public function addCollectionAddress(SeAddress $collectionAddress)
  {
    $this->collectionAddress = $collectionAddress;

    return $this;
  }

  /**
   * Create $destinationAddress
   *
   * @param SeAddress $destinationAddress
   * @return SeCreateShipmentBuilder
   */
  public function addDestinationAddress(SeAddress $destinationAddress)
  {
    $this->destinationAddress = $destinationAddress;

    return $this;
  }

  /**
   * Create Currency
   *
   * @param $currency
   * @return SeCreateShipmentBuilder
   */
  public function addCurrency($currency)
  {
    $this->currency = $currency;

    return $this;
  }

  /**
   * Create MessageToBuyer
   *
   * @param $messageToBuyer
   * @return SeCreateShipmentBuilder
   */
  public function addMessageToBuyer($messageToBuyer)
  {
    $this->messageToBuyer = $messageToBuyer;

    return $this;
  }

  /**
   * Create PickupAt
   *
   * @param $pickupAt
   * @return SeCreateShipmentBuilder
   */
  public function addPickupAt($pickupAt)
  {
    $this->pickupAt = $pickupAt;

    return $this;
  }

  /**
   * Create PickupCloseAt
   *
   * @param $pickupCloseAt
   * @return SeCreateShipmentBuilder
   */
  public function addPickupCloseAt($pickupCloseAt)
  {
    $this->pickupCloseAt = $pickupCloseAt;

    return $this;
  }

  /**
   * Create RateId
   *
   * @param $rateId
   * @return SeCreateShipmentBuilder
   */
  public function addRateId($rateId)
  {
    $this->rateId = $rateId;

    return $this;
  }

  /**
   * Create Courier
   *
   * @param $courier
   * @return SeCreateShipmentBuilder
   */
  public function addCourier($courier)
  {
    $this->courier = $courier;

    return $this;
  }

  /**
   * Create ServiceType
   *
   * @param $serviceType
   * @return SeCreateShipmentBuilder
   */
  public function addServiceType($serviceType)
  {
    $this->serviceType = $serviceType;

    return $this;
  }

  /**
   * Create PickupDate
   *
   * @param $pickupDate
   * @return SeCreateShipmentBuilder
   */
  public function addPickupDate($pickupDate)
  {
    $this->pickupDate = $pickupDate;

    return $this;
  }

  /**
   * Create Pickup
   *
   * @param $id
   * @return SeCreateShipmentBuilder
   */
  public function addPickup($id)
  {
    $pickupTimes = $this->rate->getPickupTimes();

    if (count($pickupTimes) > 0)
    {
      $this->pickupDate = $this->rate->getMetaDate();

      $times = explode('-', $pickupTimes[$id]);

      $this->pickupAt = $times[0];
      $this->pickupCloseAt = $times[1];
    }

    return $this;
  }

  /**
   * Create DropOff
   *
   * @param $dropOff
   * @return SeCreateShipmentBuilder
   */
  public function addDropOff($dropOff)
  {
    $this->dropOff = $dropOff;

    return $this;
  }

  /**
   * Create Est
   *
   * @param $est
   * @return SeCreateShipmentBuilder
   */
  public function addEst($est)
  {
    $this->est = $est;

    return $this;
  }

  /**
   * Create HumanEst
   *
   * @param $HumanEst
   * @return SeCreateShipmentBuilder
   */
  public function addHumanEst($humanEst)
  {
    $this->humanEst = $humanEst;

    return $this;
  }

  /**
   * Create Packages
   *
   * @param SeShipmentInquiryPackage $package
   * @return SeCreateShipmentBuilder
   */
  public function addPackage($package)
  {
    array_push($this->packages, $package);

    return $this;
  }

  /**
   * Create Origin
   *
   * @param $origin
   * @return SeCreateShipmentBuilder
   */
  public function addOrigin($origin)
  {
    $this->origin = $origin;

    return $this;
  }

  /**
   * Create invoiceNumber
   *
   * @param $invoiceNumber
   * @return SeCreateShipmentBuilder
   */
  public function addInvoiceNumber($invoiceNumber)
  {
    $this->invoiceNumber = $invoiceNumber;

    return $this;
  }

  /**
   * Create InvoiceComment
   *
   * @param $invoiceComment
   * @return SeCreateShipmentBuilder
   */
  public function addInvoiceComment($invoiceComment)
  {
    $this->invoiceComment = $invoiceComment;

    return $this;
  }

  /**
   * Create SeCreateShipmentInquiry
   *
   * @return SeCreateShipmentInquiry
   */
  public function build()
  {
    return new SeCreateShipmentInquiry($this->identificator, $this->collectionAddress, $this->destinationAddress,
                             $this->currency, $this->messageToBuyer, $this->pickupAt, $this->pickupCloseAt,
                             $this->rateId, $this->courier, $this->serviceType, $this->pickupDate,
                             $this->dropOff, $this->est, $this->humanEst, $this->packages,
                             $this->origin, $this->invoiceNumber, $this->invoiceComment
    );
  }
}
?>