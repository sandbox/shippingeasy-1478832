<?php
SeServiceUtils::checkInclude(array('SeServiceLoggerType', 'SeServerException'));
/**
 * SeLoggerFactory.class.php.
 *
 * PHP Version 5.3.1
 *
 * @category  Logger
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation to determine which logger should be instantiated and returned.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeLoggerFactory.v.0.1
 */
class SeLoggerFactory
{
  public static function create($logger)
  {
    // class name concatenation
    $className = 'Se'.$logger.'Logger';

    try
    {
      SeServiceUtils::checkInclude($className);

      return new $className();
    }
    catch (Exception $e)
    {
      throw new SeServerException($e->getMessage() . ' (' . $className . ').');
    }
  }
}
?>