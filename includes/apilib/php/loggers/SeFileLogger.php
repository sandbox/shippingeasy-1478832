<?php
SeServiceUtils::checkInclude('SeServiceLogger');
/**
 * SeFileLogger.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeLogger
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeFileLogger implements logging to file.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeFileLogger.v.0.1
 */
class SeFileLogger extends SeServiceLogger
{
  /**
   * File logger implementation
   *
   * @param $request, $response and optional exception
   *
   * @return
   */
  public function log($request, $response, $exception=null)
  {
    if (!LOGGER_ENABLED)
    {
      return;
    }

    $fp = fopen(LOGGER_ABSOLUTE_FILEPATH . '/log_' . date('Y-m-d') . '.txt', 'a');
    fwrite($fp, '========================================START' . "\n");
    fwrite($fp, 'REQUEST OBJECT - ' . date('Y-m-d H:i:s') . "\n");

    ob_start();
    var_dump($request);
    $data = ob_get_clean();
    fwrite($fp, $data);
    fwrite($fp, "\n");

    fwrite($fp, '---------------------------------------------' . "\n");
    fwrite($fp, 'RESPONSE OBJECT - ' . date('Y-m-d H:i:s') . "\n");

    ob_start();
    var_dump($response);
    $data = ob_get_clean();
    fwrite($fp, $data);
    fwrite($fp, "\n");

    fwrite($fp, '---------------------------------------------' . "\n");
    fwrite($fp, 'EXCEPTION OBJECT - ' . date('Y-m-d H:i:s') . "\n");

    ob_start();
    var_dump($exception);
    $data = ob_get_clean();
    fwrite($fp, $data);
    fwrite($fp, "\n");
    fwrite($fp, '==========================================END' . "\n");
    fclose($fp);
  }
}
?>