<?php
/**
 * SeResourceName.php.
 *
 * PHP Version 5.3.1
 *
 * @category  MethodType
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeResourceName is used to identify resource name used in communication. It only contains constants.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeResourceName.v.0.1
 */
class SeResourceName
{
  const MatchHsCode = 'MatchHsCode';
  const GetRate = 'GetRate';
  const GetCourier = 'GetCourier';
  const ListAddresses = 'ListAddresses';
  const AddAddress = 'AddAddress';
  const UpdateAddress = 'UpdateAddress';
  const DeleteAddress = 'DeleteAddress';
  const GetAddressDetails = 'GetAddressDetails';
  const GetTrackingInfo = 'GetTrackingInfo';
  const CreateShipment = 'CreateShipment';
  const GetDocuments = 'GetDocuments';
  const GetPackageDetails = 'GetPackageDetails';
  const AddPackage = 'AddPackage';
  const DeletePackage = 'DeletePackage';
  const UpdatePackage = 'UpdatePackage';
  const ListPackages = 'ListPackages';
  const ListCouriers = 'ListCouriers';
  const GetShipmentDetails = 'GetShipmentDetails';
  const CreatePickup = 'CreatePickup';
  const ListServiceTypes = 'ListServiceTypes';
  const ListExchangeRates = 'ListExchangeRates';
}
?>