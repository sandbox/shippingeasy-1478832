<?php
abstract class SeBaseBuilder
{
  /**
   * Create instance of current builder
   *
   * @return BaseBuilder
   */
  public static function getBuilder()
  {
    $self = get_called_class();
    return new $self();
  }

  abstract public function build();
}