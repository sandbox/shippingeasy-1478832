<?php
/**
 * SeEnvironment.php.
 *
 * PHP Version 5.3.1
 *
 * @category  MethodType
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeEnvironment is used to identify which environment we work with. It only contains constants.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeEnvironment.v.0.1
 */
class SeEnvironment
{
  const Prod = 'Prod';
  const Test = 'Test';
}
?>