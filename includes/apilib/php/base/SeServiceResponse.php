<?php
/**
 * SeServiceResponse.php.
 *
 * @category  SeService
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 */

/**
 * SeServiceResponse
 *
 * Abstract web service response class.
 *
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 */
abstract class SeServiceResponse
{
  /**
   * Headers of the response.
   *
   * @var array
   */
  private $headers;

  /**
   * Body of the response.
   *
   * @var mixed
   */
  private $body;

  /**
   * Time needed to execute request in seconds.
   *
   * @var float
   */
  private $executionTime;

  /**
   * Indicate whether class was initialized.
   *
   * @var boolean
   */
  private $initialized;

  /**
   * Show if call was successful.
   *
   * @var boolean
   */
  private $successful = false;

  /**
   * Constructor method.
   *
   * Creates empty response and set initialized to false.
   *
   * @return void
   */
  public function __construct()
  {
    $this->initialized = false;
  }

  /**
   * Checks if call was sucessful and parses response.
   *
   * @param array $headers Headers of Response of web service request.
   * @param mixed $body    Body Response of web service request.
   *
   * @return void
   */
  public function init($headers, $body)
  {
    $this->setHeaders($headers);
    $this->setBody($body);
    $this->initialized = true;
    $this->determineSuccess();
  }

  /**
   * Initializes response headers.
   *
   * @param array $headers Response header of web service request.
   *
   * @return void.
   */
  protected function setHeaders($headers)
  {
    $this->headers = $headers;
  }

  /**
   * Returns response headers.
   *
   * @return array
   */
  public function getHeaders()
  {
    return $this->headers;
  }

  /**
   * Construct the body of the response.
   *
   * @param mixed $body Response body of web service call.
   *
   * @return mixed
   */
  protected function setBody($body)
  {
    $this->body = $body;
  }

  /**
   * Returns body of the response.
   *
   * @return mixed
   */
  public function getBody()
  {
    return $this->body;
  }

  /**
   * Set web service execution time.
   *
   * Show response time in seconds.
   *
   * @param float $time Time passed since sending request in seconds.
   *
   * @return void
   */
  public function setExecutionTime($time)
  {
    $this->executionTime = $time;
  }

  /**
   * Returns time taken to execute request in seconds
   *
   * @return float
   */
  public function getExecutionTime()
  {
    return $this->executionTime;
  }

  /**
   * Set status of response based on response body and headers.
   *
   * @param boolean $success True if request was successful and false it it was not.
   *
   * @return void.
   */
  protected function setSuccessful($success)
  {
    $this->successful = $success;
  }

  /**
   * Show if request was successful.
   *
   * @return boolean
   */
  public function getSuccessful()
  {
    return $this->successful;
  }

  /**
   * Determine if request was successful based on response body and headers.
   *
   * @throws Exception if responce class is not implemented.
   *
   * @return boolean
   */
  abstract protected function determineSuccess();

  /**
   * Shows if response was initialized.
   *
   * @return boolean
   */
  public function isInitialized()
  {
    return $this->initialized;
  }

  /**
   * Magic method that returns body of request of request is passed as string.
   *
   * @return mixed
   */
  public function __toString()
  {
    return $this->body;
  }
}
?>