<?php
/**
 * SeException.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeException
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeException is used to report exceptions via HTTP error codes.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeException.v.0.1
 */
class SeException extends Exception
{
  /**
   * HTTP error code to display to client. Defaults to 500 Internal Server Error.
   *
   * @var integer
   */
  protected $httpCode = 500;

  /**
   * Array containing list of specific errors that happened.
   *
   * @var array
   */
  protected $messages;

  /**
   * Text of specific error that happened.
   *
   * @var array
   */
  protected $text;

  /**
   * Extended Exception class override function
   *
   * @param text $message message to be passed to Exception
   *
   * @return object $exception parent constructor
   */
  function __construct($message, $messages=null, Exception $previous=null, $text=null)
  {
    $this->messages = array();
    if (is_array($messages) === true)
    {
      $this->messages = $messages;
    }

    if (!is_null($text))
    {
      $this->text = $text;
    }

    return parent::__construct($message, 0, $previous);

  }//end __construct()

  /**
   * Returns object's HttpCode. For Server exceptions it's always 500. Request exceptions could change.
   *
   * @param
   *
   * @return object's HttpCode
   */
  public function getHttpCode()
  {
    return $this->httpCode;
  }

  /**
   * Returns object's Text if it exist.
   *
   * @param
   *
   * @return object's Text
   */
  public function getText()
  {
    return $this->text;
  }

  /**
   * Returns object's Messages.
   *
   * @param
   *
   * @return object's Messages
   */
  public function getMessages()
  {
    return $this->messages;
  }
}//end class
?>