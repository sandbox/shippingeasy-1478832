<?php
/**
 * SeServiceType.php.
 *
 * PHP Version 5.3.1
 *
 * @category  ServiceType
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeServiceType is used to identify service type used in communication. It only contains constants.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeServiceType.v.0.1
 */
class SeServiceType
{
  const Curl = 'Curl';
}
?>