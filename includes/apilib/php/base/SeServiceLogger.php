<?php
/**
 * SeServiceLogger.php.
 *
 * @category  WebService
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 */

/**
 * SeServiceLogger
 *
 * Logger class used for logging of web service calls.
 *
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 */
abstract class SeServiceLogger
{
  /**
   * Method for loging web service calls.
   *
   * @param WebServiceRequest  $request   Request to log.
   * @param WebServiceResponse $response  Response to log
   * @param Exception          $exception Optional exception to add to log for this request/response pair.
   *
   * @return void
   */
  abstract public function log($request, $response, $exception=null);
}
