<?php
/**
 * SeWebService.php.
 *
 * @category  SeWebService
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 */

/**
 * SeWebService
 *
 * Main web service class.
 *
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 */
abstract class SeWebService
{
  /**
   * Time it took to get a response from the service the last time it was called.
   *
   * @var float
   */
  protected $lastResponseTime;

  /**
   * Start time of request
   *
   * @var float
   */
  protected $startTime;

  /**
   * Time we got a response
   *
   * @var float
   */
  protected $endTime;

  /**
   * Starts the timer and saves starting time.
   *
   * @return void
   */
  protected function startTimer()
  {
    list($usec, $sec) = explode(' ', microtime());
    $this->startTime = (float) $sec + (float) $usec;
  }

  /**
   * Stop timer and saves measured time.
   *
   * @return void
   */
  protected function stopTimer()
  {
    list($usec, $sec) = explode(' ', microtime());
    $this->endTime = (float) $sec + (float) $usec;

    $this->lastResponseTime = round($this->endTime - $this->startTime, 5);
  }

  /**
   * Returns last execution time measured by the timer.
   *
   * @return mixed
   */
  protected function getLastResponseTime()
  {
    return $this->lastResponseTime;
  }

  /**
   * Method executes request, measures time taken for execution and saves result in response.
   *
   * @param $request   Web service request to execute.
   * @param &$response Web service response object to record results to.
   *
   * @throws SeServerException if response class is not initialized with call result.
   *
   * @return void
   */
  public function execute($request, &$response)
  {
    if ($this->preExecute($request, $response) === false)
    {
      return false;
    }

    $this->startTimer();
    $call = $this->doExecute($request, $response);
    $this->stopTimer();
    $this->postExecute($request, $response);

    if ($response->isInitialized() === false)
    {
      throw new SeServerException('Response class was not initialized');
    }

    $response->setExecutionTime($this->getLastResponseTime());
  }

  /**
   * Executes request and saves result in response.
   *
   * @param &$request   Web service request to execute.
   * @param &$response Web service response object to record results to.
   *
   * @throws Exception If method is not implented in web service class.
   *
   * @return void
   */
  abstract protected function doExecute(&$request, &$response);

  /**
   * Pre execute hook.
   *
   * @param &$request  Web service request to execute.
   * @param &$response Web service response object to record results to.
   *
   * @throws Exception If error accured in preExecute implementation.
   *
   * @return boolean Returns true if method was successfuly executed and false if it was not
   */
  abstract protected function preExecute(&$request, &$response);

  /**
   * Post execute hook.
   *
   * @param &$request  Web service request to execute.
   * @param &$response Web service response object to record results to.
   *
   * @throws Exception If error occured in postExecute implementation.
   *
   * @return void
   */
  abstract protected function postExecute(&$request, &$response);
}
?>