<?php
/**
 * SeServiceRequest.php.
 *
 * @category  SeService
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 */

/**
 * SeServiceRequest
 *
 * Abstract web service request class.
 *
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 */
abstract class SeServiceRequest
{
  /**
   * Headers of the request.
   *
   * @var array
   */
  private $headers = array();

  /**
   * Body of the request.
   *
   * @var mixed
   */
  private $body;

  /**
   * Construct headers to use in request.
   *
   * @param array $headers Headers of request.
   *
   * @return void
   */
  public function setHeaders($headers)
  {
    $this->headers = $headers;
  }

  /**
   * Adds single header item to use in request.
   *
   * @param $key, $value
   *
   * @return void
   */
  public function addHeaderItem($key, $value)
  {
    $this->headers[$key] = $value;
  }

  /**
   * Returns headers to use in request.
   *
   * @return array
   */
  public function getHeaders()
  {
    return $this->headers;
  }

  /**
   * Construct body of the request.
   *
   * @param mixed $body Body of request.
   *
   * @return void
   */
  public function setBody($body)
  {
    $this->body = $body;
  }

  /**
   * Returns body of the request.
   *
   * @return mixed
   */
  public function getBody()
  {
    return $this->body;
  }

  /**
   * Magic method that returns body of request is passed as string.
   *
   * @return mixed
   */
  public function __toString()
  {
    return $this->body;
  }
}
?>