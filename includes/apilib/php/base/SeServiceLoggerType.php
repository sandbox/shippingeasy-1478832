<?php
/**
 * SeServiceLoggerType.php.
 *
 * PHP Version 5.3.1
 *
 * @category  LoggerType
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeServiceLoggerType is used to identify logger used for logging. It only contains constants.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeServiceLoggerType.v.0.1
 */
class SeServiceLoggerType
{
  const File = 'File';
}
?>