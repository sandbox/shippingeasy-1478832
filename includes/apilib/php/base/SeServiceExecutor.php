<?php
/**
 * SeServiceExecutor.php.
 *
 * @category  SeService
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPL
 */

/**
 * SeServiceExecutor
 *
 * Used for execution and logging of web service requests.
 *
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 - 2012 ShippingEasy Pty Ltd
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPL
 */
class SeServiceExecutor
{
  /**
   * Web Service used to execute call.
   *
   * @var WebService
   */
  protected $service;

  /**
   * Logger Service used to log call.
   *
   * @var SeServiceLogger
   */
  protected $logger;

  /**
   * Initialize services.
   *
   * @param WebService       $web_service Web service to execute request with.
   * @param WebServiceLogger $logger      Logging class to log service requests and responses with.
   *
   * @return void
   */
  public function __construct($web_service, $logger)
  {
    $this->service = $web_service;
    $this->setLogger($logger);
  }

  /**
   * Returns object's Logger
   *
   * @param
   *
   * @return object's Logger
   */
  public function getLogger()
  {
    return $this->logger;
  }

  /**
   * Sets object's Logger
   *
   * @param $logger
   *
   * @return
   */
  protected function setLogger($logger)
  {
    $this->logger = $logger;
  }

  /**
   * Method for executing web service calls.
   *
   * @param WebServiceRequest  $request   Web service request passed to execution.
   * @param WebServiceResponse &$response Web service response to store result.
   *
   * @return void
   */
  public function execute(&$request, &$response)
  {
    try
    {
      $this->service->execute($request, $response);
      $this->getLogger()->log($request, $response);
    }
    catch (Exception $e)
    {
      $this->getLogger()->log($request, $response, $e);

      try
      {
        $json = json_decode($response->getBody());

        $text = $json->text;

        $messages = null;

        if (isset($json->messages))
        {
          $messages = (array)$json->messages;
        }
      }
      catch (Exception $e1)
      {
        throw new SeRequestException($e->getHttpCode(), $e->getMessage(), null, $e);
      }

      // this line of code will execute if previous catch is not executed
      throw new SeRequestException($e->getHttpCode(), $e->getMessage(), (array)$messages, $e, $text);
    }
  }
}
?>