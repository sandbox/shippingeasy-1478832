<?php
/**
 * SeServiceUtils.php.
 *
 * PHP Version 5.3.1
 *
 * @category  Utilities
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeServiceUtils is used to support different methods.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeServiceUtils.v.0.1
 */
class SeServiceUtils
{
  /**
   * A list of API folders where included files should be searched for.
   *
   * @var array
   */
  private static $folders = array(
    'base',
    'builders',
    'dto',
    'exceptions',
    'loggers',
    'requests',
    'responses',
    'services'
  );

  /**
   * A list of response codes.
   *
   * @var array
   */
  public $responseCodes = array(
    '200' => 'OK - Success!',
    '201' => 'CREATED - A new resource was successfully created.',
    '400' => 'BAD_REQUEST - There was an error in your request.',
    '401' => 'UNAUTHORIZED - You are not allowed to access the resource.',
    '403' => 'FORBIDDEN - Data you are trying to access is private.',
    '404' => 'NOT_FOUND - The requested resource could not be found.',
    '405' => 'METHOD_NOT_ALLOWED - A request was made using a request method not supported by the resource.',
    '406' => 'NOT_ACCEPTABLE - The requested resource is only capable of generating content not acceptable according to the Accept Headers sent in the request.',
    '409' => 'CONFLICT - The request could not be processed because of conflict in the request.',
    '410' => 'GONE - The requested resource is no longer available and will not be available again.',
    '415' => 'UNSUPPORTED_MEDIA_TYPE - The request entity has a media type which the server or resource does not support.',
    '500' => 'INTERNAL_SERVER_ERROR - An internal error on server side, given when no more specific error message is available.',
    '501' => 'NOT_IMPLEMENTED - The server either does not recognize the request method, or it lacks the ability to fulfill the request.',
    '503' => 'SERVICE_UNAVAILABLE - The ShippingEasy API is down for scheduled maintenance; please try again later.'
  );

  /**
   * Method to parse request parameters and to make an array.
   *
   */
  public static function getMethodParameters($args, $argsNames)
  {
    $argNamesArray = explode(',', $argsNames);

    $params = array();

    foreach($argNamesArray as $key => $value)
    {
      $value = trim($value);

      if (isset($args[$key]))
      {
        $params[$value] = $args[$key];
      }
    }

    return $params;
  }

  /**
   * Method to fetch possible response codes.
   *
   * @return array
   */
  public function getResponseCodes()
  {
    return $this->responseCodes;
  }

  /**
   * method to check if file/s is/are included and (if it's/they not) -> include it/them.
   * if className parameter is array, then all files in array are included
   * if className parameter is not array, only than className is included
   *
   * @return
   */
  public static function checkInclude($classNames)
  {
    $classNames = is_array($classNames) ? $classNames : array($classNames);

    // parse array of classes
    foreach($classNames as $class)
    {
      if (!class_exists($class))
      {
        foreach(self::$folders as $folder)
        {
          if (file_exists('../' . $folder . '/' . $class . '.php'))
          {
            require '../' . $folder . '/' . $class . '.php';
            break;
          }
        }
      }
    }
  }

  /**
   * Method which determines entry point for web service calls.
   *
   * @return
   */
  public static function determineEntryPoint($environment)
  {
    if ($environment == SeEnvironment::Test)
    {
      define('ENTRYPOINT_BASEPATH', 'https://staging.shippingeasy.com/api/v1/');
    }
    elseif ($environment == SeEnvironment::Prod)
    {
      define('ENTRYPOINT_BASEPATH', 'https://www.shippingeasy.com/api/v1/');
    }
    else
    {
      throw new SeServerException('Unknown environment!');
    }
  }

  /**
   * Method to check if array of params contains mandatory items.
   *
   * if mandatory item is missing, server exception is thrown
   *
   * @param array $mandatoryParams, array $params
   *
   * @return
   */
  public static function checkMandatoryParams($mandatoryParams, $params)
  {
    foreach ($mandatoryParams as $param)
    {
      if (!isset($params[$param]))
      {
        throw new SeServerException($param . ' parameter is mandatory.');
      }
    }
  }

  /**
   * Method to parse SeRateInquiry object.
   *
   * @param SeRateInquiry $seRateInquiry
   *
   * @return array $params
   */
  public static function parseSeRateInquiry(SeRateInquiry $seRateInquiry)
  {
    $params = array();

    $params['CollectionCountry'] = $seRateInquiry->getCollectionAddress()->getCountry();
    $params['CollectionZip'] = $seRateInquiry->getCollectionAddress()->getZip();
    $params['CollectionCity'] = $seRateInquiry->getCollectionAddress()->getCity();
    $params['DestinationCountry'] = $seRateInquiry->getDestinationAddress()->getCountry();
    $params['DestinationZip'] = $seRateInquiry->getDestinationAddress()->getZip();
    $params['DestinationCity'] = $seRateInquiry->getDestinationAddress()->getCity();

    if (!is_null($seRateInquiry->getCollectionUserAddressId()))
    {
      $params['CollectionUserAddressId'] = $seRateInquiry->getCollectionUserAddressId();
    }

    if (!is_null($seRateInquiry->getDestinationUserAddressId()))
    {
      $params['DestinationUserAddressId'] = $seRateInquiry->getDestinationUserAddressId();
    }

    $params['Date'] = $seRateInquiry->getDate();

    if (!is_null($seRateInquiry->getSignatureConfirmation()))
    {
      $params['SignatureConfirmation'] = $seRateInquiry->getSignatureConfirmation();
    }

    $params['Today'] = $seRateInquiry->getToday();

    if (!is_null($seRateInquiry->getResidential()))
    {
      $params['Residential'] = $seRateInquiry->getResidential();
    }

    if (count($seRateInquiry->getPackageX() > 0))
    {
      foreach ($seRateInquiry->getPackageX() as $package)
      {
        $params[$package->getName()] = $package->getSize();
      }
    }

    if (count($seRateInquiry->getFlatRatePackageIdX() > 0))
    {
      foreach ($seRateInquiry->getFlatRatePackageIdX() as $package)
      {
        $params[$package->getName()] = $package->getValue();
      }
    }

    if (count($seRateInquiry->getPredefinedSizeIdX() > 0))
    {
      foreach ($seRateInquiry->getPredefinedSizeIdX() as $package)
      {
        $params[$package->getName()] = $package->getValue();
      }
    }

    if (!is_null($seRateInquiry->getCourierId()))
    {
      $params['courierId'] = $seRateInquiry->getCourierId();
    }

    if (!is_null($seRateInquiry->getServiceId()))
    {
      $params['serviceId'] = $seRateInquiry->getServiceId();
    }

    if (!is_null($seRateInquiry->getCurrency()))
    {
      $params['Currency'] = $seRateInquiry->getCurrency();
    }

    if (!is_null($seRateInquiry->getPickupOnly()))
    {
      if ($seRateInquiry->getPickupOnly() == true)
      {
        $params['pickupOnly'] = $seRateInquiry->getPickupOnly();
      }
    }

    if (!is_null($seRateInquiry->getDropOffOnly()))
    {
      if ($seRateInquiry->getDropOffOnly() == true)
      {
        $params['dropOffOnly'] = $seRateInquiry->getDropOffOnly();
      }
    }

    if (!is_null($seRateInquiry->getCheapest()))
    {
      if ($seRateInquiry->getCheapest() == true)
      {
        $params['cheapest'] = $seRateInquiry->getCheapest();
      }
    }

    if (!is_null($seRateInquiry->getShortest()))
    {
      if ($seRateInquiry->getShortest() == true)
      {
        $params['shortest'] = $seRateInquiry->getShortest();
      }
    }

    if (!is_null($seRateInquiry->getLongest()))
    {
      if ($seRateInquiry->getLongest() == true)
      {
        $params['longest'] = $seRateInquiry->getLongest();
      }
    }

    if (!is_null($seRateInquiry->getIgnoreWorkingHours()))
    {
      if ($seRateInquiry->getIgnoreWorkingHours() == true)
      {
        $params['IgnoreWorkingHours'] = $seRateInquiry->getIgnoreWorkingHours();
      }
    }

    if (!is_null($seRateInquiry->getAuth()))
    {
      if ($seRateInquiry->getAuth() == true)
      {
        $params['Auth'] = $seRateInquiry->getAuth();
      }
    }

    return $params;
  }

  /**
   * Method to parse SeAddress object.
   *
   * @param SeAddress $seAddress
   *
   * @return array $params
   */
  public static function parseSeAddress(SeAddress $seAddress)
  {
    $params = array();

    if (!is_null($seAddress->getName()))
    {
      $params['Name'] = $seAddress->getName();
    }

    if (!is_null($seAddress->getDescription()))
    {
      $params['Description'] = $seAddress->getDescription();
    }

    if (!is_null($seAddress->getIsDefault()))
    {
      $params['IsDefault'] = $seAddress->getIsDefault();
    }

    if (!is_null($seAddress->getAddressType()))
    {
      $params['AddressType'] = $seAddress->getAddressType();
    }

    if (!is_null($seAddress->getType()))
    {
      $params['Type'] = $seAddress->getType();
    }

    if (!is_null($seAddress->getFirstName()))
    {
      $params['FirstName'] = $seAddress->getFirstName();
    }

    if (!is_null($seAddress->getLastName()))
    {
      $params['LastName'] = $seAddress->getLastName();
    }

    if (!is_null($seAddress->getCompanyName()))
    {
      $params['CompanyName'] = $seAddress->getCompanyName();
    }

    if (!is_null($seAddress->getLine1()))
    {
      $params['Line1'] = $seAddress->getLine1();
    }

    if (!is_null($seAddress->getLine2()))
    {
      $params['Line2'] = $seAddress->getLine2();
    }

    if (!is_null($seAddress->getPostalCode()))
    {
      $params['PostalCode'] = $seAddress->getPostalCode();
    }

    if (!is_null($seAddress->getCity()))
    {
      $params['City'] = $seAddress->getCity();
    }

    if (!is_null($seAddress->getCountry()))
    {
      $params['Country'] = $seAddress->getCountry();
    }

    if (!is_null($seAddress->getTelephoneAreaCode()))
    {
      $params['TelephoneAreaCode'] = $seAddress->getTelephoneAreaCode();
    }

    if (!is_null($seAddress->getTelephoneNumber()))
    {
      $params['TelephoneNumber'] = $seAddress->getTelephoneNumber();
    }

    if (!is_null($seAddress->getEmailAddress()))
    {
      $params['EmailAddress'] = $seAddress->getEmailAddress();
    }

    if (!is_null($seAddress->getCountryDivision()))
    {
      $params['CountryDivision'] = $seAddress->getCountryDivision();
    }

    return $params;
  }

  /**
   * Method to parse array of SeCreateShipmentInquiry objects.
   *
   * @param array SeCreateShipmentInquiry $seCreateShipmentInquiry
   *
   * @return array $params
   */
  public static function parseSeCreateShipmentInquiry(array $seCreateShipmentInquiry)
  {
    $seCreateShipmentInquiry =
      is_array($seCreateShipmentInquiry) ? $seCreateShipmentInquiry : array($seCreateShipmentInquiry);

    $params = array();

    foreach($seCreateShipmentInquiry as $inquiry)
    {
      $paramItem = array();

      // identificator
      $identificator = $inquiry->getIdentificator();

      // collection address
      $paramItem['CollectionAddress']['CountryCode'] = $inquiry->getCollectionAddress()->getCountry();
      $paramItem['CollectionAddress']['City'] = $inquiry->getCollectionAddress()->getCity();
      $paramItem['CollectionAddress']['Division'] = $inquiry->getCollectionAddress()->getCountryDivision();
      $paramItem['CollectionAddress']['PostalCode'] = $inquiry->getCollectionAddress()->getPostalCode();
      $paramItem['CollectionAddress']['Line1'] = $inquiry->getCollectionAddress()->getLine1();
      $paramItem['CollectionAddress']['Line2'] = $inquiry->getCollectionAddress()->getLine2();
      $paramItem['CollectionAddress']['FirstName'] = $inquiry->getCollectionAddress()->getFirstName();
      $paramItem['CollectionAddress']['LastName'] = $inquiry->getCollectionAddress()->getLastName();
      $paramItem['CollectionAddress']['TelephoneNumber'] = $inquiry->getCollectionAddress()->getTelephoneNumber();
      $paramItem['CollectionAddress']['TelephoneAreaCode'] = $inquiry->getCollectionAddress()->getTelephoneAreaCode();
      $paramItem['CollectionAddress']['CompanyName'] = $inquiry->getCollectionAddress()->getCompanyName();
      $paramItem['CollectionAddress']['EmailAddress'] = $inquiry->getCollectionAddress()->getEmailAddress();
      $paramItem['CollectionAddress']['IsResidential'] = $inquiry->getCollectionAddress()->getIsResidential();

      // destination address
      $paramItem['DestinationAddress']['CountryCode'] = $inquiry->getDestinationAddress()->getCountry();
      $paramItem['DestinationAddress']['City'] = $inquiry->getDestinationAddress()->getCity();
      $paramItem['DestinationAddress']['Division'] = $inquiry->getDestinationAddress()->getCountryDivision();
      $paramItem['DestinationAddress']['PostalCode'] = $inquiry->getDestinationAddress()->getPostalCode();
      $paramItem['DestinationAddress']['Line1'] = $inquiry->getDestinationAddress()->getLine1();
      $paramItem['DestinationAddress']['Line2'] = $inquiry->getDestinationAddress()->getLine2();
      $paramItem['DestinationAddress']['FirstName'] = $inquiry->getDestinationAddress()->getFirstName();
      $paramItem['DestinationAddress']['LastName'] = $inquiry->getDestinationAddress()->getLastName();
      $paramItem['DestinationAddress']['TelephoneNumber'] = $inquiry->getDestinationAddress()->getTelephoneNumber();
      $paramItem['DestinationAddress']['TelephoneAreaCode'] = $inquiry->getDestinationAddress()->getTelephoneAreaCode();
      $paramItem['DestinationAddress']['CompanyName'] = $inquiry->getDestinationAddress()->getCompanyName();
      $paramItem['DestinationAddress']['EmailAddress'] = $inquiry->getDestinationAddress()->getEmailAddress();
      $paramItem['DestinationAddress']['IsResidential'] = $inquiry->getDestinationAddress()->getIsResidential();

      // main
      $paramItem['Currency'] = $inquiry->getCurrency();
      $paramItem['MessageToBuyer'] = $inquiry->getMessageToBuyer();
      $paramItem['PickupAt'] = $inquiry->getPickupAt();
      $paramItem['PickupCloseAt'] = $inquiry->getPickupCloseAt();
      $paramItem['RateId'] = $inquiry->getRateId();
      $paramItem['Courier'] = $inquiry->getCourier();
      $paramItem['ServiceType'] = $inquiry->getServiceType();
      $paramItem['PickupDate'] = $inquiry->getPickupDate();
      $paramItem['DropOff'] = $inquiry->getDropOff();
      $paramItem['Est'] = $inquiry->getEst();
      $paramItem['HumanEst'] = $inquiry->getHumanEst();
      $paramItem['Origin'] = $inquiry->getOrigin();
      $paramItem['InvoiceNumber'] = $inquiry->getInvoiceNumber();
      $paramItem['InvoiceComment'] = $inquiry->getInvoiceComment();

      // packages
      $paramItem['Packages'] = array();

      foreach ($inquiry->getPackages() as $package)
      {
        $item = array();

        $item['ValueAmount'] = $package->getValueAmount();
        $item['ValueCurrency'] = $package->getValueCurrency();
        $item['Weight'] = $package->getWeight();
        $item['Length'] = $package->getLength();
        $item['Width'] = $package->getWidth();
        $item['Height'] = $package->getHeight();
        $item['PredefinedSizeId'] = $package->getPredefinedSizeId();
        $item['FlatRatePackageId'] = $package->getFlatRatePackageIdX();
        $item['ContainsRestrictedItems'] = $package->getContainsRestrictedItems();
        $item['IsMetric'] = $package->getIsMetric();
        $item['PackageType'] = $package->getPackageType();
        $item['AdditionalInsuranceAmount'] = $package->getAdditionalInsuranceAmount();
        $item['AdditionalInsuranceCurrency'] = $package->getAdditionalInsuranceCurrency();

        // items
        $item['Items'] = array();

        foreach ($package->getItems() as $packageItem)
        {
          $pacItem = array();

          $pacItem['Description'] = $packageItem->getDescription();
          $pacItem['SerialNumber'] = $packageItem->getSerialNumber();
          $pacItem['HsCode'] = $packageItem->getHsCode();
          $pacItem['ValueAmount'] = $packageItem->getValueAmount();
          $pacItem['ValueCurrency'] = $packageItem->getValueCurrency();
          $pacItem['Comments'] = $packageItem->getComments();
          $pacItem['Quantity'] = $packageItem->getQuantity();
          $pacItem['UnitType'] = $packageItem->getUnitType();
          $pacItem['ManufactureCountry'] = $packageItem->getManufactureCountry();
          $pacItem['OriginalId'] = $packageItem->getOriginalId();
          $pacItem['OriginalTransactionId'] = $packageItem->getOriginalTransactionId();

          array_push($item['Items'], $pacItem);
        }

        array_push($paramItem['Packages'], $item);
      }

      $params[$identificator] = $paramItem;
    }

    return $params;
  }

  /**
   * Method to parse SePackageItem object.
   *
   * @param SePackageItem $sePackageItem
   *
   * @return array $params
   */
  public static function parseSePackageItem(SePackageItem $sePackageItem)
  {
    $params = array();

    if (!is_null($sePackageItem->getName()))
    {
      $params['Name'] = $sePackageItem->getName();
    }

    if (!is_null($sePackageItem->getDescription()))
    {
      $params['Description'] = $sePackageItem->getDescription();
    }

    if (!is_null($sePackageItem->getWeight()))
    {
      $params['Weight'] = $sePackageItem->getWeight();
    }

    if (!is_null($sePackageItem->getLength()))
    {
      $params['Length'] = $sePackageItem->getLength();
    }

    if (!is_null($sePackageItem->getWidth()))
    {
      $params['Width'] = $sePackageItem->getWidth();
    }

    if (!is_null($sePackageItem->getHeight()))
    {
      $params['Height'] = $sePackageItem->getHeight();
    }

    if (!is_null($sePackageItem->getIsMetric()))
    {
      $params['IsMetric'] = $sePackageItem->getIsMetric();
    }

    return $params;
  }
}
?>