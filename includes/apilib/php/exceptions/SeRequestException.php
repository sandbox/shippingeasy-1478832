<?php
/**
 * SeRequestException.class.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeException
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeRequestException is used to report exceptions which happen during curl request. Error codes could varry.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeRequestException.v.0.1
 */

SeServiceUtils::checkInclude('SeException');

class SeRequestException extends SeException
{
  /**
   * Extended SeException class override function
   *
   * @param text $message message to be passed to Exception
   *
   * @return object $exception parent constructor
   */
  function __construct($httpCode, $message, $messages=null, Exception $previous=null, $text=null)
  {
    $this->httpCode = $httpCode;

    return parent::__construct($message, $messages, $previous, $text);
  }//end __construct()
}//end class
?>