<?php
/**
 * SeShippingItem.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one item in request result array.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeShippingItem.v.0.1
 */
class SeShippingItem
{
  protected $orderId;

  protected $created;

  protected $trackingNumber;

  protected $shippingMethod;

  protected $serviceId;

  protected $courierName;

  protected $shipDate;

  protected $expectedDelivery;

  protected $cost;

  protected $currency;

  /**
   * length of the package.
   *
   * @var double
   */
  protected $length;

  /**
   * width of the package.
   *
   * @var double
   */
  protected $width;

  /**
   * height of the package.
   *
   * @var double
   */
  protected $height;

  /**
   * package dimensions length unit.
   *
   * @var string
   */
  protected $lengthUnit;

  /**
   * weigth of the package.
   *
   * @var double
   */
  protected $weight;

  /**
   * package dimensions weight unit.
   *
   * @var string
   */
  protected $weightUnit;

  protected $products=array();

  public function getOrderId()
  {
    return $this->orderId;
  }

  public function setOrderId($orderId)
  {
    $this->orderId = $orderId;
  }

  public function getCreated()
  {
    return $this->created;
  }

  public function setCreated($created)
  {
    $this->created = $created;
  }

  public function getTrackingNumber()
  {
    return $this->trackingNumber;
  }

  public function setTrackingNumber($trackingNumber)
  {
    $this->trackingNumber = $trackingNumber;
  }

  public function getShippingMethod()
  {
    return $this->shippingMethod;
  }

  public function setShippingMethod($shippingMethod)
  {
    $this->shippingMethod = $shippingMethod;
  }

  public function getServiceId()
  {
    return $this->serviceId;
  }

  public function setServiceId($serviceId)
  {
    $this->serviceId = $serviceId;
  }

  public function getCourierName()
  {
    return $this->courierName;
  }

  public function setCourierName($courierName)
  {
    $this->courierName = $courierName;
  }

  public function getShipDate()
  {
    return $this->shipDate;
  }

  public function setShipDate($shipDate)
  {
    $this->shipDate = $shipDate;
  }

  public function getExpectedDelivery()
  {
    return $this->expectedDelivery;
  }

  public function setExpectedDelivery($expectedDelivery)
  {
    $this->expectedDelivery = $expectedDelivery;
  }

  public function getCost()
  {
    return $this->cost;
  }

  public function setCost($cost)
  {
    $this->cost = $cost;
  }

  public function getCurrency()
  {
    return $this->currency;
  }

  public function setCurrency($currency)
  {
    $this->currency = $currency;
  }

  /**
   * Returns length.
   *
   * @param
   * @return length
   */
  public function getLength()
  {
    return $this->length;
  }

  /**
   * Sets length.
   *
   * @param length
   * @return
   */
  public function setLength($length)
  {
    $this->length = $length;
  }

  /**
   * Returns width.
   *
   * @param
   * @return width
   */
  public function getWidth()
  {
    return $this->width;
  }

  /**
   * Sets width.
   *
   * @param width
   * @return
   */
  public function setWidth($width)
  {
    $this->width = $width;
  }

  /**
   * Returns height.
   *
   * @param
   * @return height
   */
  public function getHeight()
  {
    return $this->height;
  }

  /**
   * Sets height.
   *
   * @param height
   * @return
   */
  public function setHeight($height)
  {
    $this->height = $height;
  }

  /**
   * Returns lengthUnit.
   *
   * @param
   * @return lenghtUnit
   */
  public function getLengthUnit()
  {
    return $this->lengthUnit;
  }

  /**
   * Sets lengthUnit.
   *
   * @param lengthUnit
   * @return
   */
  public function setLengthUnit($lengthUnit)
  {
    $this->lengthUnit = $lengthUnit;
  }

  /**
   * Returns weight.
   *
   * @param
   * @return weight
   */
  public function getWeight()
  {
    return $this->weight;
  }

  /**
   * Sets weight.
   *
   * @param weight
   * @return
   */
  public function setWeight($weight)
  {
    $this->weight = $weight;
  }

  /**
   * Returns weightUnit.
   *
   * @param
   * @return weightUnit
   */
  public function getWeightUnit()
  {
    return $this->weightUnit;
  }

  /**
   * Sets weightUnit.
   *
   * @param weightUnit
   * @return
   */
  public function setWeightUnit($weightUnit)
  {
    $this->weightUnit = $weightUnit;
  }

  public function getProducts()
  {
    return $this->products;
  }

  public function setProducts($products=array())
  {
    $this->products = $products;
  }

  public function addProduct($product)
  {
    array_push($this->products, $product);
  }

  public function __construct($orderId, $created, $trackingNumber, $shippingMethod=null, $serviceId=null,
                              $courierName=null, $shipDate=null, $expectedDelivery=null,
                              $cost=null, $products=array(), $currency=null, $length=null, $width=null,
                              $height=null, $weight=null, $lengthUnit=null, $weightUnit=null)
  {
    $this->setOrderId($orderId);
    $this->setCreated($created);
    $this->setTrackingNumber($trackingNumber);
    $this->setShippingMethod($shippingMethod);
    $this->setServiceId($serviceId);
    $this->setCourierName($courierName);
    $this->setShipDate($shipDate);
    $this->setExpectedDelivery($expectedDelivery);
    $this->setCost($cost);
    $this->setProducts($products);
    $this->setCurrency($currency);
    $this->setLength($length);
    $this->setWidth($width);
    $this->setHeight($height);
    $this->setWeight($weight);
    $this->setLengthUnit($lengthUnit);
    $this->setWeightUnit($weightUnit);
  }
}
?>