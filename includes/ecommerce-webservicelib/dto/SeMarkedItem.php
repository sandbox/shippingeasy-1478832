<?php
/**
 * SeMarkedItem.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one item in request result array.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeMarkedItem.v.0.1
 */
class SeMarkedItem
{
  protected $orderId;

  protected $orderProductId;

  protected $quantity;

  public function getOrderProductId()
  {
    return $this->orderProductId;
  }

  public function getOrderId()
  {
    return $this->orderId;
  }

  public function setOrderId($orderId)
  {
    $this->orderId = $orderId;
  }

  public function setOrderProductId($orderProductId)
  {
    $this->orderProductId = $orderProductId;
  }

  public function getQuantity()
  {
    return $this->quantity;
  }

  public function setQuantity($quantity)
  {
    $this->quantity = $quantity;
  }

  public function __construct($orderId, $orderProductId, $quantity)
  {
    $this->setOrderId($orderId);
    $this->setOrderProductId($orderProductId);
    $this->setQuantity($quantity);
  }
}
?>