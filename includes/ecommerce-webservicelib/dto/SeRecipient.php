<?php
/**
 * SeRecipient.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one item in request result array.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeRecipient.v.0.1
 */
class SeRecipient
{
  protected $id;

  protected $firstName;

  protected $lastName;

  protected $phone;

  protected $email;

  protected $line1;

  protected $line2;

  protected $city;

  protected $postalCode;

  protected $state;

  protected $country;

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getFirstName()
  {
    return $this->firstName;
  }

  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;
  }

  public function getLastName()
  {
    return $this->lastName;
  }

  public function setLastName($lastName)
  {
    $this->lastName = $lastName;
  }

  public function getPhone()
  {
    return $this->phone;
  }

  public function setPhone($phone)
  {
    $this->phone = $phone;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }

  public function getLine1()
  {
    return $this->line1;
  }

  public function setLine1($line1)
  {
    $this->line1 = $line1;
  }

  public function getLine2()
  {
    return $this->line2;
  }

  public function setLine2($line2)
  {
    $this->line2 = $line2;
  }

  public function getCity()
  {
    return $this->city;
  }

  public function setCity($city)
  {
    $this->city = $city;
  }

  public function getPostalCode()
  {
    return $this->postalCode;
  }

  public function setPostalCode($postalCode)
  {
    $this->postalCode = $postalCode;
  }

  public function getState()
  {
    return $this->state;
  }

  public function setState($state)
  {
    $this->state = $state;
  }

  public function getCountry()
  {
    return $this->country;
  }

  public function setCountry($country)
  {
    $this->country = $country;
  }

  public function __construct($id=null, $firstName=null, $lastName=null, $phone=null, $email=null,
                              $line1=null, $line2=null, $city=null, $postalCode=null, $state=null,
                              $country=null)
  {
    $this->setId($id);
    $this->setFirstName($firstName);
    $this->setLastName($lastName);
    $this->setPhone($phone);
    $this->setEmail($email);
    $this->setLine1($line1);
    $this->setLine2($line2);
    $this->setCity($city);
    $this->setPostalCode($postalCode);
    $this->setState($state);
    $this->setCountry($country);
  }
}
?>