<?php
/**
 * SeCurlMarkShippedItems.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents request result array.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlMarkShippedItems.v.0.1
 */
class SeCurlMarkShippedItems
{
  protected $timeStamp;

  protected $errorMessage;

  protected $markedItems=array();

  public function getTimeStamp()
  {
    return $this->timeStamp;
  }

  public function setTimeStamp($timeStamp)
  {
    $this->timeStamp = $timeStamp;
  }

  public function getErrorMessage()
  {
    return $this->errorMessage;
  }

  public function setErrorMessage($errorMessage)
  {
    $this->errorMessage = $errorMessage;
  }

  public function getMarkedItems()
  {
    return $this->markedItems;
  }

  public function setMarkedItems($markedItems)
  {
    $this->markedItems = $markedItems;
  }

  public function addMarkedItem($markedItem)
  {
    array_push($this->markedItems, $markedItem);
  }

  public function __construct($timeStamp=null, $errorMessage=null, $markedItems=array())
  {
    $this->setTimeStamp($timeStamp);
    $this->setErrorMessage($errorMessage);
    $this->setMarkedItems($markedItems);
  }
}
?>