<?php
/**
 * SeCurlGetItemsForShipping.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents request result array.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetItemsForShipping.v.0.1
 */
class SeCurlGetItemsForShipping
{
  protected $timeStamp;

  protected $errorMessage;

  protected $orders=array();

  public function getTimeStamp()
  {
    return $this->timeStamp;
  }

  public function setTimeStamp($timeStamp)
  {
    $this->timeStamp = $timeStamp;
  }

  public function getErrorMessage()
  {
    return $this->errorMessage;
  }

  public function setErrorMessage($errorMessage)
  {
    $this->errorMessage = $errorMessage;
  }

  public function getOrders()
  {
    return $this->orders;
  }

  public function setOrders($orders)
  {
    $this->orders = $orders;
  }

  public function addOrder($order)
  {
    array_push($this->orders, $order);
  }

  public function __construct($timeStamp=null, $errorMessage=null, $orders=array())
  {
    $this->setTimeStamp($timeStamp);
    $this->setErrorMessage($errorMessage);
    $this->setOrders($orders);
  }
}
?>