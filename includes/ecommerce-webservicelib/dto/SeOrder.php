<?php
/**
 * SeOrder.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeDto
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents one item in request result array.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeOrder.v.0.1
 */
class SeOrder
{
  protected $id;

  protected $date;

  protected $recipient;

  protected $items=array();

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getDate()
  {
    return $this->date;
  }

  public function setDate($date)
  {
    $this->date = $date;
  }

  public function getRecipient()
  {
    return $this->recipient;
  }

  public function setRecipient($recipient)
  {
    $this->recipient = $recipient;
  }

  public function getItems()
  {
    return $this->items;
  }

  public function setItems($items=array())
  {
    $this->items = $items;
  }

  public function addItem($item)
  {
    array_push($this->items, $item);
  }

  public function __construct($id=null, $date=null, $recipient=null, $items=array())
  {
    $this->setId($id);
    $this->setDate($date);
    $this->setRecipient($recipient);
    $this->setItems($items);
  }
}
?>