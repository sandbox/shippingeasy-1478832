<?php
/**
 * SeCurlResource.php.
 *
 * PHP Version 5.3.1
 *
 * @category  Resource
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents an abstract implementation of every resource.
 *
 * @package    ShippingEasy
 * @subpackage ExternalApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlResource.v.0.1
 */
abstract class SeCurlResource
{
  protected $requestMethod;

  protected $headers=array();

  protected $parameters=array();

  protected $responseObject;

  protected $result;

  protected $generatedApiKey;

  protected $shouldCheckRequest=true;

  abstract public function execute();

  abstract public function parse();

  abstract public function rollback();

  public function getRequestMethod()
  {
    return $this->requestMethod;
  }

  public function setRequestMethod($requestMethod)
  {
    $this->requestMethod = $requestMethod;
  }

  public function getHeaders()
  {
    return $this->headers;
  }

  public function getHeaderItem($key)
  {
    if (isset($this->headers[$key]))
    {
      return $this->headers[$key];
    }
    else
    {
      return null;
    }
  }

  private function setHeaders($headers)
  {
    $this->headers = $headers;
  }

  public function getParameters()
  {
    return $this->parameters;
  }

  public function getParameter($key)
  {
    if (isset($this->parameters[$key]))
    {
      return $this->parameters[$key];
    }
    else
    {
      return null;
    }
  }

  public function setParameters($parameters)
  {
    $this->parameters = $parameters;
  }

  public function setParameter($key, $value)
  {
    $this->parameters[$key] = $value;
  }

  public function getResult()
  {
    return $this->result;
  }

  public function setResult($result)
  {
    $this->result = $result;
  }

  public function getResponseObject()
  {
    return $this->responseObject;
  }

  public function setResponseObject($responseObject)
  {
    $this->responseObject = $responseObject;
  }

  public function getGeneratedApiKey()
  {
    return $this->generatedApiKey;
  }

  public function setGeneratedApiKey($generatedApiKey)
  {
    $this->generatedApiKey = $generatedApiKey;
  }

  public function getShouldCheckRequest()
  {
    return $this->shouldCheckRequest;
  }

  public function setShouldCheckRequest($shouldCheckRequest)
  {
    $this->shouldCheckRequest = $shouldCheckRequest;
  }

  public function fetchMandatoryParameters($parameters=array())
  {
    if ($this->getRequestMethod() == 'GET')
    {
      foreach($parameters as $key)
      {
        $value = SeApiUtils::getParameter($key);

        if (!is_null($value))
        {
          $this->setParameter($key, $value);
        }
        else
        {
          SeApiUtils::outputError('Parameter (' . $key . ') is not set.', 400);
        }
      }
    }
    elseif ($this->getRequestMethod() == 'POST')
    {
      $value = file_get_contents("php://input");

      if ($value !== FALSE)
      {
        $this->setParameter('postedJson', $value);
      }
      else
      {
        SeApiUtils::outputError('JSON object is not sent.', 400);
      }
    }
    else
    {
      if (isset($_SERVER['REQUEST_METHOD']))
      {
        SeApiUtils::outputError('Request method not allowed (' . $_SERVER['REQUEST_METHOD'] .')', 400);
      }
      else
      {
        SeApiUtils::outputError('Request method not allowed', 400);
      }
    }
  }

  public function fetchOptionalParameters($parameters=array())
  {
    foreach($parameters as $key)
    {
      $value = SeApiUtils::getParameter($key);

      $this->setParameter($key, $value);
    }
  }

  private function getAllHeaders()
  {
    $retarr = array();
    $headers = array();

    if (function_exists('apache_request_headers'))
    {
      $headers = apache_request_headers();
    }
    else
    {
      $headers = array_merge($_ENV, $_SERVER);

      foreach ($headers as $key => $val)
      {
        //we need this header
        if (strpos(strtolower($key), 'content-type') !== FALSE)
        {
          continue;
        }

        if (strtoupper(substr($key, 0, 5)) != "HTTP_" && strtoupper(substr($key, 0, 14)) != "X-ShippingEasy")
        {
          unset($headers[$key]);
        }
      }
    }

    ksort($headers);

    return $headers;
  }

  /*
   * This function checks validity of the request
   */
  public function checkRequest()
  {
    if ($this->getGeneratedApiKey() != $this->getHeaderItem('X-ShippingEasy-Token'))
    {
      SeApiUtils::outputError('Generated APIKEY does not match.', 401);
    }

    if (isset($_SERVER['REQUEST_METHOD']))
    {
      if (!in_array($_SERVER['REQUEST_METHOD'], array('POST', 'GET')))
      {
        SeApiUtils::outputError('Request method not allowed (' . $_SERVER['REQUEST_METHOD'] .')', 400);
      }
    }
  }

  public function __construct($generatedApiKey)
  {
    $this->setGeneratedApiKey($generatedApiKey);

    $this->setHeaders($this->getAllHeaders());

    if ($this->getShouldCheckRequest())
    {
      $this->checkRequest();
    }

    $this->execute();

    $this->parse();
  }
}
?>