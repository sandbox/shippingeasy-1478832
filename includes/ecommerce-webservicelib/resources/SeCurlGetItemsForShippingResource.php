<?php
SeApiUtils::checkInclude(
  array(
    'SeCurlResource',
    'SeCurlGetItemsForShipping',
    'SeOrder',
    'SeRecipient',
    'SeOrderItem',
  )
);

/**
 * SeCurlGetItemsForShippingResource.php.
 *
 * PHP Version 5.3.1
 *
 * @category  Resource
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents an implementation of GetItemsForShipping resource.
 *
 * @package    ShippingEasy
 * @subpackage ExternalApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlGetItemsForShippingResource.v.0.1
 */
class SeCurlGetItemsForShippingResource extends SeCurlResource
{
  public function __construct($generatedApiKey)
  {
    $this->setRequestMethod('GET');

    $this->fetchMandatoryParameters(array());

    parent::__construct($generatedApiKey);
  }

  public function execute()
  {
    try
    {
      // get configuration parameters
      $orderStatuses = array_filter(variable_get('shippingeasy_itemsforshipping_order_statuses',
        _shippingeasy_itemsforshipping_order_statuses()));

      $sortedOrderStatuses = array();

      foreach ($orderStatuses as $key=>$value)
      {
        array_push($sortedOrderStatuses, "'".$key."'");
      }

      $statusCondition = '';

      if (count($sortedOrderStatuses) > 0)
      {
        $statusCondition = " AND o.order_status IN (" . implode(',', $sortedOrderStatuses) . ") ";
      }

      $paidOrderStatus = 'payment_received';

//      $paidOrderStatus = variable_get('shippingeasy_paid_order_status', 'payment_received');

      // get total number of orders to be shipped
      $entries = db_result(db_query(
        "
          SELECT COUNT(x.*)
          FROM
          (
            SELECT DISTINCT o.*
            FROM uc_orders o
              LEFT OUTER JOIN uc_order_products op ON o.order_id = op.order_id
              LEFT OUTER JOIN uc_products up ON op.nid = up.nid
            WHERE up.shippable = 1 ". $statusCondition . "
          ) AS x
        "
      ));

      $object = new SeCurlGetItemsForShipping();

      // get actual orders data which status is in configured statuses
      $result = db_query(
        "
          SELECT oo.*, z.zone_name, z.zone_code, c.country_iso_code_2
          FROM
          (
            SELECT DISTINCT o.order_id
            FROM uc_orders o
              LEFT OUTER JOIN uc_order_products op ON o.order_id = op.order_id
              LEFT OUTER JOIN uc_products up ON op.nid = up.nid
            WHERE up.shippable = 1 " . $statusCondition . "
          ) AS x
            LEFT JOIN uc_orders oo ON x.order_id = oo.order_id
            LEFT OUTER JOIN uc_zones z ON oo.delivery_zone = z.zone_id
            LEFT OUTER JOIN uc_countries c ON oo.delivery_country = c.country_id
          ORDER BY oo.order_id
        "
      );

      while ($row = db_fetch_array($result))
      {
        $order = new SeOrder();

        $order->setId($row['order_id']);
        $order->setDate($row['modified']);

        // read order status which will be used while determining paid state
        $orderStatus = $row['order_status'];

        $recipient = new SeRecipient();

        $recipient->setFirstName($row['delivery_first_name']);
        $recipient->setLastName($row['delivery_last_name']);
        $recipient->setPhone($row['delivery_phone']);
        $recipient->setEmail($row['primary_email']);
        $recipient->setLine1($row['delivery_street1']);
        $recipient->setLine2($row['delivery_street2']);
        $recipient->setCity($row['delivery_city']);
        $recipient->setPostalCode($row['delivery_postal_code']);
        $recipient->setState($row['zone_code']);
        $recipient->setCountry($row['country_iso_code_2']);

        $order->setRecipient($recipient);

        // get order items
        $products = db_query(
          "
            SELECT op.*, up.*, op.qty * up.sell_price AS total_price
            FROM uc_order_products op
              LEFT JOIN uc_products up ON op.nid = up.nid
            WHERE up.shippable = 1
              AND op.order_id = '".$order->getId()."'
            ORDER BY order_product_id
          "
        );

        while ($product = db_fetch_array($products))
        {
          $orderItem = new SeOrderItem();

          $orderItem->setId($product['order_product_id']);
          $orderItem->setName($product['title']);

          // TODO:
          $orderItem->setTransactionId('');

          $orderItem->setQuantity($product['qty']);
          $orderItem->setPrice((int)$product['sell_price'] * 10000);
          $orderItem->setTotal((int)$product['total_price'] * 10000);
          $orderItem->setCurrency($row['currency']);

          if ($orderStatus == $paidOrderStatus)
          {
            $orderItem->setPaid(true);
          }
          else
          {
            $orderItem->setPaid(false);
          }

          // read product dimensions
          $height = $product['height'];
          $width = $product['width'];
          $length = $product['length'];
          $weight = $product['weight'];
          $lengthUnits = $product['length_units'];
          $weightUnits = $product['weight_units'];

          // load default dimensions if needed
          if (strlen($height) == 0 || trim($height) == '' || $height == 0)
          {
            $height = variable_get('shippingeasy_default_product_height', 2);
          }

          if (strlen($width) == 0 || trim($width) == '' || $width == 0)
          {
            $width = variable_get('shippingeasy_default_product_width', 2);
          }

          if (strlen($length) == 0 || trim($length) == '' || $length == 0)
          {
            $length = variable_get('shippingeasy_default_product_length', 2);
          }

          if (strlen($weight) == 0 || trim($weight) == ''  || $weight == 0)
          {
            $weight = variable_get('shippingeasy_default_product_weight', 2);
          }

          if (strlen($lengthUnits) == 0 || trim($lengthUnits) == '' || strlen($weightUnits) == 0 || trim($weightUnits) == '')
          {
            $units = variable_get('shippingeasy_default_product_units', 0);

            if ($units == 0)
            {
              // imperial
              if (strlen($lengthUnits) == 0 || trim($lengthUnits) == '')
              {
                $lengthUnits = 'in';
              }

              if (strlen($weightUnits) == 0 || trim($weightUnits) == '')
              {
                $weightUnits = 'lb';
              }
            }
            else
            {
              // metric
              if (strlen($lengthUnits) == 0 || trim($lengthUnits) == '')
              {
                $lengthUnits = 'cm';
              }

              if (strlen($weightUnits) == 0 || trim($weightUnits) == '')
              {
                $weightUnits = 'kg';
              }
            }
          }

          // convert units
          if ($weightUnits == 'lb' && $lengthUnits != 'in')
          {
            $length_conversion = uc_length_conversion($lengthUnits, 'in');
          }
          elseif ($weightUnits == 'kg' && $lengthUnits != 'cm')
          {
            $length_conversion = uc_length_conversion($lengthUnits, 'cm');
          }
          else
          {
            $length_conversion = 1;
          }

          $height = round($height * $length_conversion, 2);
          $width = round($width * $length_conversion, 2);
          $length = round($length * $length_conversion, 2);

          // add dimensions to orderItem
          $orderItem->setHeight($height);
          $orderItem->setWidth($width);
          $orderItem->setLength($length);
          $orderItem->setWeight($weight);

          if ($weightUnits == 'lb')
          {
            $orderItem->setIsMetric(false);
          }
          else
          {
            $orderItem->setIsMetric(true);
          }

          // add orderItem to order
          $order->addItem($orderItem);
        }

        // add order to object
        $object->addOrder($order);
      }

      $object->setTimeStamp(time());

      $object->setErrorMessage('');

      $this->setResponseObject($object);
    }
    catch(Exception $e)
    {
      SeApiUtils::outputError($e->getMessage(), 400);
    }
  }

  public function parse()
  {
    $object = $this->getResponseObject();

    if (is_null($object))
    {
      SeApiUtils::outputError('Response object is not set.', 500);
    }

    try
    {
      $result = array();

      $result['timeStamp'] = $object->getTimeStamp();

      $result['errorMessage'] = $object->getErrorMessage();

      $result['orders'] = array();

      foreach($object->getOrders() as $order)
      {
        $loopItem = array();

        $loopItem['id'] = $order->getId();
        $loopItem['date'] = $order->getDate();

        $loopItem['recipient']['id'] = $order->getRecipient()->getId();
        $loopItem['recipient']['firstName'] = $order->getRecipient()->getFirstName();
        $loopItem['recipient']['lastName'] = $order->getRecipient()->getLastName();
        $loopItem['recipient']['phone'] = $order->getRecipient()->getPhone();
        $loopItem['recipient']['email'] = $order->getRecipient()->getEmail();
        $loopItem['recipient']['line1'] = $order->getRecipient()->getLine1();
        $loopItem['recipient']['line2'] = $order->getRecipient()->getLine2();
        $loopItem['recipient']['city'] = $order->getRecipient()->getCity();
        $loopItem['recipient']['postalCode'] = $order->getRecipient()->getPostalCode();
        $loopItem['recipient']['state'] = $order->getRecipient()->getState();
        $loopItem['recipient']['country'] = $order->getRecipient()->getCountry();

        $loopItem['items'] = array();

        foreach($order->getItems() as $item)
        {
          $loopSubItem = array();

          $loopSubItem['id'] = $item->getId();
          $loopSubItem['name'] = $item->getName();
          $loopSubItem['transactionId'] = $item->getTransactionId();
          $loopSubItem['quantity'] = $item->getQuantity();
          $loopSubItem['price'] = $item->getPrice();
          $loopSubItem['total'] = $item->getTotal();
          $loopSubItem['currency'] = $item->getCurrency();
          $loopSubItem['paid'] = $item->getPaid();
          $loopSubItem['height'] = $item->getHeight();
          $loopSubItem['width'] = $item->getWidth();
          $loopSubItem['length'] = $item->getLength();
          $loopSubItem['weight'] = $item->getWeight();
          $loopSubItem['isMetric'] = $item->getIsMetric();

          array_push($loopItem['items'], $loopSubItem);
        }

        array_push($result['orders'], $loopItem);
      }

      SeApiUtils::outputSuccess($result);
    }
    catch (Exception $e)
    {
      SeApiUtils::outputError('Error while parsing response object. ' . $e->getMessage(), 500);
    }
  }

  public function rollback()
  {

  }
}
?>