<?php
/**
 * SeApiUtils.php.
 *
 * PHP Version 5.3.1
 *
 * @category  Utilities
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * SeApiUtils is used to support different methods.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeApiUtils.v.0.1
 */
class SeApiUtils
{
  /**
   * A list of API folders where included files should be searched for.
   *
   * @var array
   */
  private static $folders = array(
    'base',
    'dto',
    'resources'
  );

  /**
   * A list of allowed API resource names.
   *
   * @var array
   */
  private static $resourceNames = array(
    'GetItemsForShipping',
    'MarkShippedItems',
    'Authenticate'
  );

  /**
   * method to check if file/s is/are included and (if it's/they not) -> include it/them.
   * if className parameter is array, then all files in array are included
   * if className parameter is not array, only than className is included
   *
   * @return
   */
  public static function checkInclude($classNames)
  {
    $classNames = is_array($classNames) ? $classNames : array($classNames);

    // parse array of classes
    foreach($classNames as $class)
    {
      if (!class_exists($class))
      {
        foreach(self::$folders as $folder)
        {
          if (file_exists(ABSOLUTE_EXTERNAL_API_PATH . $folder . '/' . $class . '.php'))
          {
            require ABSOLUTE_EXTERNAL_API_PATH . $folder . '/' . $class . '.php';
            break;
          }
        }
      }
    }
  }

  /**
   * method to retrieve GET or POST parameter.
   * optional functionality is to define default value for parameter.
   *
   * @return
   */
  public static function getParameter($name, $default=null)
  {
    $param = null;

    if (isset($_GET[$name]))
    {
      $param = htmlspecialchars($_GET[$name]);
    }
    elseif (isset($_POST[$name]))
    {
      $param = htmlspecialchars($_POST[$name]);
    }

    if (is_null($param))
    {
      $param = $default;
    }

    return $param;
  }

  public static function outputError($message, $code=400)
  {
    $result = array();

    $result['timeStamp'] = time();
    $result['errorMessage'] = $message;

    ob_clean();
    ob_start();
    header("Content-type: application/json", true, $code);
    echo json_encode($result);
    ob_end_flush();
    die;
  }

  public static function outputSuccess($outputArray=array(), $code=200)
  {
    $outputArray['timeStamp'] = time();
    $outputArray['errorMessage'] = '';

    ob_clean();
    ob_start();
    header("Content-type: application/json", true, $code);
    echo json_encode($outputArray);
    ob_end_flush();
    die;
  }

  public static function getResourceNames()
  {
    return self::$resourceNames;
  }
}
?>