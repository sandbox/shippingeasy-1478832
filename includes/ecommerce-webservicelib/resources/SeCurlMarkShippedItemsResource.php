<?php
SeApiUtils::checkInclude(
  array(
    'SeCurlResource',
    'SeCurlMarkShippedItems',
    'SeShippingItem',
    'SeShippingProductItem'
  )
);

/**
 * SeCurlMarkShippedItemsResource.php.
 *
 * PHP Version 5.3.1
 *
 * @category  Resource
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents an implementation of MarkShippedItems resource.
 *
 * @package    ShippingEasy
 * @subpackage ExternalApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeCurlMarkShippedItemsResource.v.0.1
 */
class SeCurlMarkShippedItemsResource extends SeCurlResource
{
  public function __construct($generatedApiKey)
  {
    $this->setRequestMethod('POST');

    $this->fetchMandatoryParameters(array());

    parent::__construct($generatedApiKey);
  }

  public function execute()
  {
    $object = new SeCurlMarkShippedItems();

    try
    {
      // get variables
      $jsonString = $this->getParameter('postedJson');

      $json = json_decode($jsonString);

      if (is_array($json->shipments))
      {
        // if count is 0, return empty response
        if (count($json->shipments) == 0)
        {
          $object->setTimeStamp(time());

          $object->setErrorMessage('');

          $this->setResponseObject($object);

          return;
        }

        $markOrderStatus = variable_get('shippingeasy_markorder_order_status', 'completed');

        // get shop address
        $address = array((array)variable_get('uc_quote_store_default_address', new stdClass()));

        if (!count($address))
        {
          SeApiUtils::outputError('Default pickup address (shop address) could not be found', 404);
        }

        // this variable contains shipments from JSON string
        $shipments = array();

        // this variable contains sum of products quantities from JSON string
        $ordersProducts = array();

        // we must check if json string has correct order data, because one order can have many shipments,
        // but one json string must contain all shipments for each specific order
        foreach($json->shipments as $shipment)
        {
          if (!array_key_exists($shipment->orderId, $ordersProducts))
          {
            $ordersProducts[$shipment->orderId] = array();
          }

          if (trim($shipment->created) == '')
          {
            $shippingItem = new SeShippingItem($shipment->orderId, time(), $shipment->trackingNumber);
          }
          else
          {
            $shippingItem = new SeShippingItem($shipment->orderId, $shipment->created, $shipment->trackingNumber);
          }

          $shippingItem->setShippingMethod($shipment->shippingMethod);
          $shippingItem->setServiceId($shipment->serviceId);
          $shippingItem->setCourierName($shipment->courierName);
          $shippingItem->setShipDate($shipment->shipDate);
          $shippingItem->setExpectedDelivery($shipment->expectedDelivery);
          $shippingItem->setCost($shipment->cost / 10000);

          if (isset($shipment->length))
          {
            $shippingItem->setLength($shipment->length);
          }

          if (isset($shipment->width))
          {
            $shippingItem->setWidth($shipment->width);
          }

          if (isset($shipment->height))
          {
            $shippingItem->setHeight($shipment->height);
          }

          if (isset($shipment->weight))
          {
            $shippingItem->setWeight($shipment->weight);
          }

          if (isset($shipment->isMetric))
          {
            if ($shipment->isMetric === false)
            {
              $shippingItem->setWeightUnit('lb');
              $shippingItem->setLengthUnit('in');
            }
            if ($shipment->isMetric === true)
            {
              $shippingItem->setWeightUnit('kg');
              $shippingItem->setLengthUnit('cm');
            }
          }

          if (isset($shipment->currency))
          {
            $shippingItem->setCurrency($shipment->currency);
          }

          foreach ($shipment->products as $product)
          {
            $shippingProductItem = new SeShippingProductItem($product->orderProductId, $product->quantity);

            $shippingItem->addProduct($shippingProductItem);

            // handle quantities in $ordersProducts[$shipment->orderId] array
            if (!array_key_exists($product->orderProductId, $ordersProducts[$shipment->orderId]))
            {
              // check if order_product_id exists in database for selected order
              $entries = db_result(db_query(
                "
                  SELECT COUNT(*)
                  FROM uc_order_products op
                    LEFT JOIN uc_products up ON op.nid = up.nid
                  WHERE up.shippable = 1
                    AND op.order_product_id = '".$product->orderProductId."'
                "
              ));

              if ($entries == 0)
              {
                SeApiUtils::outputError('OrderProductID (' . $product->orderProductId .
                  ') does not exists for Order ('. $shipment->orderId . ')', 400);
              }

              // if not found, push initial quantity
              $ordersProducts[$shipment->orderId][$product->orderProductId] = $product->quantity;
            }
            else
            {
              // if found, increase sum for current $product->orderProductId
              $ordersProducts[$shipment->orderId][$product->orderProductId] =
                $ordersProducts[$shipment->orderId][$product->orderProductId] + $product->quantity;
            }
          }

          $shippingItem->storeAddress = $address[0];

          $shippingItem->orderData = array();

          $delivery = db_query(
            "
              SELECT *
              FROM uc_orders
              WHERE order_id = '" . $shipment->orderId . "'
            "
          );

          while ($row = db_fetch_array($delivery))
          {
            $deliveryAddress[] = $row;

            // if order status is completed, throw error
            if ($row['order_status'] == $markOrderStatus)
            {
              SeApiUtils::outputError('Order already marked as shipped. orderId=' . $shipment->orderId, 404);
            }

            if (!count($deliveryAddress))
            {
              SeApiUtils::outputError('Delivery address could not be found for orderId=' . $shipment->orderId, 404);
            }
            else
            {
              $shippingItem->orderData = $row;
            }
          }

          array_push($shipments, $shippingItem);
        }

        // check if $ordersProducts quantities are equal to database products quantities
        foreach($ordersProducts as $key => $value)
        {
          // get order's shippable products
          $numberProducts = db_result(db_query(
            "
              SELECT COUNT(*)
              FROM uc_order_products op
                LEFT JOIN uc_products up ON op.nid = up.nid
              WHERE up.shippable = 1
                AND op.order_id = '".$key."'
            "
          ));

          if ($numberProducts != count($ordersProducts[$key]))
          {
            SeApiUtils::outputError('Number of products (' . $numberProducts .
              ') does not match to sent products count ('. count($ordersProducts[$key]) .
              ') for Order (' . $key . ')', 400);
          }

          $products = db_query(
            "
              SELECT op.*, up.*, op.qty * up.sell_price AS total_price
              FROM uc_order_products op
                LEFT JOIN uc_products up ON op.nid = up.nid
              WHERE up.shippable = 1
                AND op.order_id = '".$key."'
            "
          );

          while ($product = db_fetch_array($products))
          {
            $pId = $product['order_product_id'];

            if ($product['qty'] != $value[$pId])
            {
              SeApiUtils::outputError('Quantity (' . $product['qty'] . ') does not match to sent product quantity ('.
                $value[$pId] . ') for OrderProductId (' . $product['order_product_id'] . ')', 400);
            }
          }
        }

        // at this point we have everything checked and we can proceed with insert and update
        foreach($shipments as $shipment)
        {
          db_query(sprintf("INSERT INTO uc_shipments (
              order_id,
              o_first_name,
              o_last_name,
              o_company,
              o_street1,
              o_street2,
              o_city,
              o_zone,
              o_postal_code,
              o_country,
              d_first_name,
              d_last_name,
              d_company,
              d_street1,
              d_street2,
              d_city,
              d_zone,
              d_postal_code,
              d_country,
              shipping_method,
              accessorials,
              carrier,
              transaction_id,
              tracking_number,
              ship_date,
              expected_delivery,
              cost,
              changed
          ) VALUES (
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
            )",
            $shipment->getOrderId(),
            $shipment->storeAddress['first_name'],
            $shipment->storeAddress['last_name'],
            $shipment->storeAddress['company'],
            $shipment->storeAddress['street1'],
            $shipment->storeAddress['street2'],
            $shipment->storeAddress['city'],
            $shipment->storeAddress['zone'],
            $shipment->storeAddress['postal_code'],
            $shipment->storeAddress['country'],
            $shipment->orderData['delivery_first_name'],
            $shipment->orderData['delivery_last_name'],
            $shipment->orderData['delivery_company'],
            $shipment->orderData['delivery_street1'],
            $shipment->orderData['delivery_street2'],
            $shipment->orderData['delivery_city'],
            $shipment->orderData['delivery_zone'],
            $shipment->orderData['delivery_postal_code'],
            $shipment->orderData['delivery_country'],
            $shipment->getShippingMethod(),
            $shipment->getServiceId(),
            $shipment->getCourierName(),
            '',                                  // this is transactionId
            $shipment->getTrackingNumber(),
            $shipment->getShipDate(),
            $shipment->getExpectedDelivery(),
            $shipment->getCost(),
            $shipment->getCreated()
          ));

          $lastId = db_last_insert_id('uc_shipments', 'sid');

          if ($lastId > 0)
          {
            // save package
            db_query(sprintf("INSERT INTO uc_packages (
              order_id,
              shipping_type,
              pkg_type,
              length,
              width,
              height,
              length_units,
              value,
              sid,
              tracking_number
            ) VALUES (
              '%s',
              '%s',
              '%s',
              '%s',
              '%s',
              '%s',
              '%s',
              '%s',
              '%s',
              '%s'
            )",
              $shipment->getOrderId(),
              'small_package',
              '',
              $shipment->getLength(),
              $shipment->getWidth(),
              $shipment->getHeight(),
              $shipment->getLengthUnit(),
              $shipment->getCost(),
              $lastId,
              $shipment->getTrackingNumber()
            ));

            $lastPackageId = db_last_insert_id('uc_packages', 'package_id');

            if ($lastPackageId > 0)
            {
              uc_order_comment_save($shipment->getOrderId(), 0, 'Package ' . $lastPackageId .' record created.',
                'order', $markOrderStatus);

              // get shipped products
              $products = $shipment->getProducts();

              foreach($products as $product)
              {
                // save package products
                db_query(sprintf("INSERT INTO uc_packaged_products (
                  package_id,
                  order_product_id,
                  qty
                ) VALUES (
                  '%s',
                  '%s',
                  '%s'
                )",
                  $lastPackageId,
                  $product->getOrderProductId(),
                  $product->getQuantity()
                ));

                $markedItem = new SeMarkedItem($shipment->getOrderId(), $product->getOrderProductId(),
                                             $product->getQuantity());

                // store previous order status just in case if we need to rollback
                $markedItem->previousOrderStatus = $shipment->orderData['order_status'];

                $object->addMarkedItem($markedItem);
              }

              // invoke shipment save event/trigger
              $calledShipment = uc_shipping_shipment_load($lastId);

              module_invoke_all('shipment', 'save', $calledShipment);

              uc_order_comment_save($shipment->getOrderId(), 0, 'Shipment record created.', 'order',
                $markOrderStatus);

              // update order_status in uc_orders table
              uc_order_update_status($shipment->getOrderId(), $markOrderStatus);
            }
            else
            {
              $this->setResponseObject($object);

              $this->rollback();

              SeApiUtils::outputError('Cannot determine last package id.', 401);
            }
          }
          else
          {
            $this->setResponseObject($object);

            $this->rollback();

            SeApiUtils::outputError('Cannot determine last insert id.', 401);
          }
        }

        $object->setTimeStamp(time());

        $object->setErrorMessage('');

        $this->setResponseObject($object);
      }
      else
      {
        SeApiUtils::outputError('This resource expects valid JSON object as input.', 500);
      }
    }
    catch(Exception $e)
    {
      // at this point we should rollback all inserts which are in $object shipments property
      // because foreach statement has failed before all inserts are finished
      $this->setResponseObject($object);

      $this->rollback();

      SeApiUtils::outputError($e->getMessage(), 401);
    }
  }

  public function parse()
  {
    $object = $this->getResponseObject();

    if (is_null($object))
    {
      SeApiUtils::outputError('Response object is not set.', 500);
    }

    try
    {
      $result = array();

      $result['timeStamp'] = $object->getTimeStamp();

      $result['errorMessage'] = $object->getErrorMessage();

      $result['markedItems'] = array();

      foreach($object->getMarkedItems() as $markedItem)
      {
        $loopItem = array();

        $loopItem['orderId'] = $markedItem->getOrderId();
        $loopItem['orderProductId'] = $markedItem->getOrderProductId();
        $loopItem['quantity'] = $markedItem->getQuantity();

        array_push($result['markedItems'], $loopItem);
      }

      SeApiUtils::outputSuccess($result);
    }
    catch (Exception $e)
    {
      $this->rollback();

      SeApiUtils::outputError('Error while parsing response object. ' . $e->getMessage(), 500);
    }
  }

  public function rollback()
  {
    $object = $this->getResponseObject();

    if (is_array($object->getMarkedItems()))
    {
      foreach($object->getMarkedItems() as $markedItem)
      {
        // delete shipment data from uc_shipments
        db_query("DELETE FROM uc_shipments WHERE order_id = '" . $markedItem->getOrderId() . "'");

        // delete package data from uc_packages
        db_query("DELETE FROM uc_packages WHERE order_id = '" . $markedItem->getOrderId() . "'");

        // delete comments data from uc_order_comments
        db_query("DELETE FROM uc_order_comments WHERE order_id = '" . $markedItem->getOrderId() . "' AND order_status = 'completed'");

        // update order status to previous state
        db_query("UPDATE uc_orders SET order_status = '" . $markedItem->previousOrderStatus .
          "' WHERE order_id = '" . $markedItem->getOrderId() . "'");
      }
    }
  }
}
?>