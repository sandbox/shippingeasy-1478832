<?php
define('ABSOLUTE_EXTERNAL_API_PATH', dirname(__FILE__) . '/');

require ABSOLUTE_EXTERNAL_API_PATH . 'base/SeApiUtils.php';

SeApiUtils::checkInclude(
  array(
    'SeExternalApi',
    'SeResourceFactory',
    'SeApiType',
    'SeCurlGetItemsForShipping',
    'SeCurlMarkShippedItems',
    'SeMarkedItem',
    'SeOrder',
    'SeOrderItem',
    'SeShippingItem',
    'SeShippingProductItem',
    'SeRecipient',
    'SeCurlResource'
  )
);

class SeApi extends SeExternalApi
{
  public function __construct($generatedApiKey, $apiType=SeApiType::Curl, $username=null, $password=null)
  {
    parent::__construct($generatedApiKey, $apiType, $username, $password);
  }

  public function login()
  {
    return true;
  }
}
?>