<?php
/**
 * SeExternalApi.php.
 *
 * PHP Version 5.3.1
 *
 * @category  SeExternalApi
 * @package   Shippingeasy
 * @author    Saturized - The Interactive Agency <office@saturized.com>
 * @copyright 2010 Saturized - The Interactive Agency
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPLv2
 * @version   SVN: $Id: nebojsa $
 */

/**
 * This class represents implementation of internal functionality which is not exposed as a resource.
 *
 * @package    ShippingEasy
 * @subpackage SeApi
 * @author     Saturized - The Interactive Agency <office@saturized.com>
 * @version    Release: SeExternalApi.v.0.1
 */
abstract class SeExternalApi
{
  private $generatedApiKey;
  private $apiType;
  private $username;
  private $password;

  // this function should implement login functionality if required
  // if login functionality is not required, this function should return true
  abstract public function login();

  private function getApiType()
  {
    return $this->apiType;
  }

  private function getGeneratedApiKey()
  {
    return $this->generatedApiKey;
  }

  private function getUsername()
  {
    return $this->username;
  }

  private function getPassword()
  {
    return $this->password;
  }

  public function __construct($generatedApiKey, $apiType=SeApiType::Curl, $username=null, $password=null)
  {
    $this->generatedApiKey = $generatedApiKey;
    $this->apiType = $apiType;
    $this->username = $username;
    $this->password = $password;
  }

  public function executeResource($resource)
  {
    try
    {
      if ($this->login())
      {
        $res = explode('-', $resource);

        $resOutput = '';

        foreach($res as $item)
        {
          $resOutput = $resOutput . ucfirst($item);
        }

        $result = SeResourceFactory::create($resOutput, $this->getGeneratedApiKey(), $this->getApiType());
      }
      else
      {
        SeApiUtils::outputError('Cannot login user.', 400);
      }
    }
    catch (Exception $e)
    {
      SeApiUtils::outputError('Request error: ' . $e->getMessage(), 500);
    }
  }
}
?>