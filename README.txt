ShippingEasy module
========================================================

Version compatibility and requirements information:

1. ShippingEasy for Drupal is tested on Drupal 6
2. Installed and enabled Ubercart is required for this module to operate
   properly
3. ShippingEasy is tested with Ubercart 6.x.2.7.



Installation instructions:

1. Copy/Extract shippingeasy folder to Drupal's modules folder
2. Login to Drupal with admin credentials
3. Navigate to Administer/Site building/Modules
4. Find ShippingEasy module and mark checkbox next to it
5. Scroll down and click on Save configuration


Features overview
========================================================

1. Fully configurable ShippingEasy shipping method
2. Shipping quotes and fulfillment, including integration with many
   shipping providers (UPS, USPS, FedEx etc) - the list of supported
   providers is automatically updated by installed module
3. Ability to select which services will be shown on buyer's checkout
   screen in quotes section - the list of supported services is
   automatically updated by installed module
4. Integrated API communication with eCommerce application
5. Ability to ship products from one checkout separately in packages
6. More features are coming soon! - ShippingEasy Drupal module is
   constantly extended


Configuration:
========================================================

Prerequisites: ShippingEasy module is installed and enabled


- Configuring ShippingEasy shipping method:

1. Navigate to Administer/Store administration/Configuration/Shipping
   quote settings
2. Click on Quote settings
3. Populate default pickup address on this page if it is not already
   set (required)
4. Click on Quote methods
5. Make sure that ShippingEasy method is on the list and mark it as enabled
6. Click on Save configuration
7. Click on Shipping Easy tab below Quote methods tab
8. On this page you can configure Shipping Easy method in details
9. Select which courier services should appear in quotes list during checkout.
   Service list is updated automatically by module itself - use 
   check/uncheck all to speed up the process
10. When finished, click on 'Save configuration'

Note: 'Save configuration' process will validate API KEY to make sure your
API KEY is functional.


- Configuring ShippingEasy external API:

1. Navigate to Administer/Store administration/Configuration/ShippingEasy
   configuration
2. On this page you can see Drupal API key which is needed for communication
   with eCommerce application as well as shop domain for external API.
3. On this page, you can setup which order statuses should be used in various
   external API processes.
4. When finished, click on 'Save configuration'


- Configuring Phone number to be required during checkout process

1. Navigate to Administer/Store administration/Configuration/Checkout settings
2. Click on Edit on the top of the page
3. Find Phone number field and mark it as Required
4. When finished, click on 'Save configuration'


- Configuring default pickup address for products (this address will be used
  for ShippingEasy rates)

1. Navigate to Administer/Store administration/Configuration/Shipping quote
   settings
2. Click on Quote settings on the top of the page
3. Scroll down and find Default pickup address section - expand it
4. Fill in required fields
5. When finished, click on 'Save configuration'
